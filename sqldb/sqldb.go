package sqldb

import (
	"database/sql"
	"errors"
	"gitlab.com/pgmtc-lib/commons/errs"
	"strings"
)

// Query object represents a db query used by the postgres db client
type Query struct {
	Query       string
	Args        []interface{}
	ParseResult func(rs *sql.Rows) error
}

// Select runs sql select
func Select(db *sql.DB, query Query) (resultErr error) {
	if db == nil {
		return errors.New("db not provided")
	}

	rows, err := db.Query(query.Query, query.Args...)
	if err != nil {
		return errs.Wrap(err, "error when running query")
	}
	defer rows.Close()

	if query.ParseResult != nil {
		for rows.Next() {
			if err := query.ParseResult(rows); err != nil {
				resultErr = errs.Wrap(err, "error when parsing result")
				return
			}
		}
	}
	return
}

// Insert inserts row to db
func Insert(db *sql.DB, query Query) (id interface{}, resultErr error) {
	if db == nil {
		resultErr = errors.New("db not connected")
		return
	}

	row := db.QueryRow(query.Query, query.Args...)
	err := row.Scan(&id)
	if err != nil && !strings.Contains(err.Error(), "no rows in result set") {
		resultErr = err
	}
	return
}
