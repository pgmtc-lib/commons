package sqldb

import (
	"database/sql"
	"fmt"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestSelect(t *testing.T) {
	type gotStruct struct {
		a int
		b int
		c int
	}
	var gotResult []gotStruct
	// test nil db
	assert.EqualError(t, Select(nil, Query{}), "db not provided")

	db, mock, err := sqlmock.New()
	assert.NoError(t, err)
	defer db.Close()

	mock.ExpectQuery("select a,b,c from somewhere where id = (.+) and id2 = (.+)").
		WithArgs("val1", 2).
		WillReturnRows(sqlmock.NewRows([]string{"a", "b", "c"}).AddRow(1, 2, 3).AddRow(10, 20, 30))

	// now we execute our method
	err = Select(db, Query{
		Query: "select a,b,c from somewhere where id = $1 and id2 = ?",
		Args:  []interface{}{"val1", 2},
		ParseResult: func(rs *sql.Rows) error {
			var a, b, c int
			if err := rs.Scan(&a, &b, &c); err != nil {
				return err
			}
			gotResult = append(gotResult, gotStruct{a: a, b: b, c: c})
			return nil
		},
	})
	assert.NoError(t, err)

	// we make sure that all expectations were met
	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
	}

	// test success result
	assert.Len(t, gotResult, 2)
	assert.Contains(t, gotResult, gotStruct{a: 1, b: 2, c: 3})
	assert.Contains(t, gotResult, gotStruct{a: 10, b: 20, c: 30})

	// test returned error
	mock.ExpectQuery("select a,b,c from somewhere where id = (.+) and id2 = (.+)").
		WithArgs("val1", 2).
		WillReturnError(fmt.Errorf("mock error"))
	err = Select(db, Query{
		Query: "select a,b,c from somewhere where id = $1 and id2 = ?",
		Args:  []interface{}{"val1", 2},
		ParseResult: func(rs *sql.Rows) error {
			var a, b, c int
			if err := rs.Scan(&a, &b, &c); err != nil {
				return err
			}
			gotResult = append(gotResult, gotStruct{a: a, b: b, c: c})
			return nil
		},
	})
	assert.EqualError(t, err, "error when running query: mock error")

	// test with error during parsing
	mock.ExpectQuery("select a,b,c from somewhere where id = (.+) and id2 = (.+)").
		WithArgs("val1", 2).
		WillReturnRows(sqlmock.NewRows([]string{"a", "b", "c"}).AddRow(1, 2, 3).AddRow(10, 20, 30))
	err = Select(db, Query{
		Query: "select a,b,c from somewhere where id = $1 and id2 = ?",
		Args:  []interface{}{"val1", 2},
		ParseResult: func(rs *sql.Rows) error {
			return fmt.Errorf("mock error 2")
		},
	})
	assert.EqualError(t, err, "error when parsing result: mock error 2")
}

func TestInsert(t *testing.T) {
	assert.EqualError(t, Select(nil, Query{}), "db not provided")

	db, mock, err := sqlmock.New()
	assert.NoError(t, err)
	defer db.Close()

	mock.ExpectQuery("insert into somewhere \\(a, b, c\\) values \\((.+),(.+),(.+)\\)").
		WithArgs("val1", 2, 3).
		WillReturnRows(sqlmock.NewRows([]string{"id"}).AddRow(99))

	id, err := Insert(db, Query{
		Query: "insert into somewhere (a, b, c) values ($1, $2, $3)",
		Args:  []interface{}{"val1", 2, 3},
	})

	assert.NoError(t, err)
	assert.Equal(t, int64(99), id)

	// test error
	mock.ExpectQuery("insert into somewhere \\(a, b, c\\) values \\((.+),(.+),(.+)\\)").
		WithArgs("val1", 2, 3).
		WillReturnError(fmt.Errorf("mock error"))
	id, err = Insert(db, Query{
		Query: "insert into somewhere (a, b, c) values ($1, $2, $3)",
		Args:  []interface{}{"val1", 2, 3},
	})
	assert.EqualError(t, err, "mock error")

}
