package eval

import (
	"gitlab.com/pgmtc-lib/commons/ptrs"
	"testing"
)

func TestAnyStringEmpty(t *testing.T) {
	tests := []struct {
		name   string
		values []string
		want   bool
	}{
		{values: []string{"first", "second", "third"}, want: false},
		{values: []string{"first", "", "third"}, want: true},
		{values: []string{"", "second", "third"}, want: true},
		{values: []string{"first", "second", ""}, want: true},
		{values: []string{}, want: false},
		{values: []string{""}, want: true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := AnyStringEmpty(tt.values...); got != tt.want {
				t.Errorf("AnyStringEmpty() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestAnyIntZero(t *testing.T) {
	tests := []struct {
		name   string
		values []int
		want   bool
	}{
		{values: []int{1, 2, 3}, want: false},
		{values: []int{1, 0, 3}, want: true},
		{values: []int{0, 2, 3}, want: true},
		{values: []int{1, 2, 0}, want: true},
		{values: []int{}, want: false},
		{values: []int{0}, want: true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := AnyIntZero(tt.values...); got != tt.want {
				t.Errorf("AnyIntZero() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestAnyNil(t *testing.T) {
	tests := []struct {
		name   string
		values []interface{}
		want   bool
	}{
		{values: []interface{}{1, "two", ptrs.PtrString("three")}, want: false},
		{values: []interface{}{1, nil, ptrs.PtrString("three")}, want: true},
		{values: []interface{}{nil, "two", ptrs.PtrString("three")}, want: true},
		{values: []interface{}{1, "two", nil}, want: true},
		{values: []interface{}{}, want: false},
		{values: []interface{}{nil}, want: true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := AnyNil(tt.values...); got != tt.want {
				t.Errorf("AnyNil() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestAnyFloatZero(t *testing.T) {
	tests := []struct {
		name   string
		values []float64
		want   bool
	}{
		{values: []float64{0.0000001, 2, 3}, want: false},
		{values: []float64{1, 0, 3}, want: true},
		{values: []float64{0, 2, 3}, want: true},
		{values: []float64{1, 2, 0}, want: true},
		{values: []float64{}, want: false},
		{values: []float64{0.0000000}, want: true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := AnyFloatZero(tt.values...); got != tt.want {
				t.Errorf("AnyIntZero() = %v, want %v", got, tt.want)
			}
		})
	}
}
