package eval

// AnyStringEmpty returns true if any of the provided values is empty
func AnyStringEmpty(values ...string) bool {
	for _, val := range values {
		if val == "" {
			return true
		}
	}
	return false
}

// AnyIntZero returns true if any of the ints is zero
func AnyIntZero(values ...int) bool {
	for _, val := range values {
		if val == 0 {
			return true
		}
	}
	return false
}

// AnyNil returns true if any of the values is nil
func AnyNil(values ...interface{}) bool {
	for _, val := range values {
		if val == nil {
			return true
		}
	}
	return false
}

// AnyFloatZero returns true if any of the floats is zero
func AnyFloatZero(values ...float64) bool {
	for _, val := range values {
		if val == 0 {
			return true
		}
	}
	return false
}
