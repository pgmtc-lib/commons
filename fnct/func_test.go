package fnct

import "testing"

func TestName(t *testing.T) {
	type args struct {
		in interface{}
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "func",
			args: args{in: func(string, int) (string, error) { return "", nil }},
			want: "gitlab.com/pgmtc-lib/commons/fnct.TestName.func1",
		},
		{
			name: "func",
			args: args{in: TestName},
			want: "gitlab.com/pgmtc-lib/commons/fnct.TestName",
		},
		{
			name: "func",
			args: args{in: "string"},
			want: "no-func",
		},
		{
			name: "func",
			args: args{in: nil},
			want: "nil",
		},
		{
			name: "func",
			args: args{},
			want: "nil",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Name(tt.args.in); got != tt.want {
				t.Errorf("Name() = %v, want %v", got, tt.want)
			}
		})
	}
}
