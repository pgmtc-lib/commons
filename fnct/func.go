package fnct

import (
	"reflect"
	"runtime"
)

// Name returns name of the function (or other interface)
func Name(in interface{}) (result string) {
	defer func() {
		if r := recover(); r != nil {
			result = "no-func"
		}
	}()
	if in == nil {
		return "nil"
	}
	return runtime.FuncForPC(reflect.ValueOf(in).Pointer()).Name()
}
