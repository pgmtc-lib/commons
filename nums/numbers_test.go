package nums

import (
	"github.com/stretchr/testify/assert"
	"math"
	"testing"
)

func TestRangeIn(t *testing.T) {
	type args struct {
		low int
		hi  int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{name: "test", args: args{low: 1, hi: 10}},
		{name: "test", args: args{low: 1, hi: 2}},
		{name: "test", args: args{low: 1000, hi: 2000}},
		{name: "test", args: args{low: 100000, hi: 200000}},
		{name: "test", args: args{low: 2, hi: 1}},
		{name: "test", args: args{low: 1, hi: 1}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := RandomInt(tt.args.low, tt.args.hi); got < tt.args.low && got > tt.args.hi {
				t.Errorf("RandomInt() = %v, want between %d and %d", got, tt.args.low, tt.args.hi)
			}
		})
	}
}

func Test_inBoundariesInt(t *testing.T) {
	type args struct {
		a   int
		min int
		max int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{name: "test-within", args: args{a: 5, min: 0, max: 10}, want: 5},
		{name: "test-min", args: args{a: -1, min: 0, max: 10}, want: 0},
		{name: "test-max", args: args{a: 12, min: 0, max: 10}, want: 10},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := InBoundariesInt(tt.args.a, tt.args.min, tt.args.max); got != tt.want {
				t.Errorf("minMaxInt() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_inBoundariesFloat(t *testing.T) {
	type args struct {
		a   float64
		min float64
		max float64
	}
	tests := []struct {
		name string
		args args
		want float64
	}{
		{name: "test-within", args: args{a: 5.0, min: 0.0, max: 10.0}, want: 5.0},
		{name: "test-min", args: args{a: -1.0, min: 0.0, max: 10.0}, want: 0.0},
		{name: "test-max", args: args{a: 12.0, min: 0.0, max: 10.0}, want: 10.0},
		{name: "test-negative-inf", args: args{a: math.Inf(-1), min: 0.0, max: 10.0}, want: 0.0},
		{name: "test-positive-inf", args: args{a: math.Inf(1), min: 0.0, max: 10.0}, want: 10.0},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := InBoundariesFloat(tt.args.a, tt.args.min, tt.args.max); got != tt.want {
				t.Errorf("minMaxFloat() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestIf0ThenInt(t *testing.T) {
	type args struct {
		a    int
		then int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{name: "", args: args{a: 10, then: 20}, want: 10},
		{name: "", args: args{a: 0, then: 20}, want: 20},
		{name: "", args: args{a: -1, then: 20}, want: -1},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := If0ThenInt(tt.args.a, tt.args.then); got != tt.want {
				t.Errorf("If0ThenInt() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestParseString(t *testing.T) {
	type args struct {
		a           string
		ifErrorThen int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{name: "test-ok", args: args{a: "20", ifErrorThen: 30}, want: 20},
		{name: "test-fail", args: args{a: "ABC", ifErrorThen: 30}, want: 30},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ParseString(tt.args.a, tt.args.ifErrorThen); got != tt.want {
				t.Errorf("ParseString() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMinInt(t *testing.T) {
	type args struct {
		values []int
	}
	tests := []struct {
		name       string
		args       args
		wantResult int
	}{
		{
			name:       "test",
			args:       args{values: []int{4, 5, 2, 1, 3}},
			wantResult: 1,
		},
		{
			name:       "test-empty",
			wantResult: 0,
		},
		{
			name:       "test-one",
			args:       args{values: []int{4}},
			wantResult: 4,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotResult := MinInt(tt.args.values...); gotResult != tt.wantResult {
				t.Errorf("MinInt() = %v, want %v", gotResult, tt.wantResult)
			}
		})
	}
}

func TestMaxInt(t *testing.T) {
	type args struct {
		values []int
	}
	tests := []struct {
		name       string
		args       args
		wantResult int
	}{
		{
			name:       "test",
			args:       args{values: []int{4, 5, 2, 1, 3}},
			wantResult: 5,
		},
		{
			name:       "test-empty",
			wantResult: 0,
		},
		{
			name:       "test-one",
			args:       args{values: []int{4}},
			wantResult: 4,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotResult := MaxInt(tt.args.values...); gotResult != tt.wantResult {
				t.Errorf("MaxInt() = %v, want %v", gotResult, tt.wantResult)
			}
		})
	}
}

func TestIf0ThenString(t *testing.T) {
	tests := []struct {
		name string
		a    int
		then string
		want string
	}{
		{name: "test-0", a: 0, then: "zero", want: "zero"},
		{name: "test-1", a: 1, then: "zero", want: "1"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := If0ThenString(tt.a, tt.then); got != tt.want {
				t.Errorf("If0ThenString() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRoundFloat(t *testing.T) {
	type args struct {
		value     float64
		precision int
	}
	tests := []struct {
		name string
		args args
		want float64
	}{
		{args: args{value: 1.123454, precision: 5}, want: 1.12345},
		{args: args{value: 1.123455, precision: 5}, want: 1.12346},
		{args: args{value: 1.123455, precision: 6}, want: 1.123455},
		{args: args{value: 1.123455, precision: 2}, want: 1.12},
		{args: args{value: 1.125, precision: 2}, want: 1.13},
		{args: args{value: 1.9, precision: 0}, want: 2},
		{args: args{value: 64329.9, precision: -1}, want: 64330},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := RoundFloat(tt.args.value, tt.args.precision); got != tt.want {
				t.Errorf("RoundFloat() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestAbsInt(t *testing.T) {
	type args struct {
		x int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{args: args{x: 10}, want: 10},
		{args: args{x: 0}, want: 0},
		{args: args{x: -10}, want: 10},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := AbsInt(tt.args.x); got != tt.want {
				t.Errorf("AbsInt() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestAbsInt64(t *testing.T) {
	type args struct {
		x int64
	}
	tests := []struct {
		name string
		args args
		want int64
	}{
		{args: args{x: 10}, want: 10},
		{args: args{x: 0}, want: 0},
		{args: args{x: -10}, want: 10},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := AbsInt64(tt.args.x); got != tt.want {
				t.Errorf("AbsInt64() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestParseBool(t *testing.T) {
	assert.Equal(t, 1, ParseBool(true))
	assert.Equal(t, 0, ParseBool(false))
}

func TestIf0ThenFloat(t *testing.T) {
	type args struct {
		a    float64
		then float64
	}
	tests := []struct {
		name string
		args args
		want float64
	}{
		{name: "test-0", args: args{a: 0.0, then: 10.0}, want: 10.0},
		{name: "test-1", args: args{a: 1.1, then: 10.0}, want: 1.1},
		{name: "test-2", args: args{a: 0.1, then: 10.0}, want: 0.1},
		{name: "test-3", args: args{a: 0.0, then: 0.0}, want: 0.0},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := If0ThenFloat(tt.args.a, tt.args.then); got != tt.want {
				t.Errorf("If0ThenFloat() = %v, want %v", got, tt.want)
			}
		})
	}
}
