package nums

import (
	"fmt"
	"math"
	"math/rand"
	"strconv"
	"time"
)

// RandomInt returns random number between hi and low. If high <= low, low is returned
func RandomInt(min, max int) int {
	if max <= min {
		return min
	}
	rand.Seed(time.Now().UnixNano())
	return min + rand.Intn(max-min)
}

// InBoundariesInt returns number a if it falls in between min and max, if greater, returns max, if less, return min
func InBoundariesInt(a, min, max int) int {
	if a < min {
		return min
	}
	if a > max {
		return max
	}
	return a
}

// InBoundariesFloat returns number a if it falls in between min and max, if greater, returns max, if less, return min
func InBoundariesFloat(a, min, max float64) float64 {
	if a < min {
		return min
	}
	if a > max {
		return max
	}
	return a
}

// If0ThenInt returns number if a is 0
func If0ThenInt(a, then int) int {
	if a == 0 {
		return then
	}
	return a
}

// If0ThenString returns number if a is 0
func If0ThenString(a int, then string) string {
	if a == 0 {
		return then
	}
	return fmt.Sprintf("%d", a)
}

// If0ThenFloat returns number if a is 0
func If0ThenFloat(a float64, then float64) float64 {
	if a == 0 {
		return then
	}
	return a
}

// ParseString tries to parse string value. If error, then it returns ifErrorThen value
func ParseString(a string, ifErrorThen int) int {
	parsed, err := strconv.Atoi(a)
	if err != nil {
		return ifErrorThen
	}
	return parsed
}

// ParseBool returns 1 for true, 0 for false
func ParseBool(a bool) int {
	if a == true {
		return 1
	}
	return 0
}

// MinInt returns smallest value from the provided values
func MinInt(values ...int) (result int) {
	if len(values) == 0 {
		return
	}
	result = values[0]
	for _, value := range values {
		if value < result {
			result = value
		}
	}
	return
}

// MaxInt returns biggest value from the provided values
func MaxInt(values ...int) (result int) {
	if len(values) == 0 {
		return
	}
	result = values[0]
	for _, value := range values {
		if value > result {
			result = value
		}
	}
	return
}

// RoundFloat rounds float with provided precision
func RoundFloat(value float64, precision int) float64 {
	factor := math.Pow(10, float64(precision))
	return math.Round(value*factor) / factor
}

// AbsInt returns absolute value of an int
func AbsInt(x int) int {
	if x < 0 {
		return -x
	}
	return x
}

// AbsInt64 returns absolute value of an int64
func AbsInt64(x int64) int64 {
	if x < 0 {
		return -x
	}
	return x
}
