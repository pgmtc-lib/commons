package env

import "strings"

func parseArray(in string, itemSeparator string) (result []string) {
	rowSplit := strings.Split(in, itemSeparator)
	for _, row := range rowSplit {
		clensed := strings.Trim(row, " ")
		clensed = strings.Trim(clensed, "\n")
		if clensed == "" {
			continue
		}
		result = append(result, strings.Trim(clensed, " "))
	}
	return
}

func parseMap(in string, itemSeparator string) (result map[string]string) {
	valueSeparator := "->"
	result = make(map[string]string)
	rowSplit := strings.Split(in, itemSeparator)
	for _, row := range rowSplit {
		clensed := strings.Trim(row, " ")
		clensed = strings.Trim(clensed, "\n")
		itemSplit := strings.Split(clensed, valueSeparator)
		if len(itemSplit) != 2 {
			continue
		}
		result[strings.Trim(itemSplit[0], " ")] = strings.Trim(itemSplit[1], " ")
	}
	return
}
