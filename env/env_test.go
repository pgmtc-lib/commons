package env

import (
	"github.com/stretchr/testify/assert"
	"os"
	"testing"
)

func TestGetEnvString(t *testing.T) {
	targetProperty := "initial"
	type args struct {
		varName string
		target  *string
	}
	tests := []struct {
		name           string
		args           args
		expectedChange bool
	}{
		{
			name: "test-home",
			args: args{
				target:  &targetProperty,
				varName: "HOME",
			},
			expectedChange: true,
		},
		{
			name: "test-non-existing",
			args: args{
				target:  &targetProperty,
				varName: "NON_EXISTING_ENV",
			},
			expectedChange: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			targetProperty = "initial"
			GetEnvString(tt.args.varName, tt.args.target)
			if tt.expectedChange != (targetProperty != "initial") {
				t.Errorf("Expected change: %t, got the opposite", tt.expectedChange)

			}

		})
	}
}

func TestGetEnvBool(t *testing.T) {
	targetProperty := true
	os.Setenv("TEST_BOOL_FALSE", "false")
	os.Setenv("TEST_BOOL_NOBOOL", "some-value")
	os.Setenv("TEST_BOOL_TRUE", "true")
	type args struct {
		varName string
		target  *bool
	}
	tests := []struct {
		name           string
		args           args
		initialValue   bool
		expectedChange bool
	}{
		{
			name: "test-true",
			args: args{
				target:  &targetProperty,
				varName: "TEST_BOOL_TRUE",
			},
			initialValue:   false,
			expectedChange: true,
		},
		{
			name: "test-false",
			args: args{
				target:  &targetProperty,
				varName: "TEST_BOOL_FALSE",
			},
			initialValue:   true,
			expectedChange: true,
		},
		{
			name: "test-nobool",
			args: args{
				target:  &targetProperty,
				varName: "TEST_BOOL_NOBOOL",
			},
			initialValue:   false,
			expectedChange: false,
		},
		{
			name: "test-nonexisting",
			args: args{
				target:  &targetProperty,
				varName: "NON_EXISTING_ENV",
			},
			initialValue:   true,
			expectedChange: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			targetProperty = tt.initialValue
			GetEnvBool(tt.args.varName, tt.args.target)
			if tt.expectedChange != (targetProperty != tt.initialValue) {
				t.Errorf("Expected change: %t, got the opposite", tt.expectedChange)

			}

		})
	}
}

func TestGetEnvInt(t *testing.T) {
	targetProperty := 0
	os.Setenv("TEST_PROPERTY", "10")

	type args struct {
		varName string
	}
	tests := []struct {
		name          string
		args          args
		expectedValue int
	}{
		{
			name: "test-existing",
			args: args{
				varName: "TEST_PROPERTY",
			},
			expectedValue: 10,
		},
		{
			name: "test-non-existing",
			args: args{
				varName: "NON_EXISTING",
			},
			expectedValue: 0,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			targetProperty = 0
			GetEnvInt(tt.args.varName, &targetProperty)
			if targetProperty != tt.expectedValue {
				t.Errorf("Unexpected value, want %d, got %d", tt.expectedValue, targetProperty)
			}
		})
	}
}

func TestGetEnvArray(t *testing.T) {
	os.Setenv("TEST_PROPERTY", "element1;element2;element3")
	tests := []struct {
		name          string
		varName       string
		expectedValue []string
	}{
		{
			name:          "test-existing",
			varName:       "TEST_PROPERTY",
			expectedValue: []string{"element1", "element2", "element3"},
		},
		{
			name:    "test-non-existing",
			varName: "NON_EXISTING",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var targetProperty []string
			GetEnvArray(tt.varName, &targetProperty)
			assert.Equal(t, tt.expectedValue, targetProperty)
		})
	}
}

func TestGetEnvMap(t *testing.T) {
	os.Setenv("TEST_PROPERTY", "key1->value1;key2->value2")
	tests := []struct {
		name           string
		varName        string
		expectedValue  map[string]string
		targetProperty map[string]string
	}{
		{
			name:    "test-existing",
			varName: "TEST_PROPERTY",
			expectedValue: map[string]string{
				"key1": "value1",
				"key2": "value2",
			},
		},
		{
			name:           "test-existing-with-initialized-target",
			varName:        "TEST_PROPERTY",
			targetProperty: make(map[string]string),
			expectedValue: map[string]string{
				"key1": "value1",
				"key2": "value2",
			},
		},
		{
			name:    "test-non-existing",
			varName: "NON_EXISTING",
		},
		{
			name:           "test-non-existing-with-initialized-target",
			varName:        "NON_EXISTING",
			targetProperty: make(map[string]string),
			expectedValue:  make(map[string]string),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			GetEnvMap(tt.varName, &tt.targetProperty)
			assert.Equal(t, tt.expectedValue, tt.targetProperty)
		})
	}
}
