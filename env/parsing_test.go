package env

import (
	"reflect"
	"testing"
)

func TestStringToMap(t *testing.T) {
	type args struct {
		in            string
		itemSeparator string
	}
	tests := []struct {
		name       string
		args       args
		wantResult map[string]string
	}{
		{
			name: "test",
			args: args{in: "/path1->http://server.com:8080;/path2->http://server2.com:8080", itemSeparator: ";"},
			wantResult: map[string]string{
				"/path1": "http://server.com:8080",
				"/path2": "http://server2.com:8080",
			},
		},
		{
			name: "test-separator",
			args: args{in: "/path1->http://server.com:8080##/path2->http://server2.com:8080", itemSeparator: "##"},
			wantResult: map[string]string{
				"/path1": "http://server.com:8080",
				"/path2": "http://server2.com:8080",
			},
		},
		{
			name: "test",
			args: args{in: "/path1->http://server.com:8080", itemSeparator: ";"},
			wantResult: map[string]string{
				"/path1": "http://server.com:8080",
			},
		},
		{
			name: "test",
			args: args{in: "  /path1 -> http://server.com:8080; /path2 -> http://server2.com:8080  ", itemSeparator: ";"},
			wantResult: map[string]string{
				"/path1": "http://server.com:8080",
				"/path2": "http://server2.com:8080",
			},
		},
		{
			name: "test",
			args: args{in: "/path1 -> http://server.com:8080;\n/path2 -> http://server2.com:8080\n", itemSeparator: ";"},
			wantResult: map[string]string{
				"/path1": "http://server.com:8080",
				"/path2": "http://server2.com:8080",
			},
		},
		{
			name:       "test",
			args:       args{in: "", itemSeparator: ";"},
			wantResult: map[string]string{},
		},
		{
			name:       "test",
			args:       args{in: "gibberish1;gibberish2,gibberish3->xxx->ddd", itemSeparator: ";"},
			wantResult: map[string]string{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotResult := parseMap(tt.args.in, tt.args.itemSeparator); !reflect.DeepEqual(gotResult, tt.wantResult) {
				t.Errorf("ParseMapParameter() = %v, want %v", gotResult, tt.wantResult)
			}
		})
	}
}

func TestParseArrayParameter(t *testing.T) {
	type args struct {
		in            string
		itemSeparator string
	}
	tests := []struct {
		name       string
		args       args
		wantResult []string
		wantLength int
	}{
		{
			name:       "test",
			args:       args{in: "param1;param2;param3", itemSeparator: ";"},
			wantResult: []string{"param1", "param2", "param3"},
			wantLength: 3,
		},
		{
			name:       "test-separator",
			args:       args{in: "param1##param2##param3", itemSeparator: "##"},
			wantResult: []string{"param1", "param2", "param3"},
			wantLength: 3,
		},
		{
			name:       "test",
			args:       args{in: "gibberish", itemSeparator: ";"},
			wantResult: []string{"gibberish"},
			wantLength: 1,
		},
		{
			name:       "test",
			args:       args{in: " param1 ; param2 ; param3 ", itemSeparator: ";"},
			wantResult: []string{"param1", "param2", "param3"},
			wantLength: 3,
		},
		{
			name:       "test",
			args:       args{in: "", itemSeparator: ";"},
			wantLength: 0,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotResult := parseArray(tt.args.in, tt.args.itemSeparator)
			if !reflect.DeepEqual(gotResult, tt.wantResult) {
				t.Errorf("ParseArrayParameter() = %v, want %v", gotResult, tt.wantResult)
			}
			if len(gotResult) != tt.wantLength {
				t.Errorf("ParseArrayParameter() length = %d, want %d", len(gotResult), tt.wantLength)
			}
		})
	}
}
