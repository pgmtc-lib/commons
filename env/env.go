package env

import (
	"os"
	"strconv"
)

// GetEnvString reads environment variable and fills it into string, if it is defined
func GetEnvString(varName string, target *string) {
	if os.Getenv(varName) != "" {
		*target = os.Getenv(varName)
	}
}

// GetEnvBool reads environment variable and fills it into bool, if it is defined
func GetEnvBool(varName string, target *bool) {
	if os.Getenv(varName) != "" {
		*target = os.Getenv(varName) == "true"
	}
}

// GetEnvInt reads environment variable and fills it into int, if it is defined
func GetEnvInt(varName string, target *int) {
	if os.Getenv(varName) != "" {
		if parsed, err := strconv.Atoi(os.Getenv(varName)); err == nil {
			*target = parsed
		}
	}
}

// GetEnvArray reads environment variable and fills it into string array
func GetEnvArray(varName string, target *[]string) {
	if os.Getenv(varName) == "" {
		return
	}
	in := os.Getenv(varName)
	itemSeparator := ";"
	parsed := parseArray(in, itemSeparator)
	for _, item := range parsed {
		*target = append(*target, item)
	}
}

// GetEnvMap reads env variable and fills it into string pam
func GetEnvMap(varName string, target *map[string]string) {
	if os.Getenv(varName) == "" {
		return
	}
	in := os.Getenv(varName)
	itemSeparator := ";"
	parsed := parseMap(in, itemSeparator)
	if *target == nil {
		newMap := make(map[string]string)
		*target = newMap
	}
	for key, value := range parsed {
		(*target)[key] = value
	}

}
