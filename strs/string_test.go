package strs

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/pgmtc-lib/commons/arrays"
	"gitlab.com/pgmtc-lib/commons/ptrs"
	"reflect"
	"testing"
)

func TestRandomString(t *testing.T) {

	tests := []struct {
		name       string
		argLength  int
		wantLength int
	}{
		{
			name:       "test-4",
			argLength:  4,
			wantLength: 4,
		},
		{
			name:       "test-27",
			argLength:  27,
			wantLength: 27,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := RandomString(tt.argLength); len(got) != tt.wantLength {
				t.Errorf("RandomString() %s length = %d, want %d", got, len(got), tt.wantLength)
			}
		})
	}
}

func TestRandomStringFromLetters(t *testing.T) {
	type args struct {
		length  int
		letters []rune
	}
	tests := []struct {
		name            string
		args            args
		want            string
		wantContainOnly []string
	}{
		{
			name: "test-a",
			args: args{
				length:  4,
				letters: []rune("A"),
			},
			want:            "AAAA",
			wantContainOnly: []string{"A"},
		},
		{
			name: "test-a",
			args: args{
				length:  32,
				letters: []rune("AB"),
			},
			wantContainOnly: []string{"A", "B"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := RandomStringFromLetters(tt.args.length, tt.args.letters)
			if tt.want != "" && got != tt.want {
				t.Errorf("RandomStringFromLetters() = %v, want %v", got, tt.want)
			}
			// Test if contains only given characters
			fmt.Println(got)
			for _, char := range got {
				charStr := fmt.Sprintf("%c", char)
				if !arrays.ContainsString(tt.wantContainOnly, charStr) {
					t.Errorf("Unexpected char %s in the results", charStr)
				}
			}

		})
	}
}

func TestIfNotEmptyDefault(t *testing.T) {
	type args struct {
		inValue          string
		defValue         string
		ifNotEmptyValues []string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "test",
			args: args{
				inValue:  "invalue",
				defValue: "defvalue",
			},
			want: "invalue",
		},
		{
			name: "test",
			args: args{
				inValue:          "invalue",
				defValue:         "defvalue",
				ifNotEmptyValues: []string{"not", "empty"},
			},
			want: "notempty",
		},
		{
			name: "test",
			args: args{
				inValue:  "",
				defValue: "defvalue",
			},
			want: "defvalue",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := IfEmptyThen(tt.args.inValue, tt.args.defValue, tt.args.ifNotEmptyValues...); got != tt.want {
				t.Errorf("IfEmptyThen() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestStartsWith(t *testing.T) {
	type args struct {
		value      string
		startsWith []string
	}
	tests := []struct {
		name       string
		args       args
		wantResult bool
	}{
		{
			name:       "test-true",
			args:       args{value: "prefix1something", startsWith: []string{"prefix1"}},
			wantResult: true,
		},
		{
			name:       "test-true",
			args:       args{value: "prefix1something", startsWith: []string{"prefix2", "prefix1"}},
			wantResult: true,
		},
		{
			name:       "test-false",
			args:       args{value: "prefix1something", startsWith: []string{""}},
			wantResult: false,
		},
		{
			name:       "test-false",
			args:       args{value: "prefix1something", startsWith: []string{"prefix2"}},
			wantResult: false,
		},
		{
			name:       "test-false",
			args:       args{value: "prefix1something", startsWith: []string{"prefix2", "prefix3"}},
			wantResult: false,
		},
		{
			name:       "test-false",
			args:       args{value: "prefix1something", startsWith: []string{}},
			wantResult: false,
		},
		{
			name:       "test-false",
			args:       args{value: "1prefix1something", startsWith: []string{"prefix1"}},
			wantResult: false,
		},
		{
			name:       "test-false",
			args:       args{value: "", startsWith: []string{""}},
			wantResult: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotResult := StartsWith(tt.args.value, tt.args.startsWith); gotResult != tt.wantResult {
				t.Errorf("StartsWith() = %v, want %v", gotResult, tt.wantResult)
			}
		})
	}
}

func TestReverse(t *testing.T) {
	type args struct {
		value string
	}
	tests := []struct {
		name       string
		args       args
		wantResult string
	}{
		{args: args{value: "123456789"}, wantResult: "987654321"},
		{args: args{value: "some string"}, wantResult: "gnirts emos"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotResult := Reverse(tt.args.value); gotResult != tt.wantResult {
				t.Errorf("Reverse() = %v, want %v", gotResult, tt.wantResult)
			}
		})
	}
}

func TestIndent(t *testing.T) {
	tests := []struct {
		name       string
		value      string
		with       string
		wantResult string
	}{
		{
			name:       "test-multi",
			value:      "multi\nline\nstring",
			with:       "  ",
			wantResult: "  multi\n  line\n  string",
		},
		{
			name:       "test-single",
			value:      "single",
			with:       "  ",
			wantResult: "  single",
		},
		{
			name:       "test-single-lineend",
			value:      "singlewithnewline\n",
			with:       "  ",
			wantResult: "  singlewithnewline\n",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotResult := Indent(tt.value, tt.with); gotResult != tt.wantResult {
				t.Errorf("Indent() = %v, want %v", gotResult, tt.wantResult)
			}
		})
	}
}

func TestParseString(t *testing.T) {
	assert.Equal(t, "fallback", Parse(nil, "fallback"))
	assert.Equal(t, "fallback", Parse(ptrs.PtrInt(12), "fallback"))
	assert.Equal(t, "value", Parse(ptrs.PtrString("value"), "fallback"))
	assert.Equal(t, "value", Parse("value", "fallback"))
	var ptrString *string
	assert.Equal(t, "fallback", Parse(ptrString, "fallback"))
}

func TestIfNotEmptyThen(t *testing.T) {

	tests := []struct {
		name          string
		inValue       string
		trueValue     string
		ifEmptyValues []string
		want          string
	}{
		{
			name:      "test-empty",
			inValue:   "",
			trueValue: "want this",
			want:      "",
		},
		{
			name:          "test-empty",
			inValue:       "",
			trueValue:     "want this",
			ifEmptyValues: []string{"nay", "nay"},
			want:          "naynay",
		},
		{
			name:      "test-not-empty",
			inValue:   "something",
			trueValue: "want this",
			want:      "want this",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := IfNotEmptyThen(tt.inValue, tt.trueValue, tt.ifEmptyValues...); got != tt.want {
				t.Errorf("IfNotEmptyThen() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTruncate(t *testing.T) {
	type args struct {
		value     string
		maxLength int
		addSuffix []string
	}
	tests := []struct {
		name       string
		args       args
		wantResult string
	}{
		{
			args: args{
				value:     "1234567890",
				maxLength: 5,
				addSuffix: nil,
			},
			wantResult: "12345",
		},
		{
			args: args{
				value:     "1234567890",
				maxLength: 10,
				addSuffix: nil,
			},
			wantResult: "1234567890",
		},
		{
			args: args{
				value:     "1234567890",
				maxLength: 10,
				addSuffix: []string{"..."},
			},
			wantResult: "1234567890",
		},
		{
			args: args{
				value:     "1234567890",
				maxLength: 10,
				addSuffix: []string{"..."},
			},
			wantResult: "1234567890",
		},
		{
			args: args{
				value:     "some text sort out space",
				maxLength: 4,
				addSuffix: []string{"..."},
			},
			wantResult: "some...",
		},
		{
			args: args{
				value:     "some text sort out space",
				maxLength: 5,
				addSuffix: []string{"..."},
			},
			wantResult: "some...",
		},
		{
			args: args{
				value:     "some text sort out space",
				maxLength: 5,
				addSuffix: []string{"...", "end"},
			},
			wantResult: "some...end",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotResult := Truncate(tt.args.value, tt.args.maxLength, tt.args.addSuffix...); gotResult != tt.wantResult {
				t.Errorf("Truncate() = %v, want %v", gotResult, tt.wantResult)
			}
		})
	}
}

func TestUnParse(t *testing.T) {
	tests := []struct {
		name       string
		in         string
		wantResult interface{}
	}{
		{name: "test-bool", in: "true", wantResult: true},
		{name: "test-bool", in: "false", wantResult: false},
		{name: "test-bool", in: "TRUE", wantResult: true},
		{name: "test-int", in: "0", wantResult: 0},
		{name: "test-int", in: "1", wantResult: 1},
		{name: "test-int", in: "-100", wantResult: -100},
		{name: "test-float", in: "0.0", wantResult: 0.0},
		{name: "test-float", in: "1.0", wantResult: 1.0},
		{name: "test-float", in: ".0", wantResult: 0.0},
		{name: "test-float", in: "3.14159", wantResult: 3.14159},
		{name: "test-float", in: "-3.14159", wantResult: -3.14159},
		{name: "test-float", in: "9.518e23", wantResult: 9.518e+23},
		{name: "test-string", in: "Test String", wantResult: "Test String"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotResult := UnParse(tt.in); !reflect.DeepEqual(gotResult, tt.wantResult) {
				t.Errorf("Unparse() = %v, want %v", gotResult, tt.wantResult)
			}
		})
	}
}

func TestTruncateAndPadLeft(t *testing.T) {
	type args struct {
	}
	tests := []struct {
		name       string
		value      string
		length     int
		wantResult string
	}{
		{
			value:      "some longer text",
			length:     20,
			wantResult: "    some longer text",
		},
		{
			value:      "some longer text",
			length:     10,
			wantResult: "some l ...",
		},
		{
			value:      "some longer text",
			length:     8,
			wantResult: "some ...",
		},
		{
			value:      "some longer text ",
			length:     16,
			wantResult: " some longer ...",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotResult := TruncateAndPadLeft(tt.value, tt.length); gotResult != tt.wantResult {
				t.Errorf("TruncateAndPadLeft() = %v, want %v", gotResult, tt.wantResult)
			}
		})
	}
}

func TestJoinNotEmpty(t *testing.T) {
	type args struct {
		elems []string
		sep   string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "test-full",
			args: args{
				elems: []string{"first", "second", "third"},
				sep:   ";",
			},
			want: "first;second;third",
		},
		{
			name: "test-half-empty",
			args: args{
				elems: []string{"first", "", "third"},
				sep:   ";",
			},
			want: "first;third",
		},
		{
			name: "test-empty",
			args: args{
				elems: []string{"", "", ""},
				sep:   "",
			},
			want: "",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := JoinNotEmpty(tt.args.elems, tt.args.sep); got != tt.want {
				t.Errorf("JoinNotEmpty() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPlural(t *testing.T) {
	type args struct {
		i    int
		unit string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "singular",
			args: args{
				i:    1,
				unit: "minute",
			},
			want: "1 minute",
		},
		{
			name: "plural",
			args: args{
				i:    10,
				unit: "hour",
			},
			want: "10 hours",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Plural(tt.args.i, tt.args.unit); got != tt.want {
				t.Errorf("formatUnit() = %v, want %v", got, tt.want)
			}
		})
	}
}
