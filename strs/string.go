package strs

import (
	"fmt"
	"gitlab.com/pgmtc-lib/commons/arrays"
	"gitlab.com/pgmtc-lib/commons/bools"
	"math/rand"
	"strconv"
	"strings"
	"time"
)

// RandomString returns randomly generated string
func RandomString(length int) string {
	var letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
	return RandomStringFromLetters(length, letters)
}

// RandomStringFromLetters returns randomly generated string from provided letters
func RandomStringFromLetters(length int, letters []rune) string {
	rand.Seed(time.Now().UnixNano())
	b := make([]rune, length)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}

// IfEmptyThen returns first parameter if not empty, otherwise default value
func IfEmptyThen(inValue, defValue string, ifNotEmptyValues ...string) string {
	if inValue == "" {
		return defValue
	}
	if len(ifNotEmptyValues) > 0 {
		return strings.Join(ifNotEmptyValues, "")
	}
	return inValue
}

// IfNotEmptyThen returns trueValue if inValue is not empty
func IfNotEmptyThen(inValue, trueValue string, ifEmptyValues ...string) string {
	if inValue == "" {
		if len(ifEmptyValues) > 0 {
			return strings.Join(ifEmptyValues, "")
		}
		return inValue
	}
	return trueValue
}

// StartsWith returns true if value starts with one of in startsWith array
func StartsWith(value string, startsWith []string) (result bool) {
	for _, item := range startsWith {
		if item == "" {
			continue
		}
		if strings.HasPrefix(value, item) {
			return true
		}
	}
	return false
}

// Reverse reverses string
func Reverse(value string) (result string) {
	runes := []rune(value)
	for i, j := 0, len(runes)-1; i < j; i, j = i+1, j-1 {
		runes[i], runes[j] = runes[j], runes[i]
	}
	return string(runes)
}

// Indent indents each line in the string using with parameter
func Indent(value string, with string) (result string) {
	result = with + strings.ReplaceAll(value, "\n", "\n"+with)
	if strings.HasSuffix(result, "\n"+with) {
		result = strings.TrimSuffix(result, with)
	}
	return
}

// Parse returns string behind interface pointer
func Parse(in interface{}, ifEmptyThen string) (result string) {
	defer func() { recover() }()
	result = ifEmptyThen
	if _, ok := in.(string); ok {
		return in.(string)
	}
	if _, ok := in.(*string); ok {
		return *(in.(*string))
	}
	return ifEmptyThen
}

// Truncate truncates string and adds optional suffix
func Truncate(value string, maxLength int, addSuffix ...string) (result string) {
	result = fmt.Sprintf("%."+strconv.Itoa(maxLength)+"s", value)
	if len(value) > maxLength {
		for _, sfx := range addSuffix {
			result = strings.TrimSuffix(result, " ") + sfx
		}
	}
	return
}

// UnParse tries to decode string value into bool, int or float64. If none works, it returns original string
func UnParse(value string) (result interface{}) {
	lowerValue := strings.ToLower(value)
	if lowerValue == "false" || lowerValue == "true" {
		return bools.Parse(lowerValue, false)
	}
	if intValue, err := strconv.Atoi(lowerValue); err == nil {
		return intValue
	}
	if floatValue, err := strconv.ParseFloat(lowerValue, 64); err == nil {
		return floatValue
	}
	return value

}

// TruncateAndPadLeft truncates the string to max length and for shorter values, it pads by spaces from the left
func TruncateAndPadLeft(value string, length int) (result string) {
	truncated := value
	if len(value) > length {
		truncated = Truncate(value, length-4, " ...")
	}
	format := "%" + strconv.Itoa(length) + "s"
	result = fmt.Sprintf(format, truncated)
	return
}

// JoinNotEmpty does what strings.Join except it filters out empty values before joining
func JoinNotEmpty(elems []string, sep string) string {
	return strings.Join(arrays.FilterOutEmpty(elems), sep)
}

// Plural returns unit with s at the end (if more than 1)
func Plural(i int, unit string) string {
	if i == 1 {
		return fmt.Sprintf("%d %s", i, unit)
	}
	return fmt.Sprintf("%d %ss", i, unit)
}
