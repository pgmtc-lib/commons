package times

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"reflect"
	"testing"
	"time"
)

func TestTrimTime(t *testing.T) {
	now := time.Now().UTC()
	tests := []struct {
		name string
		in   time.Time
		want time.Time
	}{
		{
			name: "test-now",
			in:   now,
			want: time.Date(now.Year(), now.Month(), now.Day(), 0, 0, 0, 0, time.UTC),
		},
		{
			name: "test-provided",
			in:   time.Date(2020, 1, 1, 1, 1, 1, 1, time.UTC),
			want: time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := TrimTime(tt.in); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("TrimTime() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestToday(t *testing.T) {
	now := time.Now().UTC()
	today := Today()
	assert.Equal(t, now.Year(), today.Year())
	assert.Equal(t, now.Month(), today.Month())
	assert.Equal(t, now.Day(), today.Day())
	assert.Equal(t, 0, today.Hour())
	assert.Equal(t, 0, today.Minute())
	assert.Equal(t, 0, today.Second())
}

func TestTomorrow(t *testing.T) {
	tomorrowNow := time.Now().Add(24 * time.Hour).UTC()
	tomorrow := Tomorrow()
	assert.Equal(t, tomorrowNow.Year(), tomorrow.Year())
	assert.Equal(t, tomorrowNow.Month(), tomorrow.Month())
	assert.Equal(t, tomorrowNow.Day(), tomorrow.Day())
	assert.Equal(t, 0, tomorrow.Hour())
	assert.Equal(t, 0, tomorrow.Minute())
	assert.Equal(t, 0, tomorrow.Second())
}

func TestNextHour(t *testing.T) {
	now := time.Now().UTC()
	expected := time.Date(now.Year(), now.Month(), now.Day(), now.Hour(), 0, 0, 0, time.UTC).Add(1 * time.Hour)
	nextHour := NextHour()
	assert.Equal(t, expected.String(), nextHour.String())
}

func TestNextMinute(t *testing.T) {
	now := time.Now().UTC()
	expected := time.Date(now.Year(), now.Month(), now.Day(), now.Hour(), now.Minute(), 0, 0, time.UTC).Add(1 * time.Minute)
	nextMinute := NextMinute()
	assert.Equal(t, expected.String(), nextMinute.String())
}

func TestMax(t *testing.T) {
	type args struct {
		items []time.Time
	}
	tests := []struct {
		name       string
		args       args
		wantResult time.Time
	}{
		{
			name: "test",
			args: args{items: []time.Time{
				time.Date(2020, 3, 1, 0, 0, 0, 0, time.UTC),
				time.Date(2020, 4, 1, 0, 0, 0, 0, time.UTC),
				time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC),
				time.Date(2020, 2, 1, 0, 0, 0, 0, time.UTC),
			}},
			wantResult: time.Date(2020, 4, 1, 0, 0, 0, 0, time.UTC),
		},
		{
			name:       "test-empty",
			args:       args{},
			wantResult: time.Time{},
		},
		{
			name: "test-one",
			args: args{items: []time.Time{
				time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC),
			}},
			wantResult: time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotResult := Max(tt.args.items...); !reflect.DeepEqual(gotResult, tt.wantResult) {
				t.Errorf("Max() = %v, want %v", gotResult, tt.wantResult)
			}
		})
	}
}

func TestMin(t *testing.T) {
	type args struct {
		items []time.Time
	}
	tests := []struct {
		name       string
		args       args
		wantResult time.Time
	}{
		{
			name: "test",
			args: args{items: []time.Time{
				time.Date(2020, 3, 1, 0, 0, 0, 0, time.UTC),
				time.Date(2020, 4, 1, 0, 0, 0, 0, time.UTC),
				time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC),
				time.Date(2020, 2, 1, 0, 0, 0, 0, time.UTC),
			}},
			wantResult: time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC),
		},
		{
			name:       "test-empty",
			args:       args{},
			wantResult: time.Time{},
		},
		{
			name: "test-one",
			args: args{items: []time.Time{
				time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC),
			}},
			wantResult: time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotResult := Min(tt.args.items...); !reflect.DeepEqual(gotResult, tt.wantResult) {
				t.Errorf("Min() = %v, want %v", gotResult, tt.wantResult)
			}
		})
	}
}

func TestBeginningOfDay(t *testing.T) {
	testZone := time.FixedZone("test", 2*3600)
	tests := []struct {
		name       string
		args       time.Time
		wantBOD    time.Time
		wantEOD    time.Time
		wantMidday time.Time
	}{
		{
			name:       "test-utc",
			args:       time.Date(2020, 1, 1, 1, 2, 3, 4, time.UTC),
			wantBOD:    time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC),
			wantEOD:    time.Date(2020, 1, 2, 0, 0, 0, 0, time.UTC),
			wantMidday: time.Date(2020, 1, 1, 12, 0, 0, 0, time.UTC),
		},
		{
			name:       "test-tz",
			args:       time.Date(2020, 1, 1, 1, 2, 3, 4, testZone),
			wantBOD:    time.Date(2020, 1, 1, 0, 0, 0, 0, testZone),
			wantEOD:    time.Date(2020, 1, 2, 0, 0, 0, 0, testZone),
			wantMidday: time.Date(2020, 1, 1, 12, 0, 0, 0, testZone),
		},
		{
			name:       "test-tz-in-utc",
			args:       time.Date(2020, 1, 1, 1, 2, 3, 4, testZone).UTC(),
			wantBOD:    time.Date(2019, 12, 31, 0, 0, 0, 0, time.UTC),
			wantEOD:    time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC),
			wantMidday: time.Date(2019, 12, 31, 12, 0, 0, 0, time.UTC),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equal(t, tt.wantBOD, BOD(tt.args))
			assert.Equal(t, tt.wantEOD, EOD(tt.args))
			assert.Equal(t, tt.wantMidday, MOD(tt.args))

		})
	}
}

func TestSameWeek(t *testing.T) {
	testZone := time.FixedZone("test", 2*3600)
	tests := []struct {
		name   string
		value1 time.Time
		value2 time.Time
		want   bool
	}{
		{
			name:   "same-week",
			value1: time.Date(2022, 6, 13, 0, 0, 0, 0, time.UTC), // Monday
			value2: time.Date(2022, 6, 19, 0, 0, 0, 0, time.UTC), // Sunday
			want:   true,
		},
		{
			name:   "same-day",
			value1: time.Date(2022, 6, 13, 0, 0, 0, 0, time.UTC),
			value2: time.Date(2022, 6, 13, 0, 0, 0, 0, time.UTC),
			want:   true,
		},
		{
			name:   "timezone-test",
			value1: time.Date(2022, 6, 12, 22, 0, 0, 0, time.UTC), // monday midnight in utc
			value2: time.Date(2022, 6, 13, 0, 0, 0, 0, testZone),  // in utc - 2 hours before monday midnight
			want:   false,
		},
		{
			name:   "timezone-test",
			value1: time.Date(2022, 6, 13, 0, 0, 0, 0, time.UTC), // monday midnight in utc
			value2: time.Date(2022, 6, 13, 2, 0, 0, 0, testZone), // in utc - 2 hours before monday midnight
			want:   true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := IsSameWeek(tt.value1, tt.value2); got != tt.want {
				t.Errorf("IsSameWeek() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestWeekdayFromMonday(t *testing.T) {
	testZone := time.FixedZone("test", 2*3600)
	tests := []struct {
		name       string
		t          time.Time
		wantResult int
	}{
		{
			t:          time.Date(2022, 6, 16, 0, 0, 0, 0, time.UTC), // thursday
			wantResult: 3,
		},
		{
			t:          time.Date(2022, 6, 16, 0, 0, 0, 0, testZone), // thursday
			wantResult: 3,
		},
		{
			t:          time.Date(2022, 6, 16, 0, 0, 0, 0, testZone).UTC(), // wednesday
			wantResult: 2,
		},
		{
			t:          time.Date(2022, 6, 19, 0, 0, 0, 0, time.UTC), // sunday
			wantResult: 6,
		},
		{
			t:          time.Date(2022, 6, 13, 0, 0, 0, 0, time.UTC), // sunday
			wantResult: 0,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotResult := WeekdayFromMonday(tt.t); gotResult != tt.wantResult {
				t.Errorf("WeekdayFromMonday() = %v, want %v", gotResult, tt.wantResult)
			}
		})
	}
}

func TestBOW(t *testing.T) {
	testZone := time.FixedZone("test", 2*3600)
	tests := []struct {
		name string
		t    time.Time
		want time.Time
	}{
		{
			name: "test-utc",
			t:    time.Date(2022, 6, 16, 12, 30, 0, 0, time.UTC),
			want: time.Date(2022, 6, 13, 0, 0, 0, 0, time.UTC),
		},
		{
			name: "test-zone",
			t:    time.Date(2022, 6, 16, 12, 30, 0, 0, testZone),
			want: time.Date(2022, 6, 13, 0, 0, 0, 0, testZone),
		},
		{
			name: "test-mixed",
			t:    time.Date(2022, 6, 16, 12, 30, 0, 0, testZone),
			want: time.Date(2022, 6, 12, 22, 0, 0, 0, time.UTC).In(testZone),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := BOW(tt.t); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("BOW() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMonthBoundaries(t *testing.T) {
	tests := []struct {
		value            time.Time
		wantBegLastMonth time.Time
		wantBegCurMonth  time.Time
		wantBegNextMonth time.Time
	}{
		{
			value:            time.Date(2020, 1, 8, 0, 0, 0, 0, time.UTC),
			wantBegLastMonth: time.Date(2019, 12, 1, 0, 0, 0, 0, time.UTC),
			wantBegCurMonth:  time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC),
			wantBegNextMonth: time.Date(2020, 2, 1, 0, 0, 0, 0, time.UTC),
		},
		{
			value:            time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC),
			wantBegLastMonth: time.Date(2019, 12, 1, 0, 0, 0, 0, time.UTC),
			wantBegCurMonth:  time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC),
			wantBegNextMonth: time.Date(2020, 2, 1, 0, 0, 0, 0, time.UTC),
		},
		{
			value:            time.Date(2020, 2, 28, 0, 0, 0, 0, time.UTC),
			wantBegLastMonth: time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC),
			wantBegCurMonth:  time.Date(2020, 2, 1, 0, 0, 0, 0, time.UTC),
			wantBegNextMonth: time.Date(2020, 3, 1, 0, 0, 0, 0, time.UTC),
		},
	}
	for idx, tt := range tests {
		t.Run(fmt.Sprintf("%d", idx), func(t *testing.T) {
			gotBegLastMonth, gotBegCurMonth, gotBegNextMonth := MonthBoundaries(tt.value)
			if !reflect.DeepEqual(gotBegLastMonth, tt.wantBegLastMonth) {
				t.Errorf("MonthBoundaries() gotBegLastMonth = %v, want %v", gotBegLastMonth, tt.wantBegLastMonth)
			}
			if !reflect.DeepEqual(gotBegCurMonth, tt.wantBegCurMonth) {
				t.Errorf("MonthBoundaries() gotBegCurMonth = %v, want %v", gotBegCurMonth, tt.wantBegCurMonth)
			}
			if !reflect.DeepEqual(gotBegNextMonth, tt.wantBegNextMonth) {
				t.Errorf("MonthBoundaries() gotBegNextMonth = %v, want %v", gotBegNextMonth, tt.wantBegNextMonth)
			}
		})
	}
}

func TestIsSameDayTime(t *testing.T) {
	tests := []struct {
		name   string
		value  time.Time
		value2 time.Time
		want   bool
	}{
		{
			value:  time.Date(2020, 1, 1, 12, 5, 2, 0, time.UTC),
			value2: time.Date(2020, 1, 1, 12, 5, 2, 0, time.UTC),
			want:   true,
		},
		{
			value:  time.Date(2021, 1, 1, 12, 5, 2, 0, time.UTC),
			value2: time.Date(2020, 1, 1, 12, 5, 2, 0, time.UTC),
			want:   true,
		},
		{
			value:  time.Date(2021, 2, 3, 12, 5, 2, 0, time.UTC),
			value2: time.Date(2020, 1, 1, 12, 5, 2, 0, time.UTC),
			want:   true,
		},
		{
			value:  time.Date(2020, 1, 1, 13, 5, 2, 0, time.UTC),
			value2: time.Date(2020, 1, 1, 12, 5, 2, 0, time.UTC),
			want:   false,
		},
		{
			value:  time.Date(2020, 1, 1, 12, 6, 2, 0, time.UTC),
			value2: time.Date(2020, 1, 1, 12, 5, 2, 0, time.UTC),
			want:   false,
		},
		{
			value:  time.Date(2020, 1, 1, 12, 5, 3, 0, time.UTC),
			value2: time.Date(2020, 1, 1, 12, 5, 2, 0, time.UTC),
			want:   false,
		},
		{
			value:  time.Date(2020, 1, 1, 12, 5, 2, 1, time.UTC),
			value2: time.Date(2020, 1, 1, 12, 5, 2, 0, time.UTC),
			want:   true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equalf(t, tt.want, IsSameDayTime(tt.value, tt.value2), "IsSameTime(%v, %v)", tt.value, tt.value2)
		})
	}
}
