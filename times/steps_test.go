package times

import (
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestTimes_SortAndReverse(t *testing.T) {
	in := Times{
		time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC),
		time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC),
		time.Date(2023, 1, 1, 0, 0, 0, 0, time.UTC),
	}
	expected := Times{
		time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC),
		time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC),
		time.Date(2023, 1, 1, 0, 0, 0, 0, time.UTC),
	}
	assert.Equal(t, expected, in.Sort())
	expected = Times{
		time.Date(2023, 1, 1, 0, 0, 0, 0, time.UTC),
		time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC),
		time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC),
	}
	assert.Equal(t, expected, in.Reverse())
}

func TestSteps(t *testing.T) {
	got := Steps(time.Date(2020, 1, 15, 0, 0, 0, 0, time.UTC), 48*time.Hour, 10) // 2 days
	expected := Times{
		time.Date(2020, 1, 15, 0, 0, 0, 0, time.UTC),
		time.Date(2020, 1, 17, 0, 0, 0, 0, time.UTC),
		time.Date(2020, 1, 19, 0, 0, 0, 0, time.UTC),
		time.Date(2020, 1, 21, 0, 0, 0, 0, time.UTC),
		time.Date(2020, 1, 23, 0, 0, 0, 0, time.UTC),
		time.Date(2020, 1, 25, 0, 0, 0, 0, time.UTC),
		time.Date(2020, 1, 27, 0, 0, 0, 0, time.UTC),
		time.Date(2020, 1, 29, 0, 0, 0, 0, time.UTC),
		time.Date(2020, 1, 31, 0, 0, 0, 0, time.UTC),
		time.Date(2020, 2, 2, 0, 0, 0, 0, time.UTC),
	}
	assert.Equal(t, expected, got)
}
