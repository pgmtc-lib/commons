package times

import (
	"reflect"
	"testing"
	"time"
)

func TestNewDate(t *testing.T) {
	tests := []struct {
		name     string
		elements []int
		want     time.Time
	}{
		{
			elements: []int{2020, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			want:     time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC),
		},
		{
			elements: []int{2020, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
			want:     time.Date(2020, 1, 1, 1, 1, 1, 1, time.UTC),
		},
		{
			elements: []int{2020},
			want:     time.Date(2020, 0, 0, 0, 0, 0, 0, time.UTC),
		},
		{
			elements: []int{2020, 12},
			want:     time.Date(2020, 12, 0, 0, 0, 0, 0, time.UTC),
		},
		{
			elements: []int{},
			want:     time.Date(0, 0, 0, 0, 0, 0, 0, time.UTC),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewDate(tt.elements...); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewDate() = %v, want %v", got, tt.want)
			}
		})
	}
}
