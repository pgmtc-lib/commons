package times

import (
	"gitlab.com/pgmtc-lib/commons/ptrs"
	"testing"
	"time"
)

func TestFormatISO(t *testing.T) {
	type args struct {
		value time.Time
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "test",
			args: args{value: time.Date(2020, 1, 1, 15, 45, 50, 500000000, time.UTC)},
			want: "2020-01-01T15:45:50.500Z",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := FormatISO(tt.args.value); got != tt.want {
				t.Errorf("FormatISO() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFormatShort(t *testing.T) {
	type args struct {
		value time.Time
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "test",
			args: args{value: time.Date(2020, 1, 1, 15, 45, 50, 500000000, time.UTC)},
			want: "01/01T15:45:50",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := FormatShort(tt.args.value); got != tt.want {
				t.Errorf("FormatShort() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFormatDateTimeLocal(t *testing.T) {
	type args struct {
		value time.Time
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "test",
			args: args{value: time.Date(2020, 12, 1, 15, 45, 50, 500000000, time.Local)},
			want: "2020-12-01 15:45:50",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := FormatDateTimeLocal(tt.args.value); got != tt.want {
				t.Errorf("FormatShort() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFormatDurrationApprox(t *testing.T) {
	tests := []struct {
		name         string
		diff         time.Duration
		whenNegative string
		wantResult   string
	}{
		{
			name:         "negative",
			diff:         -1 * time.Hour,
			whenNegative: "sometimes in the past",
			wantResult:   "sometimes in the past",
		},
		{
			name:       "minutes",
			diff:       35 * time.Minute,
			wantResult: "35 minutes",
		},
		{
			name:       "hours",
			diff:       4 * time.Hour,
			wantResult: "4 hours",
		},
		{
			name:       "days",
			diff:       28 * 24 * time.Hour,
			wantResult: "28 days",
		},
		{
			name:       "future",
			diff:       61 * 24 * time.Hour,
			wantResult: "61 days",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotResult := FormatDurationApprox(tt.diff, tt.whenNegative); gotResult != tt.wantResult {
				t.Errorf("formatDiff() = %v, want %v", gotResult, tt.wantResult)
			}
		})
	}
}

func TestFormatFromNow(t *testing.T) {
	tests := []struct {
		name   string
		in     *time.Time
		future bool
		want   string
	}{
		{
			name:   "test-days",
			in:     ptrs.PtrTime(time.Now().UTC().Add(76 * time.Hour).Add(15 * time.Minute).Add(20 * time.Second)),
			future: true,
			want:   "3d4h",
		},
		{
			name:   "test-hours",
			in:     ptrs.PtrTime(time.Now().UTC().Add(5 * time.Hour).Add(15 * time.Minute).Add(20 * time.Second)),
			future: true,
			want:   "5h15m",
		},
		{
			name:   "test-minutes",
			in:     ptrs.PtrTime(time.Now().UTC().Add(15 * time.Minute).Add(20 * time.Second)),
			future: true,
			want:   "0h15m",
		},
		{
			name:   "test-seconds",
			in:     ptrs.PtrTime(time.Now().UTC().Add(20 * time.Second)),
			future: true,
			want:   "20s",
		},
		{
			name:   "test-past",
			in:     ptrs.PtrTime(time.Now().UTC().Add(-5 * time.Hour).Add(15 * time.Minute).Add(20 * time.Second)),
			future: true,
			want:   "4h44m",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := FormatFromNow(tt.in, tt.future); got != tt.want {
				t.Errorf("FormatFromNow() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFormat(t *testing.T) {
	tests := []struct {
		name   string
		t      time.Time
		layout string
		want   string
	}{
		{
			name:   "test-pass-through",
			t:      time.Date(2020, 1, 2, 3, 4, 5, 6, time.UTC),
			layout: "Mon, 2 Jan 2006 15:04:05 -0700",
			want:   "Thu, 2 Jan 2020 03:04:05 +0000",
		},
		{
			name:   "test-apply",
			t:      time.Date(2020, 1, 3, 3, 4, 5, 6, time.UTC),
			layout: "Mon, 2nd Jan 2006 15:04:05 -0700",
			want:   "Fri, 3rd Jan 2020 03:04:05 +0000",
		},
		{
			name:   "test-apply",
			t:      time.Date(2020, 1, 3, 3, 4, 5, 6, time.UTC),
			layout: "2nd January (2nd Jan)",
			want:   "3rd January (3rd Jan)",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Format(tt.t, tt.layout); got != tt.want {
				t.Errorf("Format() = %v, want %v", got, tt.want)
			}
		})
	}
}
