package times

import (
	"time"
)

// TrimTime removes time bit from the time object (returns a date only)
func TrimTime(val time.Time) time.Time {
	return val.Truncate(24 * time.Hour)
}

// Today returns date for today with time trimmed
func Today() time.Time {
	return TrimTime(time.Now().UTC())
}

// Tomorrow returns date for tomorrow with time trimmed
func Tomorrow() time.Time {
	return Today().Add(24 * time.Hour)
}

// NextHour returns next full hour
func NextHour() time.Time {
	return time.Now().Add(1 * time.Hour).Truncate(time.Hour).UTC()
}

// NextMinute returns next full minute
func NextMinute() time.Time {
	return time.Now().Add(1 * time.Minute).Truncate(time.Minute).UTC()
}

// Max returns time which is latest from the two
func Max(items ...time.Time) (result time.Time) {
	if len(items) == 0 {
		return
	}
	result = items[0]
	for _, item := range items {
		if item.After(result) {
			result = item
		}
	}
	return
}

// Min returns time which is latest from the two
func Min(items ...time.Time) (result time.Time) {
	if len(items) == 0 {
		return
	}
	result = items[0]
	for _, item := range items {
		if item.Before(result) {
			result = item
		}
	}
	return
}

// WeekdayFromMonday returns week day in European format (Monday = 0 .... 6 = Sunday)
func WeekdayFromMonday(t time.Time) (result int) {
	if result = int(t.Weekday()) - 1; result == -1 {
		result = 6
	}
	return
}

// BOD returns previous midnight in the same timezone as provided time
func BOD(t time.Time) time.Time {
	year, month, day := t.Date()
	return time.Date(year, month, day, 0, 0, 0, 0, t.Location())
}

// EOD returns next midnight in the same timezone as provided time
func EOD(t time.Time) time.Time {
	return BOD(t).Add(24 * time.Hour)
}

// MOD returns midday in the same time zone as provided time
func MOD(t time.Time) time.Time {
	return BOD(t).Add(12 * time.Hour)
}

// BOW returns beginning of the week (from Monday) in the same timezone
func BOW(t time.Time) time.Time {
	year, month, day := BOD(t).Date()
	return time.Date(year, month, day-WeekdayFromMonday(t), 0, 0, 0, 0, t.Location())
}

// IsSameWeek returns true if the two values fall into the same week
func IsSameWeek(value1, value2 time.Time) bool {
	val1Week, val1Year := value1.ISOWeek()
	val2Week, val2Year := value2.ISOWeek()
	return val1Week == val2Week && val1Year == val2Year
}

// IsSameDayTime returns true if the time of the day is the same
func IsSameDayTime(value, value2 time.Time) bool {
	hour1, minute1, second1 := value.UTC().Hour(), value.UTC().Minute(), value.UTC().Second()
	hour2, minute2, second2 := value2.UTC().Hour(), value2.UTC().Minute(), value2.UTC().Second()
	return hour1 == hour2 && minute1 == minute2 && second1 == second2

}

// MonthBoundaries returns first day of previous, current and next month
func MonthBoundaries(value time.Time) (begLastMonth, begCurMonth, begNextMonth time.Time) {
	midMonth := time.Date(value.Year(), value.Month(), 15, 0, 0, 0, 0, time.UTC)
	lastMidMonth := midMonth.AddDate(0, -1, 0) // last month, 15th
	nextMidMonth := midMonth.AddDate(0, 1, 0)  // next month, 15th
	begCurMonth = time.Date(midMonth.Year(), midMonth.Month(), 1, 0, 0, 0, 0, time.UTC)
	begLastMonth = time.Date(lastMidMonth.Year(), lastMidMonth.Month(), 1, 0, 0, 0, 0, time.UTC)
	begNextMonth = time.Date(nextMidMonth.Year(), nextMidMonth.Month(), 1, 0, 0, 0, 0, time.UTC)
	return
}
