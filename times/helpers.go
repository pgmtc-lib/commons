package times

import "time"

// NewDate creates new date with non-mandatory parameters
// times.NewDate(1,2,3) => time.Date(1,2,3,0,0,0,0,time.UTC)
func NewDate(elements ...int) time.Time {
	var args = make([]int, 7)
	copy(args, elements)
	return time.Date(args[0], time.Month(args[1]), args[2], args[3], args[4], args[5], args[6], time.UTC)
}
