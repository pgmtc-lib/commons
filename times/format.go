package times

import (
	"fmt"
	"gitlab.com/pgmtc-lib/commons/strs"
	"math"
	"strings"
	"time"
)

// FormatISO formats time to iso string
func FormatISO(value time.Time) string {
	return value.UTC().Format("2006-01-02T15:04:05.000Z")
}

// FormatShort formats time to shorter datetime string
func FormatShort(value time.Time) string {
	return value.UTC().Format("01/02T15:04:05")
}

// FormatDateTimeLocal formats time to datetime string
func FormatDateTimeLocal(value time.Time) string {
	return value.Local().Format("2006-01-02 15:04:05")
}

// FormatDurationApprox formats approximate human-readable difference such as 1 day or 13 hours, or 30 days
func FormatDurationApprox(diff time.Duration, whenNegative string) (result string) {
	switch {
	case diff < 0:
		return whenNegative
	case diff > 24*time.Hour:
		diffDays := int(diff.Round(time.Hour*24).Hours() / 24)
		return strs.Plural(diffDays, "day")
	case diff > 1*time.Hour:
		diffHours := int(diff.Round(time.Hour).Hours())
		return strs.Plural(diffHours, "hour")
	default:
		diffMinutes := int(diff.Round(time.Minute).Minutes())
		return strs.Plural(diffMinutes, "minute")
	}
}

// FormatFromNow formats time from now - similarly to kubectl
func FormatFromNow(in *time.Time, future bool) string {
	if in == nil {
		return ""
	}
	var d time.Duration
	timeNow := time.Now().UTC()
	if timeNow.After(*in) {
		d = timeNow.Sub(in.UTC()).Round(time.Second)
	} else {
		d = in.UTC().Sub(timeNow).Round(time.Second)
	}
	switch {
	case math.Abs(d.Minutes()) < 10:
		return d.String()
	case math.Abs(d.Hours()) < 24:
		minutes := int(d.Minutes()) - int(d.Hours())*60
		return fmt.Sprintf("%dh%dm", int(d.Hours()), minutes)
	default:
		hours := int(d.Hours()) - int(d.Hours()/24)*24
		return fmt.Sprintf("%dd%dh", int(d.Hours()/24), hours)
	}
}

// Format does standard golang formatting with addition of st,nd,rd after day number
// provide 2nd in your layout
//
//	example: times.Format(t, "2nd Jan 15:04:05")
func Format(t time.Time, layout string) string {
	if !strings.Contains(layout, "2nd") {
		return t.Format(layout)
	}
	suffix := "th"
	switch t.Day() {
	case 1, 21, 31:
		suffix = "st"
	case 2, 22:
		suffix = "nd"
	case 3, 23:
		suffix = "rd"
	}
	layout = strings.ReplaceAll(layout, "2nd", "2"+suffix)
	return t.Format(layout)
}
