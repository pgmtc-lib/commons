package times

import (
	"sort"
	"time"
)

// Times represents slice of time.Time
type Times []time.Time

// Sort sorts list of times ASC
func (t Times) Sort() Times {
	sort.Slice(t, func(i, j int) bool {
		return t[i].Before(t[j])
	})
	return t
}

// Reverse reverses list of times
func (t Times) Reverse() Times {
	for i, j := 0, len(t)-1; i < j; i, j = i+1, j-1 {
		t[i], t[j] = t[j], t[i]
	}
	return t
}

// Steps gets a start time and adds 'count' number of times divided by the interval
func Steps(t time.Time, interval time.Duration, count int) (result Times) {
	result = make(Times, count)
	for i := 0; i < count; i++ {
		result[i] = t.Add(time.Duration(i) * interval)
	}
	return
}
