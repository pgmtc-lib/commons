package serve

import (
	"fmt"
	"github.com/NYTimes/gziphandler"
	"gitlab.com/pgmtc-lib/commons/htutils"
	"gitlab.com/pgmtc-lib/commons/svc"
	"io/fs"
	"net/http"
	"os"
)

// Option is an option for static server
type Option func(s *staticServer)

// WithAddr is an option which sets the custom listener address
func WithAddr(addr string) Option {
	return func(s *staticServer) {
		s.addr = addr
	}
}

// WithContent is an option which sets content and it's subdir. Can be used with embed
//
//	//go:embed public-html
//	var content embed.FS
//	...
//	serve.Static(serve.WithContent(content, "public-html"))
func WithContent(content fs.FS, subPath string) Option {
	return func(s *staticServer) {
		if subPath == "" {
			s.serveFS = content
			return
		}
		var err error
		if s.serveFS, err = fs.Sub(content, subPath); err != nil {
			panic(err)
		}
	}
}

// WithExposedEnvVariables is an option which tells the static server to expose environment variable(s)
//
//	serve.Static(serve.WithExposedVariables("HOME", "SHELL", "API_ADDR")
func WithExposedEnvVariables(envVariable ...string) Option {
	return func(s *staticServer) {
		s.exposeEnvVariables = envVariable
	}
}

// WithSPA switches on SPA mode, which redirect 404 to index.html
//
//	serve.Static(serve.WithSPA)
func WithSPA(s *staticServer) {
	s.spaMode = true
}

// WithIntercept allows intercept flow with custom function
//
//	serve.Static(serve.WithIntercept(myHandlerFn))
func WithIntercept(intercept func(w http.ResponseWriter, r *http.Request) bool) Option {
	return func(s *staticServer) {
		s.interceptFn = intercept
	}
}

// WithCompression instructs server to gzip files
func WithCompression(s *staticServer) {
	s.compressFiles = true
}

type staticServer struct {
	addr               string
	exposeEnvVariables []string
	spaMode            bool
	serveFS            fs.FS
	interceptFn        func(w http.ResponseWriter, r *http.Request) bool
	compressFiles      bool
}

// Static creates static server which can expose env variables in /env handler. Returns svc.Runner
//
//		var server = serve.Static(
//		  WithAddr("localhost:7777"),
//		  WithContent(content, "test-html"),
//		  WithExposedEnvVariables("HOME", "SHELL"))
//	 err := server()
func Static(opts ...Option) svc.Runner {
	srv := newStatic(opts...)
	return func() (err error) {
		return http.ListenAndServe(srv.addr, srv.handler())
	}
}

// StaticHandler creates static server which can expose env variables in /env. Returns http.handler
func StaticHandler(opts ...Option) http.Handler {
	srv := newStatic(opts...)
	return srv.handler()
}

// newStatic instantiates the server
func newStatic(opts ...Option) staticServer {
	srv := staticServer{
		addr:    ":8080",       // default
		serveFS: os.DirFS("."), // default
	}
	for _, opt := range opts {
		opt(&srv)
	}
	return srv
}

func (s staticServer) handler() http.Handler {
	httpFS := http.FS(s.serveFS)
	mux := http.NewServeMux()
	mux.Handle("/", http.FileServer(httpFS))
	var spaIgnore []string
	if len(s.exposeEnvVariables) > 0 {
		mux.HandleFunc("/env", s.envHandler)
		mux.HandleFunc("/env.js", s.envHandlerJS)
		spaIgnore = append(spaIgnore, "/env", "/env.js")
	}
	var handler http.Handler = mux
	if s.compressFiles {
		handler = gziphandler.GzipHandler(mux)
	}
	handler = &interceptMiddleware{
		handler:     handler,
		interceptFn: s.interceptFn,
	}
	if s.spaMode {
		handler = &spaMiddleware{
			handler:     handler,
			fs:          httpFS,
			ignorePaths: spaIgnore,
		}
	}
	return handler
}

func (s staticServer) envHandler(w http.ResponseWriter, r *http.Request) {
	htutils.SendResponse(w, 200, toEnvMap(s.exposeEnvVariables))
}

func (s staticServer) envHandlerJS(w http.ResponseWriter, r *http.Request) {
	envMap := toEnvMap(s.exposeEnvVariables)
	w.Header().Add("Content-Type", "application/javascript;charset=UTF-8")
	_, _ = w.Write([]byte("window.env = new Map()\n"))
	for key, val := range envMap {
		_, _ = w.Write([]byte(fmt.Sprintf("window.env.set(%q, %q)\n", key, val)))
	}
}

func toEnvMap(envVariables []string) map[string]interface{} {
	envMap := make(map[string]interface{})
	for _, env := range envVariables {
		envMap[env] = os.Getenv(env)
	}
	return envMap
}
