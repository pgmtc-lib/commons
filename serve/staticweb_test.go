package serve

import (
	"embed"
	"encoding/json"
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/pgmtc-lib/commons/svc"
	"io/ioutil"
	"net/http"
	"os"
	"testing"
	"time"
)

//go:embed test-html
var content embed.FS

func TestStaticServer(t *testing.T) {
	var server svc.Runner = Static(
		WithAddr("localhost:7777"),
		WithContent(content, "test-html"),
		WithExposedEnvVariables("HOME", "SHELL"))
	// test that index comes back
	service := svc.New(server, svc.WithHTTPUpCheck("http://localhost:7777/", 1), svc.WithTimeout(1*time.Second))
	err := service.Start()
	assert.NoError(t, err)
	// test that index comes back
	resp, err := http.Get("http://localhost:7777")
	assert.NoError(t, err)
	body, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Equal(t, "this is an index", string(body))
	// test that subdir page comes back as well
	resp, err = http.Get("http://localhost:7777/subdir/page.html")
	assert.NoError(t, err)
	body, err = ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Equal(t, "this is a subdir page", string(body))
	// test that non-existing does not come back
	resp, err = http.Get("http://localhost:7777/spa")
	assert.NoError(t, err)
	body, err = ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Equal(t, 404, resp.StatusCode)
	assert.Equal(t, "404 page not found\n", string(body))
	// test that env does not come back with anything, unless exposed envs filled in
	resp, err = http.Get("http://localhost:7777/env")
	assert.NoError(t, err)
	body, err = ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.NotEmpty(t, string(body))
	// decode
	var env map[string]interface{}
	expectedEnv := map[string]interface{}{
		"HOME":  os.Getenv("HOME"),
		"SHELL": os.Getenv("SHELL"),
	}
	err = json.Unmarshal(body, &env)
	assert.Equal(t, expectedEnv, env)
	// test env.js
	resp, err = http.Get("http://localhost:7777/env.js")
	assert.NoError(t, err)
	body, err = ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.NotEmpty(t, string(body))
	expected1 := fmt.Sprintf("window.env = new Map()\nwindow.env.set(\"HOME\", %q)\nwindow.env.set(\"SHELL\", %q)\n", os.Getenv("HOME"), os.Getenv("SHELL"))
	expected2 := fmt.Sprintf("window.env = new Map()\nwindow.env.set(\"SHELL\", %q)\nwindow.env.set(\"HOME\", %q)\n", os.Getenv("SHELL"), os.Getenv("HOME"))
	assert.True(t, string(body) == expected1 || string(body) == expected2)
}

func TestStaticServer_SPA(t *testing.T) {
	var server svc.Runner = Static(
		WithAddr("localhost:7778"),
		WithContent(content, "test-html"),
		WithExposedEnvVariables("HOME", "SHELL"),
		WithSPA)
	// test that index comes back
	service := svc.New(server, svc.WithHTTPUpCheck("http://localhost:7778/", 1), svc.WithTimeout(1*time.Second))
	err := service.Start()
	assert.NoError(t, err)
	// test that index comes back
	resp, err := http.Get("http://localhost:7778")
	assert.NoError(t, err)
	body, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Equal(t, "this is an index", string(body))
	// test that subdir page comes back as well
	resp, err = http.Get("http://localhost:7778/subdir/page.html")
	assert.NoError(t, err)
	body, err = ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Equal(t, "this is a subdir page", string(body))
	// test that env does not come back with anything, unless exposed envs filled in
	resp, err = http.Get("http://localhost:7778/env")
	assert.NoError(t, err)
	body, err = ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.NotEmpty(t, string(body))
	// test that non-existing does come back as index
	resp, err = http.Get("http://localhost:7778/spa")
	assert.NoError(t, err)
	body, err = ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Equal(t, "this is an index", string(body))
	// test that it adds index.html, if ends with "/" for checking (not to redirect to the root if it is in a subfolder)
	resp, err = http.Get("http://localhost:7778/subdir/")
	assert.NoError(t, err)
	body, err = ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Equal(t, "this is an index of the subdir", string(body))
	// test that env does not come back with anything, unless exposed envs filled in
	resp, err = http.Get("http://localhost:7778/env")
	assert.NoError(t, err)
	body, err = ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.NotEmpty(t, string(body))
	// decode
	var env map[string]interface{}
	expectedEnv := map[string]interface{}{
		"HOME":  os.Getenv("HOME"),
		"SHELL": os.Getenv("SHELL"),
	}
	err = json.Unmarshal(body, &env)
	assert.Equal(t, expectedEnv, env)
	// test env.js
	resp, err = http.Get("http://localhost:7778/env.js")
	assert.NoError(t, err)
	body, err = ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.NotEmpty(t, string(body))
	expected := fmt.Sprintf("window.env = new Map()\nwindow.env.set(\"HOME\", %q)\nwindow.env.set(\"SHELL\", %q)\n", os.Getenv("HOME"), os.Getenv("SHELL"))
	assert.Equal(t, expected, string(body))
}

func TestStaticServer_Intercept(t *testing.T) {
	var interceptFn = func(w http.ResponseWriter, r *http.Request) bool {
		_, _ = w.Write([]byte("intercept"))
		return false
	}

	var server svc.Runner = Static(
		WithAddr("localhost:7779"),
		WithContent(content, "test-html"),
		WithExposedEnvVariables("HOME", "SHELL"),
		WithIntercept(interceptFn))

	// test that index comes back
	service := svc.New(server, svc.WithHTTPUpCheck("http://localhost:7779/", 1), svc.WithTimeout(1*time.Second))
	err := service.Start()
	assert.NoError(t, err)
	// test that intercept comes back and halts
	resp, err := http.Get("http://localhost:7779")
	assert.NoError(t, err)
	body, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Equal(t, "interceptthis is an index", string(body))
}

func TestStaticServer_Intercept2(t *testing.T) {
	var interceptFn = func(w http.ResponseWriter, r *http.Request) bool {
		_, _ = w.Write([]byte("intercept"))
		return true
	}

	var server svc.Runner = Static(
		WithAddr("localhost:7780"),
		WithContent(content, "test-html"),
		WithExposedEnvVariables("HOME", "SHELL"),
		WithIntercept(interceptFn))

	// test that index comes back
	service := svc.New(server, svc.WithHTTPUpCheck("http://localhost:7780/", 1), svc.WithTimeout(1*time.Second))
	err := service.Start()
	assert.NoError(t, err)
	// test that intercept comes back and halts
	resp, err := http.Get("http://localhost:7780")
	assert.NoError(t, err)
	body, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Equal(t, "intercept", string(body))
}
