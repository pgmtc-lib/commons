package serve

import (
	"gitlab.com/pgmtc-lib/commons/arrays"
	"net/http"
	"strings"
)

// spaMiddleware replaces missing files with index
type spaMiddleware struct {
	handler     http.Handler
	fs          http.FileSystem
	ignorePaths []string
}

// ServeHTTP implements http.Handler interface
func (l *spaMiddleware) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if !arrays.ContainsString(l.ignorePaths, r.URL.Path) {
		path := r.URL.Path
		if strings.HasSuffix(r.URL.Path, "/") {
			path = path + "index.html"
		}
		if _, err := l.fs.Open(path); err != nil {
			// can't read the file, change path to root
			r.URL.Path = "/"
		}
	}
	l.handler.ServeHTTP(w, r)
}
