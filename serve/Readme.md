# serve package
## Static Web
serve.Static is a helper for running static web containers. Main purpose is to provide mechanism to expose container environment variables to the front-end application.

### Example
### main.go
If you create the following program,
```go
package main

import (
	"embed"
	"gitlab.com/pgmtc-lib/commons/serve"
	"gitlab.com/pgmtc-lib/commons/svc"
	"log"
	"net/http"
)

//go:embed dist
var content embed.FS

func main() {
  var server := serve.Static(
    // allows front end routing - redirects 404 to /index.html
    serve.WithSPA,
    // listener
    serve.WithAddr(":8080"),
    // server content from var content, strip path dist
    serve.WithContent(content, "dist"),
    // expose only these env variables
    serve.WithExposedEnvVariables("ENV_VAR1", "ENV_VAR2"),
    // GZIP content
    serve.WithCompression,
    // You can intercept request before it is processed, return true to stop
    serve.WithIntercept(func(w http.ResponseWriter, r *http.Request) bool {
      log.Println(r.URL.Path)
      return false
    }),
  ) 
  // run ...
  if err := server(); err != nil {
    log.Fatal(err.Error())
  }
  // ...
}
```
Then once you build the container, front end application (which was in *dist* directory) can access container's env variables using the following relative paths:
#### json map (GET /env)
```json
{
  "ENV_VAR1":"env. variable 1",
  "ENV_VAR2":"env. variable 2",
}
```
You can get this using AJAX request.

#### javascript map object (GET /env.js)
which is assigned to the top level window object (get /env.js).
```javascript
window.env = new Map()
window.env.set("ENV_VAR1", "env. variable 1")
window.env.set("ENV_VAR2", "env. variable 2")
```
If you then reference this in the index.html, you will be able to access variables in your front end application.
For example:
```html
<script src=/env.js></script>
...
<script>
function sometimesLaterOn() {
   console.log("env variable 1 = ", window.env.get("ENV_VAR1)
}
</script>
```