package serve

import (
	"net/http"
)

// interceptMiddleware replaces missing files with index
type interceptMiddleware struct {
	handler     http.Handler
	interceptFn func(w http.ResponseWriter, r *http.Request) (stop bool)
}

// ServeHTTP implements http.Handler interface
func (l *interceptMiddleware) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if l.interceptFn != nil {
		if l.interceptFn(w, r) {
			return
		}
	}
	l.handler.ServeHTTP(w, r)
}
