package tst

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/pgmtc-lib/commons/ptrs"
	"testing"
	"time"
)

func TestForEach_Variadic(t *testing.T) {
	mockT := new(testing.T)
	assert.True(t, AllNotEmpty(mockT, 4, 3, 2, 1, -1))
	assert.False(t, AllNotEmpty(mockT, 4, 3, 2, 1, 0, -1))
	assert.True(t, AllEmpty(mockT, 0, 0, 0, "", time.Time{}))
	assert.False(t, AllEmpty(mockT, 0, 0, 0, "", time.Now().UTC()))
	assert.True(t, AllTrue(mockT, true, true, true))
	assert.False(t, AllTrue(mockT, true, true, false))
	assert.True(t, AllFalse(mockT, false, false, false))
	assert.False(t, AllFalse(mockT, true, true, false))

}

func TestForEach(t *testing.T) {
	type args struct {
	}
	tests := []struct {
		name   string
		in     interface{}
		testFn func(t assert.TestingT, object interface{}, msgAndArgs ...interface{}) bool
		want   bool
	}{
		{
			name:   "test-mix-notempty",
			testFn: assert.NotEmpty,
			in:     []interface{}{1, "a", time.Now().UTC(), ptrs.PtrTime(time.Now().UTC())},
			want:   true,
		},
		{
			name:   "test-mix-empty-inl",
			testFn: assert.Empty,
			in:     []interface{}{1, "a", time.Now().UTC(), ptrs.PtrTime(time.Now().UTC()), nil},
			want:   false,
		},
		{
			name:   "test-mix-empty-int",
			testFn: assert.Empty,
			in:     []interface{}{0, "a", time.Now().UTC(), ptrs.PtrTime(time.Now().UTC())},
			want:   false,
		},
		{
			name:   "test-mix-empty-str",
			testFn: assert.Empty,
			in:     []interface{}{1, "", time.Now().UTC(), ptrs.PtrTime(time.Now().UTC())},
			want:   false,
		},
		{
			name:   "test-mix-empty-int",
			testFn: assert.Empty,
			in:     []interface{}{1, "a", time.Time{}, ptrs.PtrTime(time.Now().UTC())},
			want:   false,
		},
		{
			name:   "test-mix-empty-int",
			testFn: assert.Empty,
			in:     []interface{}{1, "a", time.Now().UTC(), ptrs.PtrTime(time.Time{})},
			want:   false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mockT := new(testing.T)
			if got := loopInterface(mockT, tt.testFn, tt.in); got != tt.want {
				t.Errorf("loopInterface() = %v, want %v", got, tt.want)
			}
		})
	}
}
