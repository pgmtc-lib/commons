package tst

import (
	"fmt"
	"net/http"
)

// httpResponder is a function signature
type httpResponder func(req *http.Request) *http.Response

// NewTestHTTPClient returns testing http client which responds with the provided response
//
//	tst.NewTestHTTPSequencedClient(func(request *http.request) *http.Response {
//	  ...
//	})
func NewTestHTTPClient(onRequest httpResponder) (httpClient *http.Client) {
	var roundTripFn = func(requestHandler httpResponder) *http.Client {
		return &http.Client{
			Transport: requestHandler,
		}
	}
	httpClient = roundTripFn(onRequest)
	return
}

// NewTestHTTPSequencedClient returns testing http client which responds with provided responses in sequence
//
//	  tst.NewTestHTTPSequencedClient(
//			func(request *http.Request) *http.Response { ... response 1 ... },
//			func(request *http.Request) *http.Response { ... response 2 ... }
//	  )
func NewTestHTTPSequencedClient(onRequest ...httpResponder) (httpClient *http.Client) {
	var currentStep int
	var requestsHandler httpResponder = func(req *http.Request) (result *http.Response) {
		result = onRequest[currentStep](req)
		currentStep++
		return
	}
	var roundTripFn = func(requestHandler httpResponder) *http.Client {
		return &http.Client{
			Transport: requestHandler,
		}
	}
	httpClient = roundTripFn(requestsHandler)
	return
}

// RoundTrip implements RoundTripper interface
func (f httpResponder) RoundTrip(req *http.Request) (*http.Response, error) {
	return f(req), nil
}

// ErrReadCloser can be used for testing when reading from body in a request had failed
//
//	response = &http.Response{StatusCode: 200, Body: tst.ErrReadCloser(0), Header: make(http.Header)}
type ErrReadCloser int

// Read implements ReadCloser interface
func (e ErrReadCloser) Read(p []byte) (n int, err error) {
	return 0, fmt.Errorf("test read error")
}

// Close implements ReadCloser interface
func (e ErrReadCloser) Close() error {
	return fmt.Errorf("test close error")
}
