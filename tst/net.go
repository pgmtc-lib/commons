package tst

import (
	"bytes"
	"net"
)

// TestNetPipe returns net like pipe for testing
func TestNetPipe() (result net.Conn, responseChan chan []byte) {
	responseChan = make(chan []byte)
	server, client := net.Pipe()
	go func() {
		buf := make([]byte, 1024)
		_, _ = server.Read(buf)
		trimmed := bytes.Trim(buf, "\x00")
		responseChan <- trimmed
		server.Close()
	}()
	// Do some stuff
	result = client
	return
}
