package tst

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNetPipe_Test(t *testing.T) {
	conn, rChan := TestNetPipe()
	conn.Write([]byte("test data"))
	result := <-rChan
	assert.Equal(t, []byte("test data"), result)
	fmt.Println(result)
}
