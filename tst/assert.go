package tst

import (
	"github.com/stretchr/testify/assert"
	"reflect"
)

// AllEmpty runs test for every item in the slice
func AllEmpty(t assert.TestingT, in ...interface{}) bool {
	return loopInterface(t, assert.Empty, in)
}

// AllNotEmpty runs test for every item in the slice
func AllNotEmpty(t assert.TestingT, in ...interface{}) bool {
	return loopInterface(t, assert.NotEmpty, in)
}

// AllTrue tests every item in parameters
func AllTrue(t assert.TestingT, in ...bool) bool {
	testFn := func(t assert.TestingT, object interface{}, msgAndArgs ...interface{}) bool {
		return assert.True(t, object.(bool))
	}
	return loopInterface(t, testFn, in)
}

// AllFalse tests every item in parameters
func AllFalse(t assert.TestingT, in ...bool) bool {
	testFn := func(t assert.TestingT, object interface{}, msgAndArgs ...interface{}) bool {
		return assert.False(t, object.(bool))
	}
	return loopInterface(t, testFn, in)
}

// loopInterface runs testify function for all items
func loopInterface(t assert.TestingT, testFn func(t assert.TestingT, object interface{}, msgAndArgs ...interface{}) bool, in interface{}) bool {
	switch reflect.TypeOf(in).Kind() {
	case reflect.Slice:
		s := reflect.ValueOf(in)
		for i := 0; i < s.Len(); i++ {
			if !testFn(t, s.Index(i).Interface()) {
				assert.Failf(t, "loopInterface fail", "one of the items (index = %d) in loopInterface has failed test", i)
				return false
			}
		}
		return true
	}
	assert.Fail(t, "unexpected type in provided")
	return false
}
