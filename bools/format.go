package bools

// Format formats bool or *bool, returns trueValue or falseValue
func Format(in bool, trueValue, falseValue string) (result string) {
	if in {
		return trueValue
	}
	return falseValue
}
