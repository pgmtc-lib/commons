package bools

import (
	"gitlab.com/pgmtc-lib/commons/ptrs"
	"testing"
)

func TestParseString(t *testing.T) {
	tests := []struct {
		name string
		in   string
		want bool
	}{
		{name: "test-true", in: "true", want: true},
		{name: "test-true", in: "tRue", want: true},
		{name: "test-true", in: "1", want: true},
		{name: "test-true", in: "yes", want: true},
		{name: "test-true", in: "yEs", want: true},
		{name: "test-false", in: "", want: false},
		{name: "test-false", in: "false", want: false},
		{name: "test-false", in: "no", want: false},
		{name: "test-false", in: "anything else", want: false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ParseString(tt.in); got != tt.want {
				t.Errorf("ParseString() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestParseIntPtr(t *testing.T) {
	type args struct {
		in          interface{}
		ifEmptyThen bool
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{name: "test-true", want: true, args: args{in: 1, ifEmptyThen: false}},
		{name: "test-true", want: true, args: args{in: true, ifEmptyThen: false}},
		{name: "test-true", want: true, args: args{in: "true", ifEmptyThen: false}},
		{name: "test-true", want: true, args: args{in: ptrs.PtrInt(1), ifEmptyThen: false}},
		{name: "test-true", want: true, args: args{in: ptrs.PtrBool(true), ifEmptyThen: false}},
		{name: "test-true", want: true, args: args{in: ptrs.PtrString("true"), ifEmptyThen: false}},
		// test parsing falses
		{name: "test-false", want: false, args: args{in: false}},
		{name: "test-false", want: false, args: args{in: 0}},
		{name: "test-false", want: false, args: args{in: "false"}},
		// test parsing errors
		{name: "test-false", want: true, args: args{in: nil, ifEmptyThen: true}},
		{name: "test-false", want: true, args: args{in: 2, ifEmptyThen: true}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Parse(tt.args.in, tt.args.ifEmptyThen); got != tt.want {
				t.Errorf("ParseIntPtr() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestParseStringStrict(t *testing.T) {
	tests := []struct {
		name       string
		in         string
		wantResult bool
		wantErr    bool
	}{
		{name: "test-true", in: "true", wantResult: true},
		{name: "test-true", in: "tRue", wantResult: true},
		{name: "test-true", in: "1", wantResult: true},
		{name: "test-true", in: "yes", wantResult: true},
		{name: "test-true", in: "yEs", wantResult: true},
		{name: "test-false", in: "false", wantResult: false},
		{name: "test-false", in: "no", wantResult: false},
		{name: "test-false", in: "0", wantResult: false},
		{name: "test-false", in: "", wantErr: true},
		{name: "test-false", in: "anything else", wantErr: true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotResult, err := ParseStringStrict(tt.in)
			if (err != nil) != tt.wantErr {
				t.Errorf("ParseStringStrict() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if gotResult != tt.wantResult {
				t.Errorf("ParseStringStrict() gotResult = %v, want %v", gotResult, tt.wantResult)
			}
		})
	}
}
