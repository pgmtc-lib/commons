package bools

import (
	"testing"
)

func TestFormat(t *testing.T) {
	type args struct {
	}
	tests := []struct {
		name       string
		in         bool
		trueValue  string
		falseValue string
		wantResult string
	}{
		{name: "test-bool-true", in: true, trueValue: "yes", falseValue: "no", wantResult: "yes"},
		{name: "test-bool-false", in: false, trueValue: "yes", falseValue: "no", wantResult: "no"},
		{name: "test-bool-true-custom", in: true, trueValue: "yay", falseValue: "nah", wantResult: "yay"},
		{name: "test-bool-false-custom", in: false, trueValue: "yay", falseValue: "nah", wantResult: "nah"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotResult := Format(tt.in, tt.trueValue, tt.falseValue); gotResult != tt.wantResult {
				t.Errorf("Format() = %v, want %v", gotResult, tt.wantResult)
			}
		})
	}
}
