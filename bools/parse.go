package bools

import (
	"fmt"
	"strings"
)

// ParseString returns true if in equals case insensitive true, 1 or yes
func ParseString(in string) bool {
	lower := strings.ToLower(in)
	if lower == "true" || lower == "1" || lower == "yes" {
		return true
	}
	return false
}

// ParseStringStrict returns true for true/1/yes and false for false/0/no, otherwise error
func ParseStringStrict(in string) (result bool, resultErr error) {
	lower := strings.ToLower(in)
	if lower == "true" || lower == "1" || lower == "yes" {
		result = true
		return
	}
	if lower == "false" || lower == "0" || lower == "no" {
		result = false
		return
	}
	resultErr = fmt.Errorf("cannot parse %s to bool", in)
	return
}

// Parse parses interface to bool
func Parse(in interface{}, ifEmptyThen bool) bool {
	if _, ok := in.(bool); ok {
		return in.(bool)
	}
	if _, ok := in.(*bool); ok {
		return *(in.(*bool))
	}
	if _, ok := in.(string); ok {
		return ParseString(in.(string))
	}
	if _, ok := in.(*string); ok {
		return ParseString(*in.(*string))
	}
	if _, ok := in.(int); ok {
		if in.(int) != 1 {
			return ifEmptyThen
		}
		return true
	}
	if _, ok := in.(*int); ok {
		if in.(*int) == nil {
			return ifEmptyThen
		}
		if *in.(*int) != 1 {
			return ifEmptyThen
		}
		return true
	}
	return ifEmptyThen
}
