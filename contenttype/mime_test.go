package contenttype

import "testing"

func TestFromFileName(t *testing.T) {
	tests := []struct {
		name     string
		filename string
		want     string
	}{
		{
			name:     "html",
			filename: "index.html",
			want:     "text/html",
		},
		{
			name:     "html",
			filename: "index.css",
			want:     "text/css",
		},
		{
			name:     "html",
			filename: "index.js",
			want:     "application/javascript",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := FromFileName(tt.filename); got != tt.want {
				t.Errorf("GetContentType() = %v, want %v", got, tt.want)
			}
		})
	}
}
