package geo

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestDistance(t *testing.T) {
	type args struct {
		lat1 float64
		lon1 float64
		lat2 float64
		lon2 float64
	}
	tests := []struct {
		name string
		args args
		want float64
	}{
		{
			name: "test-1",
			args: args{
				lat1: 51.507351,
				lon1: -0.127758,
				lat2: 55.953251,
				lon2: -3.188267,
			},
			want: 533626.84,
		},
		{
			name: "test-2",
			args: args{
				lat1: 51.507351,
				lon1: -0.127758,
				lat2: 50.075539,
				lon2: 14.437800,
			},
			want: 1034303.48,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := DistanceMetres(tt.args.lat1, tt.args.lon1, tt.args.lat2, tt.args.lon2)
			assert.Equal(t, tt.want, got)
		})
	}
}
