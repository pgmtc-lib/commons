package nets

import (
	"net"
	"strings"
)

// GetHost parses host from addr
func GetHost(in net.Addr) (result string) {
	domainPortSplit := strings.Split(in.String(), ":")
	if len(domainPortSplit) > 0 {
		result = strings.TrimSuffix(in.String(), ":"+domainPortSplit[len(domainPortSplit)-1])
		return
	}
	result = in.String()
	return
}

// MyIP returns primary IP address
func MyIP() string {
	conn, err := net.Dial("udp", "8.8.8.8:80")
	if err != nil {
		return ""
	}
	defer conn.Close()
	return GetHost(conn.LocalAddr().(*net.UDPAddr))
}
