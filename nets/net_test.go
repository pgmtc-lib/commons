package nets

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"net"
	"testing"
)

func TestGetHost(t *testing.T) {
	tests := []struct {
		name       string
		in         net.Addr
		wantResult string
	}{
		{
			name:       "test-ipv4",
			in:         &net.TCPAddr{IP: []byte{127, 0, 0, 1}, Port: 8080, Zone: ""},
			wantResult: "127.0.0.1",
		},
		{
			name:       "test-hostname",
			in:         &net.UnixAddr{Name: "localhost", Net: "tcp"},
			wantResult: "localhost",
		},
		{
			name:       "test-ipv6",
			in:         &net.TCPAddr{IP: net.ParseIP("2001:db8:85a3:8d3:1319:8a2e:370:7348"), Port: 8080, Zone: ""},
			wantResult: "[2001:db8:85a3:8d3:1319:8a2e:370:7348]",
		},
		{
			name:       "test-ipv6-local",
			in:         &net.TCPAddr{IP: net.ParseIP("::1"), Port: 8080, Zone: ""},
			wantResult: "[::1]",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotResult := GetHost(tt.in); gotResult != tt.wantResult {
				t.Errorf("GetHost() = %v, want %v", gotResult, tt.wantResult)
			}
		})
	}
}

func TestMyIP(t *testing.T) {
	ip := MyIP()
	assert.NotEmpty(t, ip)
	fmt.Println(ip)
}
