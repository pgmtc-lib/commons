module gitlab.com/pgmtc-lib/commons

go 1.16

require (
	bou.ke/monkey v1.0.2
	github.com/DATA-DOG/go-sqlmock v1.5.0
	github.com/NYTimes/gziphandler v1.1.1 // indirect
	github.com/stretchr/testify v1.6.1
	github.com/tj/assert v0.0.3
	nhooyr.io/websocket v1.8.7 // indirect
)
