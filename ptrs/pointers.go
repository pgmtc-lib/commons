package ptrs

import "time"

// PtrInt converts int to *int
func PtrInt(value int) *int {
	return &value
}

// PtrString converts string to *string
func PtrString(value string) *string {
	return &value
}

// PtrFloat64 converts float64 to *float64
func PtrFloat64(value float64) *float64 {
	return &value
}

// PtrTime converts time to *time
func PtrTime(value time.Time) *time.Time {
	return &value
}

// PtrDuration converts duration to *duration
func PtrDuration(value time.Duration) *time.Duration {
	return &value
}

// PtrBool converts bool to *bool
func PtrBool(value bool) *bool {
	return &value
}

// FromPtrOrNilString returns string (for valid pointer) or empty string (for nils)
func FromPtrOrNilString(value *string) string {
	if value == nil {
		return ""
	}
	return *value
}

// PtrOrNilString returns pointer to string unless it is empty. In that case, returns nil
func PtrOrNilString(value string) *string {
	if value == "" {
		return nil
	}
	return &value
}

// FromPtrOrNilTime returns time (for valid pointer) or empty time (for nils)
func FromPtrOrNilTime(value *time.Time) time.Time {
	if value == nil {
		return time.Time{}
	}
	return *value
}

// PtrOrNilTime returns pointer to time unless it is empty. In that case, returns nil
func PtrOrNilTime(value time.Time) *time.Time {
	emptyValue := time.Time{}
	if value == emptyValue {
		return nil
	}
	return &value
}
