package ptrs

import (
	"github.com/stretchr/testify/assert"
	"reflect"
	"testing"
	"time"
)

func TestPtrMethods(t *testing.T) {
	intValue := 10
	intResult := PtrInt(intValue)
	assert.Equal(t, intValue, *intResult)

	stringValue := "test"
	stringResult := PtrString(stringValue)
	assert.Equal(t, stringValue, *stringResult)

	float64Value := 23.54564
	float64Result := PtrFloat64(float64Value)
	assert.Equal(t, float64Value, *float64Result)

	timeValue := time.Now().UTC()
	timeResult := PtrTime(timeValue)
	assert.Equal(t, timeValue, *timeResult)

	durationValue := 5 * time.Minute
	durationResult := PtrDuration(durationValue)
	assert.Equal(t, durationValue, *durationResult)

	boolResult := PtrBool(true)
	assert.Equal(t, true, *boolResult)

}

func TestFromPtrOrNilString(t *testing.T) {
	fullString := "some-value"
	type args struct {
		value *string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "test-value",
			args: args{value: &fullString},
			want: fullString,
		},
		{
			name: "test-value",
			args: args{value: nil},
			want: "",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := FromPtrOrNilString(tt.args.value); got != tt.want {
				t.Errorf("FromPtrOrNilString() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPtrOrNilString(t *testing.T) {
	fullString := "some-value"
	emptyString := ""
	type args struct {
		value string
	}
	tests := []struct {
		name string
		args args
		want *string
	}{
		{
			name: "test-full",
			args: args{value: fullString},
			want: &fullString,
		},
		{
			name: "test-full",
			args: args{value: emptyString},
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := PtrOrNilString(tt.args.value); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("PtrOrNilString() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFromPtrOrNilTime(t *testing.T) {

	tests := []struct {
		name  string
		value *time.Time
		want  time.Time
	}{
		{
			name:  "value",
			value: PtrTime(time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC)),
			want:  time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC),
		},
		{
			name:  "time-nil",
			value: nil,
			want:  time.Time{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := FromPtrOrNilTime(tt.value); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("FromPtrOrNilTime() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPtrOrNilTime(t *testing.T) {
	tests := []struct {
		name  string
		value time.Time
		want  *time.Time
	}{
		{
			name:  "value",
			value: time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC),
			want:  PtrTime(time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC)),
		},
		{
			name:  "empty",
			value: time.Time{},
			want:  nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := PtrOrNilTime(tt.value); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("PtrOrNilTime() = %v, want %v", got, tt.want)
			}
		})
	}
}
