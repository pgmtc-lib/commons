package errs

import (
	"fmt"
	"log"
)

// LogHandler is a generic log handler - could be overridden
var LogHandler = func(message string) {
	log.Println(message)
}

// Wrap wraps error with an extra format
func Wrap(err error, format string, a ...interface{}) error {
	if err != nil {
		return fmt.Errorf("%s: %s", fmt.Sprintf(format, a...), err.Error())
	}
	return nil
}

// Log prints the error into provided handler. If no handler provided, LogHandler is used
func Log(err error, handlers ...func(string)) {
	if err != nil {
		if len(handlers) == 0 {
			LogHandler(err.Error())
			return
		}
		for _, handler := range handlers {
			handler(err.Error())
		}
	}
}
