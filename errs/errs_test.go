package errs

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestWrapWith(t *testing.T) {
	tests := []struct {
		name    string
		err     error
		format  string
		a       []interface{}
		wantErr error
	}{
		{
			name:    "test-with-error",
			err:     fmt.Errorf("something went wrong"),
			format:  "what a shame number %d - %s",
			a:       []interface{}{10, "string"},
			wantErr: fmt.Errorf("what a shame number 10 - string: something went wrong"),
		},
		{
			name:    "test-without-error",
			err:     nil,
			format:  "what a shame",
			wantErr: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotErr := Wrap(tt.err, tt.format, tt.a...)
			assert.Equal(t, tt.wantErr, gotErr)
		})
	}
}

func TestLog(t *testing.T) {
	gotHandlerError := ""
	// test bare
	err := fmt.Errorf("mock error")
	Log(err)
	assert.Empty(t, gotHandlerError)
	// test with one handler
	Log(err, func(s string) { gotHandlerError = s })
	assert.Equal(t, "mock error", gotHandlerError)
	// test with two handlers
	Log(err, func(s string) { gotHandlerError += s }, func(s string) { gotHandlerError += s })
	assert.Equal(t, gotHandlerError, "mock errormock errormock error")
	// test generic handler
	LogHandler = func(message string) {
		gotHandlerError = message
	}
	Log(err)
	assert.Equal(t, "mock error", gotHandlerError)
}
