package htutils

import (
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestErrorHandler_ServeHTTP(t *testing.T) {
	in := ErrorHandler{
		Status:  100,
		Message: "some message",
	}
	rr := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/my_url", nil)

	in.ServeHTTP(rr, req)

	assert.Equal(t, 100, rr.Code)
	assert.Equal(t, "{\"error\":\"some message\"}", rr.Body.String())

}
