package htutils

import (
	"gitlab.com/pgmtc-lib/commons/bools"
	"net/http"
	"strconv"
	"time"
)

// HasGetParameter returns if the parameter is present in the URL
func HasGetParameter(r *http.Request, param string) (result bool) {
	return r.URL.Query().Has(param)
}

// GetParameterString tries to parse int parameter
func GetParameterString(r *http.Request, param string) (result string) {
	if r.URL.Query().Has(param) {
		val := r.URL.Query().Get(param)
		result = val
	}
	return
}

// GetParameterInt tries to parse int parameter
func GetParameterInt(r *http.Request, param string) (result int) {
	if parsed, err := strconv.Atoi(r.URL.Query().Get(param)); err == nil {
		result = parsed
	}
	return
}

// GetParameterTime tries to parse time parameter
func GetParameterTime(r *http.Request, param string) (result time.Time) {
	if parsed, err := time.Parse(time.RFC3339, r.URL.Query().Get(param)); err == nil {
		result = parsed
	}
	return
}

// GetParameterFloat tries to parse float parameter
func GetParameterFloat(r *http.Request, param string) (result float64) {
	if parsed, err := strconv.ParseFloat(r.URL.Query().Get(param), 64); err == nil {
		result = parsed
	}
	return
}

// GetParameterBool tries to parse int parameter
func GetParameterBool(r *http.Request, param string) (result bool) {
	if r.URL.Query().Has(param) {
		val := bools.ParseString(r.URL.Query().Get(param))
		result = val
	}
	return
}
