package htutils

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func TestPanicRecoveryMiddleWare(t *testing.T) {
	gotLogMessage := ""
	targetHandlerCalled := false
	loggerFn := func(message string, args ...interface{}) {
		gotLogMessage = fmt.Sprintf(message, args...)
	}
	targetHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		targetHandlerCalled = true
		panic("oh no!")
	})
	protectedHandler := PanicRecoveryMiddleWare(loggerFn)(targetHandler)
	req := httptest.NewRequest("GET", "http://testing", nil)
	protectedHandler.ServeHTTP(httptest.NewRecorder(), req)
	assert.True(t, targetHandlerCalled)
	assert.True(t, strings.HasPrefix(gotLogMessage, "recovering from err oh no!"))
}
