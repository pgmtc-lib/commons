package htutils

import (
	"github.com/stretchr/testify/assert"
	"net/http"
	"strings"
	"testing"
)

func Test_stringValue(t *testing.T) {
	reader := strings.NewReader("string=value&number=2&float=3.1")
	req, _ := http.NewRequest("POST", "/my_url", reader)
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	// test existing
	out, err := stringValue(req, "string", true)
	assert.NoError(t, err)
	assert.Equal(t, "value", out)

	// test existing
	out, err = stringValue(req, "float", true)
	assert.NoError(t, err)
	assert.Equal(t, "3.1", out)

	// test missing when mandatory
	out, err = stringValue(req, "string2", true)
	assert.EqualError(t, err, "field string2 is empty")

	// test missing when non-mandatory
	out, err = stringValue(req, "string2", false)
	assert.NoError(t, err)
	assert.Equal(t, "", out)
}

func TestFormParser_MandInt(t *testing.T) {
	reader := strings.NewReader("string=value&number=2&float=3.1")
	req, _ := http.NewRequest("POST", "/my_url", reader)
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	parser := FormParser{Req: req}
	out, err := parser.MandInt("number")
	assert.NoError(t, err)
	assert.Equal(t, 2, out)

	// test non-existing
	out, err = parser.MandInt("number2")
	assert.EqualError(t, err, "field number2 is empty")

	// test non-parsable
	out, err = parser.MandInt("string")
	assert.EqualError(t, err, "cannot parse string value value to int")
}

func TestFormParser_MandFloat(t *testing.T) {
	reader := strings.NewReader("string=value&number=2&float=3.1")
	req, _ := http.NewRequest("POST", "/my_url", reader)
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	parser := FormParser{Req: req}
	out, err := parser.MandFloat("float")
	assert.NoError(t, err)
	assert.Equal(t, 3.1, out)

	// test non-existing
	out, err = parser.MandFloat("float2")
	assert.EqualError(t, err, "field float2 is empty")

	// test non-parsable
	out, err = parser.MandFloat("string")
	assert.EqualError(t, err, "cannot parse string value value to float64")
}

func TestFormParser_MandBool(t *testing.T) {
	reader := strings.NewReader("string=value&number=2&float=3.1&bool=true&bool2=false")
	req, _ := http.NewRequest("POST", "/my_url", reader)
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	parser := FormParser{Req: req}
	out, err := parser.MandBool("bool")
	assert.NoError(t, err)
	assert.Equal(t, true, out)

	out, err = parser.MandBool("bool2")
	assert.NoError(t, err)
	assert.Equal(t, false, out)

	// test non-existing
	out, err = parser.MandBool("float2")
	assert.EqualError(t, err, "field float2 is empty")

	// test non-parsable
	out, err = parser.MandBool("string")
	assert.EqualError(t, err, "cannot parse string value value to bool")
}

func TestFormParser_MandString(t *testing.T) {
	reader := strings.NewReader("string=value&number=2&float=3.1&bool=true&bool2=false")
	req, _ := http.NewRequest("POST", "/my_url", reader)
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	parser := FormParser{Req: req}
	out, err := parser.MandString("string")
	assert.NoError(t, err)
	assert.Equal(t, "value", out)

	// test non-existing
	out, err = parser.MandString("string2")
	assert.EqualError(t, err, "field string2 is empty")
}

func TestFormParser_NonMandatories(t *testing.T) {
	reader := strings.NewReader("string=value&number=2&float=3.1&bool=true")
	req, _ := http.NewRequest("POST", "/my_url", reader)
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	parser := FormParser{Req: req}
	assert.Equal(t, "value", parser.String("string", "default"))
	assert.Equal(t, "default", parser.String("string2", "default"))
	assert.Equal(t, 2, parser.Int("number", -1))
	assert.Equal(t, -1, parser.Int("number2", -1))
	assert.Equal(t, 3.1, parser.Float("float", -1.1))
	assert.Equal(t, -1.1, parser.Float("float2", -1.1))
	assert.Equal(t, true, parser.Bool("bool", true))
	assert.Equal(t, true, parser.Bool("bool2", true))
	assert.Equal(t, false, parser.Bool("bool2", false))

}
