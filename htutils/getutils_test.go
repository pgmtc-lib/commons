package htutils

import (
	"github.com/stretchr/testify/assert"
	"net/http"
	"testing"
	"time"
)

func TestHasGetParameter(t *testing.T) {
	req, _ := http.NewRequest("GET", "/some-url?string=value&bool1=true&bool2=false&bool3=yes&bool4=crap&number=2&float=3.1&time=2021-03-22T15:03:57.327Z&string2=value2&string2=value3", nil)
	assert.True(t, HasGetParameter(req, "bool1"))
	assert.True(t, HasGetParameter(req, "number"))
	assert.True(t, HasGetParameter(req, "float"))
	assert.True(t, HasGetParameter(req, "time"))
	assert.True(t, HasGetParameter(req, "string"))
	assert.False(t, HasGetParameter(req, "boolx"))
	assert.False(t, HasGetParameter(req, ""))
}

func TestGetParameterString(t *testing.T) {
	req, _ := http.NewRequest("GET", "/some-url?string=value&number=2&float=3.1&time=2021-03-22T15:03:57.327Z&string2=value2&string2=value3", nil)
	out := GetParameterString(req, "string")
	assert.Equal(t, out, "value")
	out = GetParameterString(req, "string2")
	assert.Equal(t, out, "value2")
	// test invalid
	out = GetParameterString(req, "string3")
	assert.Empty(t, out)
}

func TestGetGetParameterInt(t *testing.T) {
	req, _ := http.NewRequest("GET", "/some-url?string=value&number=2&float=3.1&time=2021-03-22T15:03:57.327Z", nil)
	out := GetParameterInt(req, "number")
	assert.Equal(t, 2, out)
	// test invalid
	out = GetParameterInt(req, "string")
	assert.Empty(t, out)
}

func TestGetParameterTime(t *testing.T) {
	req, _ := http.NewRequest("GET", "/some-url?string=value&number=2&float=3.1&time=2021-03-22T15:03:57.327Z", nil)
	out := GetParameterTime(req, "time")
	assert.Equal(t, time.Date(2021, 3, 22, 15, 3, 57, 327000000, time.UTC), out)
	// test invalid
	out = GetParameterTime(req, "string")
	assert.Empty(t, out)
}

func TestGetParameterFloat(t *testing.T) {
	req, _ := http.NewRequest("GET", "/some-url?string=value&number=2&float=3.1&time=2021-03-22T15:03:57.327Z", nil)
	out := GetParameterFloat(req, "float")
	assert.Equal(t, 3.1, out)
	// test invalid
	out = GetParameterFloat(req, "string")
	assert.Empty(t, out)
}

func TestGetParameterBool(t *testing.T) {
	req, _ := http.NewRequest("GET", "/some-url?string=value&bool1=true&bool2=false&bool3=yes&bool4=crap&number=2&float=3.1&time=2021-03-22T15:03:57.327Z&string2=value2&string2=value3", nil)
	out := GetParameterBool(req, "bool1")
	assert.Equal(t, out, true)
	out = GetParameterBool(req, "bool2")
	assert.Equal(t, out, false)
	out = GetParameterBool(req, "bool3")
	assert.Equal(t, out, true)
	out = GetParameterBool(req, "bool4")
	assert.Equal(t, out, false)
	out = GetParameterBool(req, "bool5")
	assert.Equal(t, out, false)

	// test invalid
	out = GetParameterBool(req, "string3")
	assert.Equal(t, out, false)
}
