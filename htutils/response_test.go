package htutils

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"net/http/httptest"
	"testing"
)

func TestSendResponse(t *testing.T) {
	rr := httptest.NewRecorder()
	SendResponse(rr, 200, Response{
		Data:  true,
		Error: "Test Message",
	})
	out := rr.Body.String()
	assert.Equal(t, "{\"error\":\"Test Message\",\"data\":true}", out)
}

func TestSendSuccess(t *testing.T) {
	rr := httptest.NewRecorder()
	SendSuccess(rr, "result")
	assert.Equal(t, 200, rr.Code)
	assert.Equal(t, "{\"data\":\"result\"}", rr.Body.String())
	// test shutter
	ShutterMessage = "my shutter message"
	rr = httptest.NewRecorder()
	SendSuccess(rr, "result")
	assert.Equal(t, 200, rr.Code)
	assert.Equal(t, "{\"data\":\"result\",\"shutter_message\":\"my shutter message\"}", rr.Body.String())
	ShutterMessage = ""
}

func TestSendError(t *testing.T) {
	rr := httptest.NewRecorder()
	SendError(rr, 999, "some error")
	assert.Equal(t, 999, rr.Code)
	assert.Equal(t, "{\"error\":\"some error\"}", rr.Body.String())
	// test formatting
	rr = httptest.NewRecorder()
	SendError(rr, 999, "some error %s", "formatted")
	assert.Equal(t, 999, rr.Code)
	assert.Equal(t, "{\"error\":\"some error formatted\"}", rr.Body.String())
	// test shutter
	ShutterMessage = "my shutter message"
	rr = httptest.NewRecorder()
	SendError(rr, 999, "some error")
	assert.Equal(t, 999, rr.Code)
	assert.Equal(t, "{\"error\":\"some error\",\"shutter_message\":\"my shutter message\"}", rr.Body.String())
	ShutterMessage = ""
}

func TestSendUnauthorized(t *testing.T) {
	rr := httptest.NewRecorder()
	SendUnauthorized(rr)
	assert.Equal(t, 401, rr.Code)
	assert.Equal(t, "{\"error\":\"401 - Unauthorized\"}", rr.Body.String())
}

func TestSendForbidden(t *testing.T) {
	rr := httptest.NewRecorder()
	SendForbidden(rr)
	assert.Equal(t, 403, rr.Code)
	assert.Equal(t, "{\"error\":\"403 - Forbidden\"}", rr.Body.String())
}

func TestProcessError(t *testing.T) {
	rr := httptest.NewRecorder()
	ProcessError(rr, fmt.Errorf("test error"))
	assert.Equal(t, 500, rr.Code)
	assert.Equal(t, "{\"error\":\"test error\"}", rr.Body.String())
	// test without error
	rr = httptest.NewRecorder()
	ProcessError(rr, nil)
	assert.Equal(t, 200, rr.Code)
	assert.Equal(t, "{\"data\":true}", rr.Body.String())
}

func TestProcessErrorWithSuccess(t *testing.T) {
	rr := httptest.NewRecorder()
	ProcessErrorWithSuccess(rr, "success message", fmt.Errorf("test error"))
	assert.Equal(t, 500, rr.Code)
	assert.Equal(t, "{\"error\":\"test error\"}", rr.Body.String())
	// test without error
	rr = httptest.NewRecorder()
	ProcessErrorWithSuccess(rr, "success message", nil)
	assert.Equal(t, 200, rr.Code)
	assert.Equal(t, "{\"data\":\"success message\"}", rr.Body.String())
}

func TestProcessResponseOrError(t *testing.T) {
	rr := httptest.NewRecorder()
	ProcessResponseOrError(rr)("some result", fmt.Errorf("test error"))
	assert.Equal(t, 500, rr.Code)
	assert.Equal(t, "{\"error\":\"test error\"}", rr.Body.String())

	rr = httptest.NewRecorder()
	ProcessResponseOrError(rr)("some result", nil)
	assert.Equal(t, 200, rr.Code)
	assert.Equal(t, "{\"data\":\"some result\"}", rr.Body.String())
}

func TestProcessSuccessOrError(t *testing.T) {
	rr := httptest.NewRecorder()
	ProcessSuccessOrError(rr, "success message")("some result", fmt.Errorf("test error"))
	assert.Equal(t, 500, rr.Code)
	assert.Equal(t, "{\"error\":\"test error\"}", rr.Body.String())

	rr = httptest.NewRecorder()
	ProcessSuccessOrError(rr, "success message")("some result", nil)
	assert.Equal(t, 200, rr.Code)
	assert.Equal(t, "{\"data\":\"success message\"}", rr.Body.String())

}

func TestToJSON(t *testing.T) {
	type testStruct struct {
		ID   int
		Name string
	}
	in := testStruct{
		ID:   10,
		Name: "some name",
	}
	out := ToJSON(in)
	assert.Equal(t, "{\"ID\":10,\"Name\":\"some name\"}", string(out))
}
