package htutils

import (
	"fmt"
	"gitlab.com/pgmtc-lib/commons/bools"
	"net/http"
	"strconv"
)

// FormParser is a parser for http POST forms
type FormParser struct {
	Req *http.Request
}

// Bool returns bool value from the form
func (p FormParser) Bool(fieldName string, def bool) bool {
	if result, err := p.MandBool(fieldName); err == nil {
		return result
	}
	return def
}

// Float returns float value from the form
func (p FormParser) Float(fieldName string, def float64) float64 {
	if result, err := p.MandFloat(fieldName); err == nil {
		return result
	}
	return def
}

// Int returns int value from the form
func (p FormParser) Int(fieldName string, def int) int {
	if result, err := p.MandInt(fieldName); err == nil {
		return result
	}
	return def
}

// String returns string value from the form
func (p FormParser) String(fieldName string, def string) string {
	if result, err := stringValue(p.Req, fieldName, true); err == nil {
		return result
	}
	return def
}

// MandBool returns mandatory bool from the form
func (p FormParser) MandBool(fieldName string) (result bool, resultErr error) {
	var strResult string
	if strResult, resultErr = stringValue(p.Req, fieldName, true); resultErr != nil {
		return
	}
	if result, resultErr = bools.ParseStringStrict(strResult); resultErr != nil {
		resultErr = fmt.Errorf("cannot parse %s value %s to bool", fieldName, strResult)
	}
	return
}

// MandFloat returns mandatory float from the form
func (p FormParser) MandFloat(fieldName string) (result float64, resultErr error) {
	var strResult string
	if strResult, resultErr = stringValue(p.Req, fieldName, true); resultErr != nil {
		return
	}
	if result, resultErr = strconv.ParseFloat(strResult, 64); resultErr != nil {
		resultErr = fmt.Errorf("cannot parse %s value %s to float64", fieldName, strResult)
	}
	return
}

// MandInt returns mandatory int from the form
func (p FormParser) MandInt(fieldName string) (result int, resultErr error) {
	var strResult string
	if strResult, resultErr = stringValue(p.Req, fieldName, true); resultErr != nil {
		return
	}
	if result, resultErr = strconv.Atoi(strResult); resultErr != nil {
		resultErr = fmt.Errorf("cannot parse %s value %s to int", fieldName, strResult)
	}
	return
}

// MandString returns mandatory string from the form
func (p FormParser) MandString(fieldName string) (result string, resultErr error) {
	return stringValue(p.Req, fieldName, true)
}

// stringValue parses string value from the form, returns error when empty and manatory flag is set to true
func stringValue(r *http.Request, fieldName string, mandatory bool) (result string, resultErr error) {
	if result = r.FormValue(fieldName); result == "" && mandatory {
		resultErr = fmt.Errorf("field %s is empty", fieldName)
		return
	}
	return
}
