package htutils

import (
	"net/http"
	"runtime"
)

// PanicRecoveryMiddleWare returns middleware function which, when first in the chain, catches any panic in subsequent handlers
func PanicRecoveryMiddleWare(logFn func(message string, args ...interface{})) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			defer func() {
				if err := recover(); err != nil {
					buf := make([]byte, 2048)
					n := runtime.Stack(buf, false)
					buf = buf[:n]
					logFn("recovering from err %v\n %s", err, buf)
					SendError(w, 500, "internal server error")
				}
			}()
			next.ServeHTTP(w, r)
		})
	}
}
