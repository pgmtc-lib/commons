package htutils

import (
	"encoding/json"
	"fmt"
	"net/http"
)

// ShutterMessage is included with every response - when set
var ShutterMessage = ""

// Response is an object exchanged between server and the client
type Response struct {
	Error          string      `json:"error,omitempty"`
	Data           interface{} `json:"data,omitempty"`
	ShutterMessage string      `json:"shutter_message,omitempty"`
}

// SendSuccess responds with success message
func SendSuccess(w http.ResponseWriter, data interface{}) {
	resp := Response{
		Data:           data,
		ShutterMessage: ShutterMessage,
	}
	SendResponse(w, 200, resp)
}

// SendError responds with error message
func SendError(w http.ResponseWriter, httpStatus int, formatString string, a ...interface{}) {
	resp := Response{
		Error:          fmt.Sprintf(formatString, a...),
		ShutterMessage: ShutterMessage,
	}
	SendResponse(w, httpStatus, resp)
}

// SendUnauthorized sends 401 error
func SendUnauthorized(w http.ResponseWriter) {
	SendError(w, http.StatusUnauthorized, "401 - Unauthorized")
}

// SendForbidden sends 401 error
func SendForbidden(w http.ResponseWriter) {
	SendError(w, http.StatusForbidden, "403 - Forbidden")
}

// SendResponse is a generic sender for response
func SendResponse(w http.ResponseWriter, statusCode int, resp interface{}) {
	js, err := json.Marshal(resp)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(statusCode)
	_, _ = w.Write(js)
}

// ToJSON tries to marshall into json, if fails, then returns no bytes
func ToJSON(in interface{}) (result []byte) {
	result, _ = json.Marshal(in)
	return
}

// ProcessError returns status 500 if error, otherwise 200 with true
func ProcessError(w http.ResponseWriter, err error) {
	if err != nil {
		SendError(w, 500, err.Error())
		return
	}
	SendSuccess(w, true)
}

// ProcessErrorWithSuccess returns status 500 if error, otherwise 200 with custom successResponse
func ProcessErrorWithSuccess(w http.ResponseWriter, successResponse interface{}, err error) {
	if err != nil {
		SendError(w, 500, err.Error())
		return
	}
	SendSuccess(w, successResponse)
}

// ProcessResponseOrError returns error if there was an error, otherwise it sends success
func ProcessResponseOrError(w http.ResponseWriter) func(res interface{}, err error) {
	return func(res interface{}, err error) {
		if err != nil {
			SendError(w, 500, err.Error())
			return
		}
		SendSuccess(w, res)
	}
}

// ProcessSuccessOrError returns error if there was an error, otherwise it sends success
func ProcessSuccessOrError(w http.ResponseWriter, successResponse interface{}) func(res interface{}, err error) {
	return func(res interface{}, err error) {
		if err != nil {
			SendError(w, 500, err.Error())
			return
		}
		SendSuccess(w, successResponse)
	}
}
