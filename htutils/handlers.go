package htutils

import "net/http"

// ErrorHandler is a struct which could be used as a generic error handler
type ErrorHandler struct {
	Status  int
	Message string
}

// ServeHTTP implements the interface
func (e ErrorHandler) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	SendError(w, e.Status, e.Message)
}
