# commons
[![pipeline status](https://gitlab.com/pgmtc-lib/commons/badges/master/pipeline.svg)](https://gitlab.com/pgmtc-lib/commons/-/commits/master)
[![coverage report](https://gitlab.com/pgmtc-lib/commons/badges/master/coverage.svg)](https://gitlab.com/pgmtc-lib/commons/-/commits/master)
[![go report card](https://goreportcard.com/badge/gitlab.com/pgmtc-lib/commons)](https://goreportcard.com/report/gitlab.com/pgmtc-lib/commons)

Set of common tools / utils  used in every day life

## arrays
* ContainsString(a []string, contains string) bool
* ContainsInt(a []int, contains int) bool
* ContainsInterface(a []interface{}, contains interface{}) bool
* IndexOfString(a []string, find string) int
* IndexOfInt(a []int, find int) int
* IndexOfInterface(a []interface{}, find interface{}) int
* FirstItemString(a []string) string
* MapString(a []string, mapFn func(string) string) []string
* FilterString(a []string, filterFn func(string) bool) []string
* TrimSpace(a []string) []string
* FilterOutEmpty(a []string) []string
* IntersectStrings(a []string, b []string) (result []string)

## bools
* ParseString(in string) bool
* Parse(in interface{}) bool
* Format(in bool) string

## clr
* Red(format, a... interface{}) string
* Green(format, a... interface{}) string
* Yellow(format, a... interface{}) string
* Blue(format, a... interface{}) string
* Purple(format, a... interface{}) string
* Cyan(format, a... interface{}) string
* White(format, a... interface{}) string

## contenttype
* FromFileName(filename string) string

## byts
* Format(in []byte, separator ...int) string
* Chunks(in []byte, chunkLength int) (result [][]byte)
* Reverse(in []byte) []byte 
* SetBit(n byte, posFromLBit uint) (result byte)
* SetBitIf(n byte, posFromLBit uint, condition bool) (result byte)
* ClearBit(n byte, posFromLBit uint) byte
* HasBit(n int, posFromLBit uint) bool
* PrintBits(b byte) string
* SplitByte(in byte, pos int) (result [2]int)
* JoinByte(num1 int, num2 int, pos int) (result byte)

## chans
* WaitWithTimeout(doneChan chan bool, timeout time.Duration) error
* WaitFor(responseChan chan interface{}, timeout time.Duration) (interface{}, error)

## crypt
* Encrypt(key, value string) (string, error)
* Decrypt(key, value string) (string, error)
* HashWithKey(key, value string) (string)
* GenerateToken(len int) (string)

## env
* GetEnvString(varName string, target *string)
* GetEnvBool(varName string, target *string)
* GetEnvInt(varName string, target *int)
* GetEnvArray(varName string, target *[]string)
* GetEnvMap(varName string, target *map\[string\]string)

## eventbus
* [Docs](eventbus/README.md)

## errs
* Wrap(err error, message string) (error)
* Log(err error, handler ...func(message))

## eval
* AnyStringEmpty(values ...string) bool
* AnyIntZero(values ...int) bool
* AnyNil(values ...interface{}) bool
* AnyFloatZero(values ...float64) bool

## fnct
* Name(in interface{})

## geo
* DistanceMetres(lat1, lng2, lat2, lng2) float64

## htutils
* FormParser.String(fieldName string, def string) string
* FormParser.MandString(fieldName string) (string, error)
* FormParser.Bool(fieldNamestring, def bool) bool
* FormParser.MandBool(fieldName string) (bool, error)
* FormParser.Int(fieldName string, def int) int
* FormParser.MandInt(fieldName string) (int, error)
* FormParser.Float(fieldName string, def float64) float64
* FormParser.MandFloat(fieldName string) (float64, err)
* HasGetParameter(r *http.Request, param string) bool
* GetParameterString(r *http.Request, param string) string
* GetParameterInt(r *http.Request, param string) int
* GetParameterTime(r *http.Request, param string) time.Time
* GetParameterFloat(r *http.Request, param string) float64
* GetParameterBool(r *http.Request, param string) bool
* SendSuccess(w http.responseWriter, data interface{})
* SendError(w http.responseWriter, httpStatus int, message string)
* SendUnauthorized(w http.ResponseWriter)
* SendForbidden(w http.ResponseWriter)
* SendResponse(w http.ResponseWriter, resp Response)
* ToJSON(in interface{}) (result []byte)
* ErrorHandler http.Handler
* PanicRecoveryMiddleWare(logFn func(message string, args ...interface{}))
* ProcessError(w http.ResponseWriter, err error)
* ProcessResponseOrError(w http.ResponseWriter) func(res interface{}, err error)
* ProcessErrorWithSuccess(w http.ResponseWriter, successResponse interface{}, err error)
* ProcessSuccessOrError(w http.ResponseWriter, successResponse interface{}) func(res interface{}, err error)

## ifs
* When(condition).ThenString(trueValue string, falseValue string) string
* When(condition).ThenInt(trueValue int, falseValue int) int
* When(condition).Then(trueFunc).Otherwise(falseFunc func())

## marshal
* TryJSON(in interface{}) []byte
* TryJSONPretty(in interface{}) (result []byte)
* JSONField(objMap map[string]json.RawMessage, field string, target interface{})
* ToRawJSONMap(bytes []byte) (result map[string]json.RawMessage, resultErr error)

## nets
* GetHost(in net.Addr) string
* MyIP() string

## nums
* RandomInt(min, max int) int 
* InBoundariesInt(a, min, max int) int
* InBoundariesFloat(a, min, max float64) float64
* If0ThenInt(a, then int) int 
* If0ThenString(a int, then string) string
* If0ThenFloat(a float64, then float64) float64
* ParseString(a string, ifErrorThen int) int
* ParseBool(a bool) int
* MaxInt(values ...int) int
* MinInt(values ...int) int
* AbsInt(value) int
* AbsInt64(value) int64

## ptrs
* PtrInt(value int) *int
* PtrString(value string) *string
* PtrFloat64(value float64) *float64
* PtrTime(value time.Time) *time.Time
* PtrDuration(value time.Duration) *time.Duration
* PtrBool(value bool) *bool
* FromPtrOrNilString(value *string) string
* PtrOrNilString(value string) *string
* FromPtrOrNilTime(value *time.Time) time.Time
* PtrOrNilTime(value time.Time) *time.Time

## run
* IfErrorThenExit(err error, message string, logHandler ...func(string, ...interface{}))
* CatchInterruptSignal(handler func())
* Periodic(freq time.Duration, startNow bool, handler func()) (cancel func())
* HandlePanic(message string, messageHandler func(format string))
* StartSameTime(runner ...func(){})

## serve
* StaticServer(opts ...Option) svc.Runner
  * WithAddr(":8080")
  * WithContent(myFS, "my-fs-path")
  * WithExposedEnvVariables("API_ADDR", "HOME", "MY_ENV_VARIABLE")


## sqldb
* Select(db *sql.DB, query Query) (resultErr error)
* Insert(db *sql.DB, query Query) (id interface{}, resultErr error)

## strs
* RandomString(length int) string
* RandomStringFromLetters(length int, letters []rune) string
* IfEmptyThen(inValue, defValue string, ifNotEmptyValues ...[]string) string
* IfNotEmptyThen(inValue, trueValue, ifEmptyValues ...[]string) string
* StartsWith(value string, startsWith []string) (result bool)
* Reverse(value string) (result string)
* Indent(value string, with string) (result string) 
* ParseString(value interface{}, ifEmptyThen string) (result string)
* Truncate(value string, maxLength int, addSuffix ...string) (result string)
* UnParse(value string) (result interface{})
* TruncateAndPadLeft(value string, length int) (result string)
* JoinNotEmpty(elems []string, sep string) string
* Plural(i int, unit string) string

## svc
* svc.Run(runner) error

## tags
* NewTagParser(tagID string)
* TagParser.ToMap(structure interface{}) msp[string]interface{}
* TagParser.FromMap(s interface{}, m amp[string]interface{})
* TryGetFieldNameByTagValue(objPointer interface{}, tagID, tagValue string) string
* TryUnmarshalField(objPointer interface{}, fieldName string, value []byte) error
* TryUnmarshalFieldByTag(objPointer interface{}, tagName string, tagValue string, value []byte) error

## throttler
* New(New(flushInterval, expiryInterval time.Duration) (result Throttler)
* Throttler.Update(id interface{}, func()))

## times
* NewDate(elements ...int) time.Time
* TrimTime(val time.Time) time.Time  
* Today() time.Time 
* Tomorrow() time.Time
* NextHour() time.Time
* NextMinute() time.Time
* FormatISO(value time.Time) string
* FormatShort(value time.Time) string
* FormatDateTimeLocal(value time.Time) string
* Max(values ...time.Time) time.Time
* Min(values ...time.Time) time.Time
* FormatDurationApprox(diff time.Duration, whenNegative string)
* FormatFromNow(in *time.Time, future bool) string
* WeekdayFromMonday(t time.Time) int
* BOD(t time.Time) time.Time
* EOD(t time.Time) time.Time
* MOD(t time.Time) time.Time
* BOW(t time.Time) time.Time
* IsSameWeek(t1, t2 time.Time)
* IsSameDayTime(value, value2 time.Time) bool
* WeekdayFromMonday(t time.Time)
* Steps(t time.Time, interval time.Duration, count int) (result Times)
* Times.Sort() Times
* Times.Reverse() Times
* MonthBoundaries(value time.Time) (begLastMonth, begCurMonth, begNextMonth time.Time)

## tst
* TestNetPipe() (result net.Conn, responseChan chan []byte)
* AllEmpty(t assert.TestingT, in ...interface{}) bool
* AllNotEmpty(t assert.TestingT, in ...interface{}) bool
* AllTrue(t assert.TestingT, in ...bool) bool
* AllFalse(t assert.TestingT, in ...bool) bool