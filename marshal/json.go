package marshal

import "encoding/json"

// TryJSON tries to marshal into JSON, if fails, then fails silently
func TryJSON(in interface{}) (result []byte) {
	result = make([]byte, 0)
	result, _ = json.Marshal(in)
	return
}

// TryJSONPretty tries to marshal into JSON - the pretty way, if fails, then fails silently
func TryJSONPretty(in interface{}) (result []byte) {
	result = make([]byte, 0)
	result, _ = json.MarshalIndent(in, "", "  ")
	return
}

// JSONField tries to marshall field from json object - useful for custom unmarshalling (ex. TryUnmarshallField)
func JSONField(objMap map[string]json.RawMessage, field string, target interface{}) {
	jsonVal, ok := objMap[field]
	if !ok {
		return
	}
	_ = json.Unmarshal(jsonVal, target)
}

// ToRawJSONMap unmarshal bytes to Raw JSON map
func ToRawJSONMap(bytes []byte) (result map[string]json.RawMessage, resultErr error) {
	resultErr = json.Unmarshal(bytes, &result)
	return
}
