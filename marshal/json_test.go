package marshal

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestTryJSON(t *testing.T) {
	type testStruct struct {
		Value1 string
		Value2 int
	}
	in := testStruct{
		Value1: "test",
		Value2: 12,
	}
	assert.Equal(t, `{"Value1":"test","Value2":12}`, string(TryJSON(in)))
	// try failure
	assert.Equal(t, ``, string(TryJSON(make(chan bool))))

}
