package tags

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/pgmtc-lib/commons/ptrs"
	"reflect"
	"testing"
)

type testSubStruct struct {
	PropA string
	PropB string
}
type testStruct struct {
	Prop1  bool          `myTag:"prop1"`
	Prop2  string        `myTag:"prop2"`
	Prop3  int           `myTag:"prop3"`
	Prop4  testSubStruct `myTag:"prop4"`
	Prop5  string
	Prop6  string      `myTag:"prop6,omitempty"`
	Prop7  string      `myTag:"prop7,omitempty"`
	Prop8  string      `myTag:"prop8"`
	Prop9  interface{} `myTag:"prop9"`
	Prop10 *string     `myTag:"prop10"` // string pointer - filled in
	Prop11 *string     `myTag:"prop11"` // string pointer - empty
}

func TestParseTagAttributes(t *testing.T) {
	ap := NewTagParser("myTag")
	type args struct {
		structure interface{}
	}
	tests := []struct {
		name string
		args args
		want map[string]interface{}
	}{
		{
			name: "test",
			args: args{
				structure: testStruct{
					Prop1: true,
					Prop2: "test",
					Prop3: 3,
					Prop4: testSubStruct{
						PropA: "aa",
						PropB: "bb",
					},
					Prop5:  "test",
					Prop6:  "",     // should be ignored by omit empty
					Prop7:  "test", // value filled in, should not be ignored by omitempty
					Prop8:  "",     // value should be coming out as empty as there is no omitempty
					Prop9:  nil,
					Prop10: ptrs.PtrString("strpointer"),
					Prop11: nil,
				},
			},
			want: map[string]interface{}{
				"prop1": true,
				"prop2": "test",
				"prop3": 3,
				"prop4": testSubStruct{
					PropA: "aa",
					PropB: "bb",
				},
				"prop7":  "test",
				"prop8":  "",
				"prop9":  nil,
				"prop10": ptrs.PtrString("strpointer"),
				"prop11": ptrs.PtrOrNilString(""),
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := ap.ToMap(tt.args.structure)
			assert.Equal(t, tt.want, got)
		})
	}
}

func TestFillTagAttributes(t *testing.T) {
	ap := NewTagParser("myTag")
	to := testStruct{}
	from := map[string]interface{}{
		"prop1": true,
		"prop2": "test",
		"prop3": 3,
		"prop4": testSubStruct{
			PropA: "aa",
			PropB: "bb",
		},
		"unknownprop": "some value",
		"prop6":       "test6",
		"prop7":       "test7",
		"prop8":       "",
		"prop9":       nil,
		"prop10":      ptrs.PtrString("strpointer"),
		"prop11":      nil,
	}
	want := testStruct{
		Prop1: true,
		Prop2: "test",
		Prop3: 3,
		Prop4: testSubStruct{
			PropA: "aa",
			PropB: "bb",
		},
		Prop6:  "test6",
		Prop7:  "test7",
		Prop8:  "",
		Prop9:  nil,
		Prop10: ptrs.PtrString("strpointer"),
		Prop11: nil,
	}

	ap.FromMap(&to, from)
	if !reflect.DeepEqual(to, want) {
		t.Errorf("ToMap() = %v, want %v", to, want)
	}
}
