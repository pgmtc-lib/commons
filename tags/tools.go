package tags

import (
	"encoding/json"
	"reflect"
	"strings"
)

// TryUnmarshalFieldByTag tries unmarshal json value into field with tag having tag value
func TryUnmarshalFieldByTag(objPointer interface{}, tagName string, tagValue string, value []byte) (resultErr error) {
	if fieldName := TryGetFieldNameByTagValue(objPointer, tagName, tagValue); fieldName != "" {
		resultErr = TryUnmarshalField(objPointer, fieldName, value)
	}
	return
}

// TryUnmarshalField tries unmarshal
func TryUnmarshalField(objPointer interface{}, fieldName string, value []byte) (resultErr error) {
	if objPointer == nil || fieldName == "" {
		return
	}
	structValue := reflect.ValueOf(objPointer).Elem()
	structFieldValue := structValue.FieldByName(fieldName)
	targetValue := structFieldValue.Addr().Interface()
	resultErr = json.Unmarshal(value, targetValue)
	return
}

// TryGetFieldNameByTagValue returns first field with tagValue equaling requested
func TryGetFieldNameByTagValue(objPointer interface{}, tagID, tagValue string) (fieldName string) {
	if objPointer == nil || tagID == "" {
		return
	}
	t := reflect.TypeOf(objPointer).Elem()
	for i := 0; i < t.NumField(); i++ {
		fieldType := t.Field(i)
		attrTagValue := fieldType.Tag.Get(tagID)
		attrTagSplit := strings.Split(attrTagValue, ",")
		attrTagValueValue := attrTagSplit[0]
		if attrTagValueValue == tagValue {
			return fieldType.Name
		}
	}
	return ""
}
