package tags

import (
	"fmt"
	"log"
	"reflect"
	"strings"
)

// TagParser is an interface
type TagParser interface {
	ToMap(s interface{}) map[string]interface{}
	FromMap(s interface{}, m map[string]interface{})
}

type tagParser struct{ tagID string }

// NewTagParser is a constructor for tag parser
func NewTagParser(tagID string) TagParser {
	return &tagParser{
		tagID: tagID,
	}
}

// ToMap returns map of tag values from tag stored in tagParser.tagID  annotations on struct
func (a *tagParser) ToMap(structure interface{}) map[string]interface{} {
	tagValues := make(map[string]interface{})
	t := reflect.TypeOf(structure)
	v := reflect.ValueOf(structure)
	for i := 0; i < t.NumField(); i++ {
		fieldType := t.Field(i)
		fieldValue := v.Field(i)
		attrTagValue := fieldType.Tag.Get(a.tagID)
		attrTagSplit := strings.Split(attrTagValue, ",")
		attrIDValue := attrTagSplit[0]
		if attrIDValue != "" {
			// tag is defined ...
			// ignore for strings which have omitempty
			if fieldValue.Kind() == reflect.String && len(attrTagSplit) > 1 && attrTagSplit[1] == "omitempty" && fieldValue.String() == "" {

				continue
			}
			tagValues[attrIDValue] = fieldValue.Interface()
		}
	}
	return tagValues
}

// FillTags fills tagged interface from a map
func (a *tagParser) FromMap(s interface{}, m map[string]interface{}) {
	// build map
	tagToFieldNameMap := make(map[string]string)
	t := reflect.TypeOf(s)
	x := t.Elem()
	for i := 0; i < x.NumField(); i++ {
		fieldType := x.Field(i)
		fieldName := fieldType.Name
		attrTagValue := fieldType.Tag.Get(a.tagID)
		attrTagSplit := strings.Split(attrTagValue, ",")
		attrIDValue := attrTagSplit[0]
		tagToFieldNameMap[attrIDValue] = fieldName
	}

	for k, v := range m {
		if fieldName, ok := tagToFieldNameMap[k]; ok {
			err := a.setField(s, fieldName, v)
			if err != nil {
				log.Printf("can't parse %s: %s", k, err.Error())
			}
		}
	}
}

func (a *tagParser) setField(obj interface{}, name string, value interface{}) error {
	structValue := reflect.ValueOf(obj).Elem()
	structFieldValue := structValue.FieldByName(name)

	if !structFieldValue.IsValid() {
		return fmt.Errorf("no such field: %s in obj", name)
	}

	if !structFieldValue.CanSet() {
		return fmt.Errorf("cannot set %s field value", name)
	}

	structFieldType := structFieldValue.Type()
	if value != nil {
		val := reflect.ValueOf(value)
		if structFieldType != val.Type() {
			return fmt.Errorf("provided value %v type didn't match obj field %s type", val, name)
		}
		structFieldValue.Set(val)
	}
	return nil
}
