package tags

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/pgmtc-lib/commons/ptrs"
	"strconv"
	"testing"
	"time"
)

func TestTryUnmarshalFieldByTag(t *testing.T) {
	type testStruct struct {
		Field1 string `attrid:"field1"`
		Field2 int    `attrid:"field2"`
	}
	out := testStruct{}
	assert.NoError(t, TryUnmarshalFieldByTag(&out, "attrid", "field1", []byte("\"value 1\"")))
	assert.NoError(t, TryUnmarshalFieldByTag(&out, "attrid", "field2", []byte(strconv.Itoa(31415926))))
	assert.NoError(t, TryUnmarshalFieldByTag(&out, "attrid", "field3", []byte(strconv.Itoa(31415926)))) // test invalid
	expected := testStruct{
		Field1: "value 1",
		Field2: 31415926,
	}
	assert.Equal(t, expected, out)
}

func TestTryUnmarshalField(t *testing.T) {
	type subScruct struct {
		Name  string
		Value string
	}
	type testStruct struct {
		Field1 string
		Field2 int
		Field3 float64
		Field4 time.Time
		Field5 *string
		Field6 subScruct
	}
	out := testStruct{}
	assert.NoError(t, TryUnmarshalField(&out, "Field1", []byte("\"value 1\"")))
	assert.NoError(t, TryUnmarshalField(&out, "Field2", []byte(strconv.Itoa(31415926))))
	assert.NoError(t, TryUnmarshalField(&out, "Field3", []byte("1.23456789")))
	assert.NoError(t, TryUnmarshalField(&out, "Field4", []byte("\"2022-03-16T01:02:03.000000Z\"")))
	assert.NoError(t, TryUnmarshalField(&out, "Field5", []byte("\"value 5\"")))
	assert.NoError(t, TryUnmarshalField(&out, "Field6", []byte("{\"name\":\"my name\",\"value\":\"my value\"}")))
	expected := testStruct{
		Field1: "value 1",
		Field2: 31415926,
		Field3: 1.23456789,
		Field4: time.Date(2022, 03, 16, 1, 2, 3, 0, time.UTC),
		Field5: ptrs.PtrString("value 5"),
		Field6: subScruct{
			Name:  "my name",
			Value: "my value",
		},
	}
	assert.Equal(t, expected, out)
}

func TestGetFieldNameFromTagValue(t *testing.T) {
	type testStruct struct {
		Field1 string `attrid:"field1"`
		Field2 int    `attrid:"field2"`
	}
	in := testStruct{
		Field1: "value 1",
		Field2: 10,
	}
	assert.Equal(t, "Field2", TryGetFieldNameByTagValue(&in, "attrid", "field2"))
	assert.Equal(t, "Field1", TryGetFieldNameByTagValue(&in, "attrid", "field1"))
	assert.Empty(t, TryGetFieldNameByTagValue(&in, "attrid", "field3"))
	assert.Empty(t, TryGetFieldNameByTagValue(&in, "anothrerattrid", "field1"))
	assert.Empty(t, TryGetFieldNameByTagValue(nil, "attrid", "field2"))
	assert.Empty(t, TryGetFieldNameByTagValue(&in, "", "field2"))
}
