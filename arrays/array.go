package arrays

import "strings"

// ContainsString tells whether a contains x.
func ContainsString(a []string, contains string) bool {
	for _, n := range a {
		if contains == n {
			return true
		}
	}
	return false
}

// ContainsInt tells whether a contains x.
func ContainsInt(a []int, contains int) bool {
	for _, n := range a {
		if contains == n {
			return true
		}
	}
	return false
}

// ContainsInterface tells whether a contains x.
func ContainsInterface(a []interface{}, contains interface{}) bool {
	for _, n := range a {
		if contains == n {
			return true
		}
	}
	return false
}

// IndexOfString returns index of item
func IndexOfString(a []string, find string) int {
	for i, n := range a {
		if find == n {
			return i
		}
	}
	return -1
}

// IndexOfInt returns index of item
func IndexOfInt(a []int, find int) int {
	for i, n := range a {
		if find == n {
			return i
		}
	}
	return -1
}

// IndexOfInterface returns index of item
func IndexOfInterface(a []interface{}, find interface{}) int {
	for i, n := range a {
		if find == n {
			return i
		}
	}
	return -1
}

// FirstItemString grabs first item from string array
func FirstItemString(a []string) string {
	if len(a) > 0 {
		return a[0]
	}
	return ""
}

// MapString maps items in array to another array
func MapString(a []string, mapFn func(string) string) (result []string) {
	result = make([]string, len(a))
	for i := 0; i < len(result); i++ {
		result[i] = mapFn(a[i])
	}
	return
}

// FilterString returns only items which return true from filterFn
func FilterString(a []string, filterFn func(string) bool) (result []string) {
	for i := 0; i < len(a); i++ {
		if filterFn(a[i]) {
			result = append(result, a[i])
		}
	}
	return
}

// TrimSpace trims whitespace from every item in slice using strings.TrimSpace() under the hood
func TrimSpace(a []string) (result []string) {
	return MapString(a, strings.TrimSpace)
}

// FilterOutEmpty returns only items which are not empty
func FilterOutEmpty(a []string) []string {
	return FilterString(a, func(s string) bool {
		return s != ""
	})
}

// IntersectStrings returns array of items contained in both a and b
func IntersectStrings(a []string, b []string) (result []string) {
	result = make([]string, 0)
	var iter []string
	matchMap := make(map[string]struct{})
	if len(b) <= len(a) {
		iter = b
		for _, item := range a {
			matchMap[item] = struct{}{}
		}
	} else {
		iter = a
		for _, item := range b {
			matchMap[item] = struct{}{}
		}
	}
	for _, item := range iter {
		if _, ok := matchMap[item]; ok {
			result = append(result, item)
		}
	}
	return
}
