package arrays

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/pgmtc-lib/commons/ptrs"
	"reflect"
	"strings"
	"testing"
	"time"
)

func TestArrayContains(t *testing.T) {
	type args struct {
		a []string
		x string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "test-contain",
			args: args{
				a: []string{"element1", "element2", "element3"},
				x: "element1",
			},
			want: true,
		},
		{
			name: "test-nocontain",
			args: args{
				a: []string{"element1", "element2", "element3"},
				x: "element4",
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ContainsString(tt.args.a, tt.args.x); got != tt.want {
				t.Errorf("ContainsString() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestArrayIntContains(t *testing.T) {
	type args struct {
		a []int
		x int
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "test-contain",
			args: args{
				a: []int{1, 2, 3},
				x: 2,
			},
			want: true,
		},
		{
			name: "test-nocontain",
			args: args{
				a: []int{1, 2, 3},
				x: 4,
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ContainsInt(tt.args.a, tt.args.x); got != tt.want {
				t.Errorf("ContainsInt() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestArrayFindString(t *testing.T) {
	type args struct {
		a []string
		x string
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "find-success",
			args: args{
				a: []string{"element1", "element2", "element3"},
				x: "element2",
			},
			want: 1,
		},
		{
			name: "find-nosuccess",
			args: args{
				a: []string{"element1", "element2", "element3"},
				x: "element4",
			},
			want: -1,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := IndexOfString(tt.args.a, tt.args.x); got != tt.want {
				t.Errorf("IndexOfString() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestArrayFindInt(t *testing.T) {
	type args struct {
		a []int
		x int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "find-success",
			args: args{
				a: []int{10, 20, 30},
				x: 20,
			},
			want: 1,
		},
		{
			name: "find-nosuccess",
			args: args{
				a: []int{10, 20, 30},
				x: 40,
			},
			want: -1,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := IndexOfInt(tt.args.a, tt.args.x); got != tt.want {
				t.Errorf("IndexOfInt() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFirstItemString(t *testing.T) {
	tests := []struct {
		name string
		in   []string
		want string
	}{
		{
			name: "test",
			in:   []string{"first", "second", "third"},
			want: "first",
		},
		{
			name: "test",
			in:   []string{"first"},
			want: "first",
		},
		{
			name: "empty",
			in:   nil,
			want: "",
		},
		{
			name: "empty-2",
			in:   []string{},
			want: "",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := FirstItemString(tt.in); got != tt.want {
				t.Errorf("FirstItemString() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestArrayContainsInterface(t *testing.T) {
	type args struct {
		a        []interface{}
		contains interface{}
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			args: args{
				a:        []interface{}{1, "second", time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC)},
				contains: 1,
			},
			want: true,
		},
		{
			args: args{
				a:        []interface{}{1, "second", time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC)},
				contains: "second",
			},
			want: true,
		},
		{
			args: args{
				a:        []interface{}{1, "second", time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC)},
				contains: time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC),
			},
			want: true,
		},
		{
			args: args{
				a:        []interface{}{1, "second", time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC)},
				contains: 2,
			},
			want: false,
		},
		{
			args: args{
				a:        []interface{}{1, "second", time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC)},
				contains: "third",
			},
			want: false,
		},
		{
			name: "",
			args: args{
				a:        []interface{}{1, "second", time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC)},
				contains: time.Date(2021, 1, 1, 0, 0, 0, 0, time.UTC),
			},
			want: false,
		},
		{
			args: args{
				a:        []interface{}{1, "second", time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC)},
				contains: nil,
			},
			want: false,
		},
		{
			args: args{
				a:        []interface{}{1, "second", nil, time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC)},
				contains: nil,
			},
			want: true,
		},
		{
			args: args{
				a:        []interface{}{1, "second", nil, time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC), ptrs.PtrString("third")},
				contains: "third",
			},
			want: false,
		},
		{
			args: args{
				a:        []interface{}{1, "second", nil, time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC), ptrs.PtrString("third")},
				contains: ptrs.PtrString("third"),
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ContainsInterface(tt.args.a, tt.args.contains); got != tt.want {
				t.Errorf("ContainsInterface() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestArrayIndexOfInterface(t *testing.T) {
	tests := []struct {
		name string
		a    []interface{}
		find interface{}
		want int
	}{
		{
			a:    []interface{}{1, "second", time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC), ptrs.PtrString("third"), nil, ptrs.PtrInt(4)},
			find: 1,
			want: 0,
		},
		{
			a:    []interface{}{1, "second", time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC), ptrs.PtrString("third"), nil, ptrs.PtrInt(4)},
			find: "second",
			want: 1,
		},
		{
			a:    []interface{}{1, "second", time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC), ptrs.PtrString("third"), nil, ptrs.PtrInt(4)},
			find: time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC),
			want: 2,
		},
		{
			a:    []interface{}{1, "second", time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC), ptrs.PtrString("third"), nil, ptrs.PtrInt(4)},
			find: ptrs.PtrString("third"),
			want: -1,
		},
		{
			a:    []interface{}{1, "second", time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC), ptrs.PtrString("third"), nil, ptrs.PtrInt(4)},
			find: nil,
			want: 4,
		},
		{
			a:    []interface{}{1, "second", time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC), ptrs.PtrString("third"), nil, ptrs.PtrInt(4)},
			find: ptrs.PtrInt(4),
			want: -1,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := IndexOfInterface(tt.a, tt.find); got != tt.want {
				t.Errorf("IndexOfInterface() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTrimSpace(t *testing.T) {
	in := []string{
		"string",
		"   string   ",
		"\n\tstring\t\n  ",
	}
	expected := []string{
		"string",
		"string",
		"string",
	}
	assert.Equal(t, expected, TrimSpace(in))
}

func TestMapString(t *testing.T) {
	in := []string{
		"string",
		"string ",
		"\n\tstring\t\n",
	}
	expected := []string{
		"string+1",
		"string +1",
		"\n\tstring\t\n+1",
	}
	assert.Equal(t, expected, MapString(in, func(s string) string {
		return s + "+1"
	}))
}

func TestFilterString(t *testing.T) {
	in := []string{
		"aaa",
		"aaa bbb ccc",
		"bbb ccc",
		"aaa ccc",
	}
	expected := []string{
		"aaa bbb ccc",
		"bbb ccc",
	}
	assert.Equal(t, expected, FilterString(in, func(s string) bool {
		return strings.ContainsAny(s, "bbb")
	}))
}

func TestFilterOutEmpty(t *testing.T) {
	in := []string{
		"",
		"aaa bbb ccc",
		"bbb ccc",
		"",
		"aaa ccc",
		"",
	}
	expected := []string{
		"aaa bbb ccc",
		"bbb ccc",
		"aaa ccc",
	}
	assert.Equal(t, expected, FilterOutEmpty(in))
}

func TestIntersectStrings(t *testing.T) {
	tests := []struct {
		name       string
		a          []string
		b          []string
		wantResult []string
	}{
		{
			name:       "",
			a:          []string{"a", "b", "c", "d"},
			b:          []string{"b", "d", "f"},
			wantResult: []string{"b", "d"},
		},
		{
			name:       "",
			a:          []string{"a", "b", "c", "d"},
			b:          []string{"a", "b", "c", "d"},
			wantResult: []string{"a", "b", "c", "d"},
		},
		{
			name:       "",
			a:          []string{"a", "b", "c", "d"},
			b:          []string{"e", "f", "g", "h"},
			wantResult: []string{},
		},
		{
			name:       "",
			a:          []string{"a"},
			b:          []string{},
			wantResult: []string{},
		},
		{
			name:       "",
			a:          []string{},
			b:          []string{"b"},
			wantResult: []string{},
		},
		{
			name:       "",
			a:          []string{"b"},
			b:          []string{"b", "c", "d"},
			wantResult: []string{"b"},
		},
		{
			name:       "",
			a:          []string{"b"},
			b:          []string{"b"},
			wantResult: []string{"b"},
		},
		{
			name:       "",
			a:          nil,
			b:          nil,
			wantResult: []string{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotResult := IntersectStrings(tt.a, tt.b); !reflect.DeepEqual(gotResult, tt.wantResult) {
				t.Errorf("IntersectStrings() = %v, want %v", gotResult, tt.wantResult)
			}
		})
	}
}
