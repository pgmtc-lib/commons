package crypt

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestGenerateToken(t *testing.T) {

	tests := []struct {
		name   string
		length int
	}{
		{name: "test-32", length: 32},
		{name: "test-64", length: 64},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := GenerateToken(tt.length)
			fmt.Println(got)
			assert.NotEmpty(t, got)
			//assert.Len(t, got, tt.length)
		})
	}
}
