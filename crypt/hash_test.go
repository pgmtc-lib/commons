package crypt

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestHashWithKey(t *testing.T) {
	assert.Equal(t, "b613679a0814d9ec772f95d778c35fc5ff1697c493715653c6c712144292c5ad", HashWithKey("", ""))
	assert.Equal(t, "f026fae0f6ff17ec396a085058c8288ccb36bd90a01db31244d3d59843dd3971", HashWithKey("", "some string"))
	assert.Equal(t, "bb1b796dce90e272d4453804b207efc2b23993bb031c7ba194c6a587f807bd22", HashWithKey("key1", "some string"))
	assert.Equal(t, "c9c724e7079a0cb5b02aba3cd88d74578275ecf6c0b13a1c367f9e2acb091ba1", HashWithKey("key2", "some string"))
}
