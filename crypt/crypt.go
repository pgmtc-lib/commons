package crypt

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"encoding/base64"
	"encoding/hex"
	"fmt"

	"errors"

	"io"
)

// Encrypt encrypts string value using key
func Encrypt(key, value string) (string, error) {
	keyBytes := []byte(key)
	valueBytes := []byte(value)
	block, err := aes.NewCipher(keyBytes)
	if err != nil {
		return "", err
	}
	b := base64.StdEncoding.EncodeToString(valueBytes)
	ciphertext := make([]byte, aes.BlockSize+len(b))
	iv := ciphertext[:aes.BlockSize]
	if _, err := io.ReadFull(rand.Reader, iv); err != nil {
		return "", err
	}
	cfb := cipher.NewCFBEncrypter(block, iv)
	cfb.XORKeyStream(ciphertext[aes.BlockSize:], []byte(b))
	encoded := fmt.Sprintf("%0x", ciphertext)
	return encoded, nil
}

// Decrypt decrypts string value using key
func Decrypt(key string, value string) (string, error) {
	keyBytes := []byte(key)
	valueBytes, err := hex.DecodeString(value)
	if err != nil {
		return "", err
	}
	block, err := aes.NewCipher(keyBytes)
	if err != nil {
		return "", err
	}
	if len(valueBytes) < aes.BlockSize {
		return "", errors.New("ciphertext too short")
	}
	iv := valueBytes[:aes.BlockSize]
	valueBytes = valueBytes[aes.BlockSize:]
	cfb := cipher.NewCFBDecrypter(block, iv)
	cfb.XORKeyStream(valueBytes, valueBytes)
	data, err := base64.StdEncoding.DecodeString(string(valueBytes))
	if err != nil {
		return "", err
	}
	return string(data), nil
}
