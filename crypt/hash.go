package crypt

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
)

// HashWithKey creates hash using provided key, same key and value always ends up with the same result
func HashWithKey(key string, data string) (result string) {
	h := hmac.New(sha256.New, []byte(key))
	h.Write([]byte(data))
	result = hex.EncodeToString(h.Sum(nil))
	return
}
