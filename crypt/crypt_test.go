package crypt

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/pgmtc-lib/commons/strs"
	"testing"
)

func TestEncrypt(t *testing.T) {
	key := strs.RandomString(16)
	toEncrypt := strs.RandomString(100)
	encrypted, err := Encrypt(key, toEncrypt)
	assert.NoError(t, err)
	decrypted, err := Decrypt(key, encrypted)
	assert.NoError(t, err)
	assert.Equal(t, toEncrypt, decrypted)
}

func TestEncrypt_Errors(t *testing.T) {
	_, err := Encrypt("", "something")
	assert.Error(t, err)
	_, err = Decrypt("", "something")
	assert.Error(t, err)
	_, err = Decrypt(strs.RandomString(16), "")
	assert.Error(t, err)
	_, err = Decrypt("somecrap", "something")
	assert.Error(t, err)
}
