package clr

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestColors(t *testing.T) {
	assert.Equal(t, Red("value"), "\033[31mvalue\033[0m")
	assert.Equal(t, Green("value"), "\033[32mvalue\033[0m")
	assert.Equal(t, Yellow("value"), "\033[33mvalue\033[0m")
	assert.Equal(t, Blue("value"), "\033[34mvalue\033[0m")
	assert.Equal(t, Purple("value"), "\033[35mvalue\033[0m")
	assert.Equal(t, Cyan("value"), "\033[36mvalue\033[0m")
	assert.Equal(t, White("value"), "\033[37mvalue\033[0m")
	fmt.Println(Red("red text"))
	fmt.Println(Green("green text"))
	fmt.Println(Yellow("yellow text"))
	fmt.Println(Blue("blue text"))
	fmt.Println(Purple("purple text"))
	fmt.Println(Cyan("cyan text"))
	fmt.Println(White("white text"))
}
