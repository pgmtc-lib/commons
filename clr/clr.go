package clr

import "fmt"

// Red returns string in red
func Red(format string, a ...interface{}) string {
	return fmt.Sprintf("%s%s%s", "\033[31m", fmt.Sprintf(format, a...), "\033[0m")
}

// Green returns string in green
func Green(format string, a ...interface{}) string {
	return fmt.Sprintf("%s%s%s", "\033[32m", fmt.Sprintf(format, a...), "\033[0m")
}

// Yellow returns string in yellow
func Yellow(format string, a ...interface{}) string {
	return fmt.Sprintf("%s%s%s", "\033[33m", fmt.Sprintf(format, a...), "\033[0m")
}

// Blue returns string in blue
func Blue(format string, a ...interface{}) string {
	return fmt.Sprintf("%s%s%s", "\033[34m", fmt.Sprintf(format, a...), "\033[0m")
}

// Purple returns string in purple
func Purple(format string, a ...interface{}) string {
	return fmt.Sprintf("%s%s%s", "\033[35m", fmt.Sprintf(format, a...), "\033[0m")
}

// Cyan returns string in cyan
func Cyan(format string, a ...interface{}) string {
	return fmt.Sprintf("%s%s%s", "\033[36m", fmt.Sprintf(format, a...), "\033[0m")
}

// White returns string in white
func White(format string, a ...interface{}) string {
	return fmt.Sprintf("%s%s%s", "\033[37m", fmt.Sprintf(format, a...), "\033[0m")
}
