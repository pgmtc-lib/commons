package ifs

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/pgmtc-lib/commons/ifs/use"
	"testing"
	"time"
)

func TestEvaluator_Test(t *testing.T) {
	// test one liner evaluation
	assert.Equal(t, "YES", When(true).Then(func() interface{} { return "YES" }).Otherwise(func() interface{} { return "NO" }).Result())
	assert.Equal(t, "NO", When(false).Then(func() interface{} { return "YES" }).Otherwise(func() interface{} { return "NO" }).Result())
	assert.Equal(t, 10, When(true).Then(func() interface{} { return 10 }).Otherwise(func() interface{} { return 22.5 }).Result())
	assert.Equal(t, 22.5, When(false).Then(func() interface{} { return 10 }).Otherwise(func() interface{} { return 22.5 }).Result())

	assert.Equal(t, "YES", When(true).Then(use.String("YES")).Otherwise(use.String("NO")).Result())
	assert.Equal(t, 20, When(false).Then(use.Int(10)).Otherwise(use.Int(20)).Result())
	assert.Equal(t, true, When(true).Then(use.Bool(true)).Otherwise(use.Bool(false)).Result())
	assert.Equal(t, false, When(false).Then(use.Bool(true)).Otherwise(use.Bool(false)).Result())
	assert.Equal(t, 10.0, When(true).Then(use.Float64(10.0)).Otherwise(use.Float64(20.0)).Result())
	assert.Equal(t, 20.0, When(false).Then(use.Float64(10.0)).Otherwise(use.Float64(20.0)).Result())
	assert.Equal(t, time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC), When(true).Then(use.Time(time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC))).Otherwise(use.Time(time.Date(2021, 1, 1, 0, 0, 0, 0, time.UTC))).Result())
	assert.Equal(t, time.Date(2021, 1, 1, 0, 0, 0, 0, time.UTC), When(false).Then(use.Time(time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC))).Otherwise(use.Time(time.Date(2021, 1, 1, 0, 0, 0, 0, time.UTC))).Result())

	var ran bool
	When(true).Then(Run(func() { ran = true }))
	assert.True(t, ran)

	// test result getters
	var strResult string = When(true).Then(func() interface{} { return "haha" }).String()
	assert.Equal(t, "haha", strResult)
	strResult = When(false).Then(func() interface{} { return "hihi" }).String()
	assert.Equal(t, "", strResult)
	var intResult = When(true).Then(func() interface{} { return 10 }).Int()
	assert.Equal(t, 10, intResult)
	intResult = When(false).Then(func() interface{} { return 20 }).Int()
	assert.Equal(t, 0, intResult)
	var boolResult = When(true).Then(func() interface{} { return true }).Bool()
	assert.Equal(t, true, boolResult)
	boolResult = When(false).Then(func() interface{} { return false }).Bool()
	assert.Equal(t, false, boolResult)
	var floatResult = When(true).Then(func() interface{} { return 10.0 }).Float64()
	assert.Equal(t, 10.0, floatResult)
	floatResult = When(false).Then(func() interface{} { return 10.0 }).Float64()
	assert.Equal(t, 0.0, floatResult)
	var timeResult = When(true).Then(func() interface{} { return time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC) }).Time()
	assert.Equal(t, time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC), timeResult)
	timeResult = When(false).Then(func() interface{} { return time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC) }).Time()
	assert.Equal(t, time.Time{}, timeResult)

	assert.Equal(t, "YES", When(true).Then(func() interface{} { return "YES" }).Otherwise(func() interface{} { return "NO" }).Result())
}

func TestEvaluator_ThenString(t *testing.T) {
	var trueRun, falseRun, trueRun2, falseRun2 bool
	var trueFunc = func() { trueRun = true }
	var falseFunc = func() { falseRun = true }
	var trueFunc2 = func() { trueRun2 = true }
	var falseFunc2 = func() { falseRun2 = true }
	// test true
	in := When(true)
	assert.Equal(t, 1, in.ThenInt(1, 0))
	in.then(trueFunc).otherwise(falseFunc)
	in.otherwise(falseFunc2).then(trueFunc2) // try in reverse
	assert.True(t, trueRun)
	assert.False(t, falseRun)
	assert.True(t, trueRun2)
	assert.False(t, falseRun2)
	// test false
	trueRun, falseRun, trueRun2, falseRun2 = false, false, false, false
	in = When(false)
	assert.Equal(t, 0, in.ThenInt(1, 0))
	in.then(trueFunc).otherwise(falseFunc)
	in.otherwise(falseFunc2).then(trueFunc2) // try in reverse
	assert.False(t, trueRun)
	assert.True(t, falseRun)
	assert.False(t, trueRun2)
	assert.True(t, falseRun2)
}

func TestEvaluator_StringArray(t *testing.T) {
	eval := Evaluator{
		result: []string{"val1", "val2", "val3"},
	}
	got := eval.StringArray()
	assert.Equal(t, eval.result, got)
	// test invalid
	eval = Evaluator{
		result: "string",
	}
	got = eval.StringArray()
	assert.Equal(t, []string{}, got)

}
