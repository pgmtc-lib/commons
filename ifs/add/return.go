package add

// String is helper for Then functions
func String(target []string, value string) func() interface{} {
	return func() interface{} {
		return append(target, value)
	}
}
