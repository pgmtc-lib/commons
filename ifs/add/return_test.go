package add

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestString(t *testing.T) {
	var target []string
	got := String(target, "string1")()
	assert.Equal(t, []string{"string1"}, got)
	got = String(got.([]string), "string2")()
	assert.Equal(t, []string{"string1", "string2"}, got)
}
