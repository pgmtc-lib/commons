package ifs

import (
	"fmt"
	"time"
)

// Evaluator is a strucg for evaluator
type Evaluator struct {
	condition bool
	result    interface{}
}

// When is a constructor for evaluator
func When(condition bool) Evaluator {
	return Evaluator{condition: condition}
}

// Result returns result as interface
func (w Evaluator) Result() interface{} {
	return w.result
}

// String returns result as string
func (w Evaluator) String() string {
	if w.result != nil {
		return fmt.Sprintf("%s", w.result)
	}
	return ""
}

// Int returns result as int
func (w Evaluator) Int() int {
	if intResult, ok := w.result.(int); ok {
		return intResult
	}
	return 0
}

// Bool returns result ad bool
func (w Evaluator) Bool() bool {
	if boolResult, ok := w.result.(bool); ok {
		return boolResult
	}
	return false
}

// Float64 returns result as float64
func (w Evaluator) Float64() float64 {
	if float64Result, ok := w.result.(float64); ok {
		return float64Result
	}
	return 0.0
}

// Time returns result as time
func (w Evaluator) Time() time.Time {
	if timeResult, ok := w.result.(time.Time); ok {
		return timeResult
	}
	return time.Time{}
}

// StringArray returns result as array of strings
func (w Evaluator) StringArray() []string {
	if strArrResult, ok := w.result.([]string); ok {
		return strArrResult
	}
	return []string{}
}

// ThenInt returns trueValue if condition true, otherwise falseValue
func (w Evaluator) ThenInt(trueValue, falseValue int) int {
	if w.condition {
		return trueValue
	}
	return falseValue
}

// Then evaluates true function and stores the result
func (w Evaluator) Then(trueFunc func() interface{}) Evaluator {
	w.then(func() {
		w.result = trueFunc()
	})
	return w
}

// Otherwise evaluates true function and stores the result
func (w Evaluator) Otherwise(falseFunc func() interface{}) Evaluator {
	w.otherwise(func() {
		w.result = falseFunc()
	})
	return w
}

// then is a generic function, run only when evaluation is true
func (w Evaluator) then(trueFunc func()) Evaluator {
	if w.condition {
		trueFunc()
	}
	return w
}

// otherwise is a generic function, run only when evaluation is false
func (w Evaluator) otherwise(falseFunc func()) Evaluator {
	if !w.condition {
		falseFunc()
	}
	return w
}

// Run is helper for Then functions
func Run(handler func()) func() interface{} {
	return func() interface{} {
		handler()
		return nil
	}
}
