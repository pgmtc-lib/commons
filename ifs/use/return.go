package use

import "time"

// String is helper for Then functions
func String(value string) func() interface{} {
	return func() interface{} {
		return value
	}
}

// Int is helper for Then functions
func Int(value int) func() interface{} {
	return func() interface{} {
		return value
	}
}

// Bool is helper for Then functions
func Bool(value bool) func() interface{} {
	return func() interface{} {
		return value
	}
}

// Float64 is helper for Then functions
func Float64(value float64) func() interface{} {
	return func() interface{} {
		return value
	}
}

// Time is helper for Then functions
func Time(value time.Time) func() interface{} {
	return func() interface{} {
		return value
	}
}
