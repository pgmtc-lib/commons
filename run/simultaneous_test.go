package run

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/pgmtc-lib/commons/nums"
	"runtime"
	"testing"
	"time"
)

func TestStartSameTime(t *testing.T) {
	var tm1, tm0 int64
	var fn1 = func() {
		tm1 = time.Now().UnixNano()
	}
	var fn2, fn3, fn4, fn5, fn6, fn7, fn8, fn9 = func() {}, func() {}, func() {}, func() {}, func() {}, func() {}, func() {}, func() {}

	var fn0 = func() {
		tm0 = time.Now().UnixNano()
	}

	go fn1()
	go fn2()
	go fn3()
	go fn4()
	go fn5()
	go fn6()
	go fn7()
	go fn8()
	go fn9()
	go fn0()
	time.Sleep(200 * time.Millisecond)
	diff1 := nums.AbsInt64(tm0 - tm1)

	StartSameTime(fn1, fn2, fn3, fn4, fn5, fn6, fn7, fn8, fn9, fn0)
	time.Sleep(200 * time.Millisecond)
	diff2 := nums.AbsInt64(tm0 - tm1)
	// this fails in cicd - probably due to the lower cpu cores, it is actually faster without this helper
	if runtime.NumCPU() > 1 {
		assert.Greater(t, diff1, diff2)
	}
	fmt.Printf("cores = %d, diff plain = %d, diff simultaneous = %d\n", runtime.NumCPU(), diff1, diff2)
}
