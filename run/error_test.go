package run

import (
	"bou.ke/monkey"
	"errors"
	"fmt"
	"github.com/stretchr/testify/assert"
	"os"
	"testing"
)

func TestCheckError_error(t *testing.T) {
	returnMessage := ""
	expectedMessage := "os.Exit called with"
	fakeExit := func(exitCode int) {
		returnMessage = expectedMessage
	}
	patch := monkey.Patch(os.Exit, fakeExit)
	defer patch.Unpatch()
	IfErrorThenExit(errors.New("problem"), "some error message")
	if returnMessage != expectedMessage {
		t.Errorf("message = %s, expected %s", returnMessage, expectedMessage)
	}
}

func TestCheckError_noerror(t *testing.T) {
	returnMessage := ""
	expectedMessage := "os.Exit called with"
	fakeExit := func(exitCode int) {
		returnMessage = expectedMessage
	}
	patch := monkey.Patch(os.Exit, fakeExit)
	defer patch.Unpatch()
	IfErrorThenExit(nil, "some error message")
	if returnMessage != "" {
		t.Errorf("message = %s, expected %s", returnMessage, "")
	}
}

func TestCheckError_logHandlers(t *testing.T) {
	returnMessage := ""
	expectedMessage := "some error message: problem"
	gotMessage := ""
	logHandler := func(message string, v ...interface{}) {
		gotMessage = fmt.Sprintf(message, v...)
	}
	fakeExit := func(exitCode int) {
		returnMessage = expectedMessage
	}
	patch := monkey.Patch(os.Exit, fakeExit)
	defer patch.Unpatch()

	IfErrorThenExit(errors.New("problem"), "some error message", logHandler)
	assert.Equal(t, expectedMessage, gotMessage)
	assert.Equal(t, expectedMessage, returnMessage)
}

func TestCheckError_logHandlersOverride(t *testing.T) {
	returnMessage := ""
	expectedMessage := "some error message: problem"
	gotMessage := ""
	LogFatalHandler = func(message string, v ...interface{}) {
		gotMessage = fmt.Sprintf(message, v...)
		os.Exit(1)
	}
	fakeExit := func(exitCode int) {
		returnMessage = expectedMessage
	}
	patch := monkey.Patch(os.Exit, fakeExit)
	defer patch.Unpatch()

	IfErrorThenExit(errors.New("problem"), "some error message")
	assert.Equal(t, expectedMessage, gotMessage)
	assert.Equal(t, expectedMessage, returnMessage)
}
