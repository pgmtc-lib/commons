package run

import "log"

// LogFatalHandler can be overridden if needed
var LogFatalHandler func(format string, v ...interface{}) = func(format string, v ...interface{}) {
	log.Fatalf(format, v...)
}

// IfErrorThenExit checks if error is not nil, prints message and exit
func IfErrorThenExit(err error, message string, logHandlers ...func(format string, v ...interface{})) {
	if err != nil {
		for _, logHandler := range logHandlers {
			logHandler(message+": %s", err.Error())
		}
		LogFatalHandler(message+": %s", err.Error())
	}
}
