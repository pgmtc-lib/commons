package run

import (
	"os"
	"os/signal"
	"syscall"
)

// CatchInterruptSignal listens for external interrupt signals, if happens, runs a handler
func CatchInterruptSignal(handler func()) {
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, os.Interrupt, syscall.SIGINT, syscall.SIGTERM, syscall.SIGKILL)
	go func() {
		<-sigs
		handler()
		signal.Stop(sigs)
	}()
}
