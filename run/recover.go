package run

import (
	"fmt"
	"runtime/debug"
)

// HandlePanic recovers from panic and prints the message
func HandlePanic(message string, messageHandler func(message string)) {
	if r := recover(); r != nil {
		messageHandler(fmt.Sprintf("%s recovered from: %s", message, r))
		debug.PrintStack()
	}
}
