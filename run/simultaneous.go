package run

import "sync"

// StartSameTime runs provided runners at the same time (or as close as possible)
func StartSameTime(runners ...func()) {
	var wg sync.WaitGroup
	wg.Add(len(runners))
	start := make(chan struct{})
	for i := 0; i < len(runners); i++ {
		runner := runners[i]
		go func() {
			<-start
			runner()
			defer wg.Done()
		}()
	}
	close(start)
	wg.Wait()
}
