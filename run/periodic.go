package run

import "time"

// Periodic runs handler in a frequency defined by the parameters. Returns cancel function
func Periodic(freq time.Duration, startNow bool, handler func()) (cancel func()) {
	cancelCh := make(chan bool)
	go func() {
		for {
			select {
			case <-cancelCh:
				return
			case <-time.After(freq):
				go handler()
			}
		}
	}()
	cancel = func() {
		cancelCh <- true
	}
	if startNow {
		go handler()
	}
	return
}
