package run

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestHandlePanic(t *testing.T) {
	handlerCalled := false
	handlerMessage := ""
	var handlerFn = func(message string) {
		handlerCalled = true
		handlerMessage = fmt.Sprintf(message)
	}
	var testFn = func() {
		defer HandlePanic("test message", handlerFn)
		panic("xxx")
	}

	testFn()
	assert.True(t, handlerCalled)
	assert.Equal(t, "test message recovered from: xxx", handlerMessage)
}
