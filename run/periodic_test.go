package run

import (
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestPeriodic(t *testing.T) {
	// test with start now
	runCount := 0
	cancel := Periodic(90*time.Millisecond, true, func() { runCount++ })
	time.Sleep(300 * time.Millisecond)
	cancel()
	assert.Equal(t, 4, runCount)
	time.Sleep(300 * time.Millisecond)
	assert.Equal(t, 4, runCount)
	// test with start later
	runCount = 0
	cancel = Periodic(90*time.Millisecond, false, func() { runCount++ })
	time.Sleep(300 * time.Millisecond)
	cancel()
	assert.Equal(t, 3, runCount)
	time.Sleep(300 * time.Millisecond)
	assert.Equal(t, 3, runCount)
}
