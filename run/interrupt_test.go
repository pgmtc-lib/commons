package run

import (
	"github.com/stretchr/testify/assert"
	"os"
	"os/signal"
	"testing"
	"time"
)

func TestHandleInterruptSignal(t *testing.T) {
	called := false
	CatchInterruptSignal(func() {
		called = true
	})
	proc, err := os.FindProcess(os.Getpid())
	if err != nil {
		t.Fatal(err)
	}

	sigc := make(chan os.Signal, 1)
	signal.Notify(sigc, os.Interrupt)

	go func() {
		<-sigc
		signal.Stop(sigc)
	}()

	proc.Signal(os.Interrupt)
	time.Sleep(1 * time.Second)
	assert.True(t, called)
}
