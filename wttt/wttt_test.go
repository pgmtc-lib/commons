package wttt

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/pgmtc-lib/commons/fnct"
	"gitlab.com/pgmtc-lib/commons/times"
	"runtime"
	"testing"
	"time"
)

func TestAddProcessor(t *testing.T) {
	wttt := &wttt{stats: &statistics{}}
	wttt.processors = []Processor{}
	assert.Empty(t, wttt.processors)
	proc1 := func(event interface{}, next Next) {}
	proc2 := func(event interface{}, next Next) {}
	wttt.Add(proc1)
	assert.Len(t, wttt.processors, 1)
	assert.Equal(t, fnct.Name(proc1), fnct.Name(wttt.processors[0]))
	wttt.Add(proc2)
	assert.Len(t, wttt.processors, 2)
	assert.Equal(t, fnct.Name(proc2), fnct.Name(wttt.processors[1]))
	// test adding multiple
	proc3 := func(event interface{}, next Next) {}
	proc4 := func(event interface{}, next Next) {}
	wttt.Add(proc3, proc4)
	assert.Len(t, wttt.processors, 4)
	assert.Equal(t, fnct.Name(proc3), fnct.Name(wttt.processors[2]))
	assert.Equal(t, fnct.Name(proc4), fnct.Name(wttt.processors[3]))

	// test reset
	wttt.Reset()
	assert.Len(t, wttt.processors, 0)
}

func TestCommitPeriodic(t *testing.T) {
	wttt := &wttt{stats: &statistics{}}
	wttt.ResetStats()
	var caughtTicks100 = 0
	var caughtTicks50 = 0
	wttt.Add(func(event interface{}, next Next) {
		if event == "tick100" {
			caughtTicks100++
		}
		if event == "tick50" {
			caughtTicks50++
		}
	})
	time.Sleep(500 * time.Millisecond)
	assert.Equal(t, 0, wttt.stats.numEvents)
	wttt.CommitEvery(100*time.Millisecond, "tick100")
	wttt.CommitEvery(50*time.Millisecond, "tick50")
	time.Sleep(510 * time.Millisecond)
	assert.Equal(t, 15, wttt.stats.numEvents)
	assert.Equal(t, 5, caughtTicks100)
	assert.Equal(t, 10, caughtTicks50)

}

func TestCommit(t *testing.T) {
	wttt := &wttt{stats: &statistics{}}
	wttt.ResetStats()
	var gotEventsp1 []interface{}
	var gotEventsp2 []interface{}
	wttt.Add(func(event interface{}, next Next) {
		gotEventsp1 = append(gotEventsp1, event)
	})
	wttt.Add(func(event interface{}, next Next) {
		gotEventsp2 = append(gotEventsp2, event)
	})
	wttt.Commit("first_event")
	wttt.Commit("second_event")
	assert.Equal(t, 2, wttt.stats.numEvents)
	assert.Len(t, gotEventsp1, 2)
	assert.Len(t, gotEventsp2, 2)
	assert.Equal(t, "first_event", gotEventsp1[0])
	assert.Equal(t, "second_event", gotEventsp1[1])
	assert.Equal(t, "first_event", gotEventsp2[0])
	assert.Equal(t, "second_event", gotEventsp2[1])

	// test with throttle timeout
	wttt.throttleTimeout = 100 * time.Millisecond
	start := time.Now().UTC()
	wttt.Commit("first_event", "second_event")
	diff := time.Now().UTC().Sub(start)
	lowMargin := float64(wttt.throttleTimeout) / 1.05  // less than 5 pct deviation
	highMargin := float64(wttt.throttleTimeout) * 1.05 // less than 5 pct deviation
	assert.True(t, float64(diff) < highMargin && float64(diff) > lowMargin, "diff is %v", diff)
	assert.Equal(t, 4, wttt.stats.numEvents)
	assert.Len(t, gotEventsp1, 4)
	assert.Len(t, gotEventsp2, 4)
}

func Test_worker(t *testing.T) {
	wttt := &wttt{stats: &statistics{}}
	wttt.ResetStats()
	var gotEvent interface{}
	proc := func(event interface{}, next Next) {
		gotEvent = event
	}
	wttt.worker(proc, "test")
	assert.Equal(t, "test", gotEvent)
}

func TestRunEvery(t *testing.T) {
	wttt := &wttt{stats: &statistics{}}
	wttt.ResetStats()
	var caughtTicks100 = 0
	var caughtTicks50 = 0
	var stopCounting bool
	var run100 = func(next Next) {
		if stopCounting {
			return
		}
		caughtTicks100++
		next("dummy") // this increases stats
	}
	var run50 = func(next Next) {
		if stopCounting {
			return
		}
		caughtTicks50++
		next("dummy") // this increases stats
	}
	fmt.Printf("start: %s\n", times.FormatISO(time.Now().UTC()))
	wttt.RunEvery(100*time.Millisecond, run100)
	cancel50 := wttt.RunEvery(50*time.Millisecond, run50)
	time.Sleep(510 * time.Millisecond)
	stopCounting = true // stop counting incoming ones and let it to finish - to count the next emits
	assert.Equal(t, 5, caughtTicks100)
	assert.Equal(t, 10, caughtTicks50)
	time.Sleep(500 * time.Millisecond) // let the dummy events bubble through
	assert.Equal(t, 15, wttt.stats.EventStat("dummy"))
	fmt.Println(wttt.stats.eventCounts)
	// test cancel
	begRoutines := runtime.NumGoroutine()
	caughtTicks50 = 0
	caughtTicks100 = 0
	cancel50()
	stopCounting = false
	time.Sleep(510 * time.Millisecond)
	assert.Equal(t, 5, caughtTicks100)
	assert.Equal(t, 0, caughtTicks50)
	assert.Equal(t, begRoutines-1, runtime.NumGoroutine())

}

func Test_wttt_Stats(t *testing.T) {
	w := wttt{}
	assert.Equal(t, "no stats", w.Stats())
	w.stats = &statistics{}
	assert.Equal(t, w.stats.string(), w.Stats())
}

func Test_wttt_EventStats(t *testing.T) {
	w := wttt{}
	assert.Equal(t, "no event stats", w.EventStats())
	w.stats = &statistics{
		eventCounts: map[string]int{
			"event1": 10,
			"event2": 10,
		},
	}
	assert.Equal(t, fmt.Sprintf("%+v", w.stats.eventCounts), w.EventStats())
}

func Test_wttt_Throttle(t *testing.T) {
	wttt := &wttt{stats: &statistics{}}
	assert.Equal(t, time.Duration(0), wttt.throttleTimeout)
	wttt.Throttle(10 * time.Millisecond)
	assert.Equal(t, 10*time.Millisecond, wttt.throttleTimeout)
}

func Test_wttt_NumProcessors(t *testing.T) {
	w := wttt{}
	assert.Equal(t, 0, w.NumProcessors())
	w.Add(func(event interface{}, next Next) {})
	assert.Equal(t, 1, w.NumProcessors())
	w.Add(func(event interface{}, next Next) {})
	assert.Equal(t, 2, w.NumProcessors())
}
