package wttt

import (
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestStatistics_String(t *testing.T) {
	beg := time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC)
	now = func() time.Time {
		return beg.Add(5 * time.Second)
	}
	defer func() {
		now = time.Now
	}()

	type fields struct {
		Start  time.Time
		Events int
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		{
			name:   "test-1",
			fields: fields{Start: beg, Events: 5000},
			want:   "events: 5000, throughput: 1000.00 ps",
		},
		{
			name:   "test-2",
			fields: fields{Start: beg, Events: 4},
			want:   "events: 4, throughput: 0.80 ps",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := statistics{
				startTime: tt.fields.Start,
				numEvents: tt.fields.Events,
			}
			if got := s.string(); got != tt.want {
				t.Errorf("string() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestStatistics_Reset(t *testing.T) {
	now = func() time.Time {
		return time.Date(2020, 2, 2, 0, 0, 0, 0, time.UTC)
	}
	defer func() {
		now = time.Now
	}()

	in := statistics{
		startTime: time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC),
		numEvents: 10,
	}
	in.reset()
	assert.Equal(t, now(), in.startTime)
	assert.Equal(t, 0, in.numEvents)
}

func TestStatistics_EventStat(t *testing.T) {
	type testStruct struct{}
	stats := statistics{
		eventCounts: map[string]int{
			getEventID("stat1"):      10,
			getEventID("stat2"):      20,
			getEventID(testStruct{}): 30,
		},
	}
	assert.Equal(t, 10, stats.EventStat("stat1"))
	assert.Equal(t, 20, stats.EventStat("stat2"))
	assert.Equal(t, 30, stats.EventStat(testStruct{}))
	assert.Equal(t, 0, stats.EventStat("stat4"))
}

func Test_getEventID(t *testing.T) {
	type testStruct struct{}
	tests := []struct {
		name       string
		event      interface{}
		wantResult string
	}{
		{event: "string-event", wantResult: "string(string-event)"},
		{event: 20, wantResult: "int(20)"},
		{event: testStruct{}, wantResult: "wttt.testStruct"},
		{event: &testStruct{}, wantResult: "*wttt.testStruct"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotResult := getEventID(tt.event); gotResult != tt.wantResult {
				t.Errorf("getEventID() = %v, want %v", gotResult, tt.wantResult)
			}
		})
	}
}

func TestStatistics_RecordEvent(t *testing.T) {
	stats := statistics{}
	stats.reset()
	assert.Empty(t, stats.eventCounts)
	stats.RecordEvent("test")
	assert.Len(t, stats.eventCounts, 1)
	assert.Contains(t, stats.eventCounts, getEventID("test"))
	assert.Equal(t, 1, stats.eventCounts[getEventID("test")])
	stats.RecordEvent(12)
	assert.Len(t, stats.eventCounts, 2)
	assert.Contains(t, stats.eventCounts, getEventID(12))
	assert.Equal(t, 1, stats.eventCounts[getEventID(12)])
	stats.RecordEvent("test")
	assert.Len(t, stats.eventCounts, 2)
	assert.Equal(t, 2, stats.eventCounts[getEventID("test")])

}
