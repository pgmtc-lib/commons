package wttt

import "time"

var globalWTTT WTTT

func init() {
	globalWTTT = New()
}

// Add adds processor to global wttt
func Add(processor ...Processor) {
	globalWTTT.Add(processor...)
}

// Reset clears all processors
func Reset() {
	globalWTTT.Reset()
}

// Commit commits event to global wttt
func Commit(events ...interface{}) {
	globalWTTT.Commit(events...)
}

// CommitEvery sets a ticker to commit given event on given frequency
func CommitEvery(interval time.Duration, event interface{}) {
	globalWTTT.CommitEvery(interval, event)
}

// RunEvery runs handler on given frequency
func RunEvery(interval time.Duration, handler func(next Next)) (cancel func()) {
	return globalWTTT.RunEvery(interval, handler)
}

// Throttle throttles the processing engine
func Throttle(timeout time.Duration) {
	globalWTTT.Throttle(timeout)
}

// ResetStats resets statistics
func ResetStats() {
	globalWTTT.ResetStats()
}

// Stats returns statistics
func Stats() string {
	return globalWTTT.Stats()
}

// EventStats returns event statistics
func EventStats() string {
	return globalWTTT.EventStats()
}

// NumProcessors returns number of attached processors
func NumProcessors() int {
	return globalWTTT.NumProcessors()
}
