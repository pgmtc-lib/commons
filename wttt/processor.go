package wttt

// Processor is a signature of a processor function
type Processor func(event interface{}, next Next)

// Next is a signature of a commit function
type Next func(events ...interface{})
