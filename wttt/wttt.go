package wttt

import (
	"fmt"
	"runtime"
	"sync"
	"time"
)

// WTTT is an interface for when-this-then-that
type WTTT interface {
	Add(processor ...Processor)
	Reset()
	Commit(events ...interface{})
	CommitEvery(interval time.Duration, event interface{})
	RunEvery(interval time.Duration, handler func(next Next)) (cancel func())
	Throttle(timeout time.Duration)
	ResetStats()
	Stats() string
	EventStats() string
	NumProcessors() int
}

type wttt struct {
	throttleTimeout time.Duration
	processors      []Processor
	workerLock      sync.Mutex
	stats           *statistics
}

// New is a constructor
func New() WTTT {
	return &wttt{
		throttleTimeout: 0 * time.Microsecond,
		processors:      []Processor{},
		workerLock:      sync.Mutex{},
		stats:           (&statistics{}).reset(),
	}
}

// Reset clears the processors
func (w *wttt) Reset() {
	w.processors = make([]Processor, 0)
}

// Add adds processor to the list of processors
func (w *wttt) Add(processor ...Processor) {
	w.processors = append(w.processors, processor...)
	return
}

// Commit sends events to the engine
func (w *wttt) Commit(events ...interface{}) {
	runtime.Gosched()
	if w.throttleTimeout > 0 {
		time.Sleep(w.throttleTimeout)
	}
	for _, event := range events {
		w.stats.RecordEvent(event)
		for _, proc := range w.processors {
			w.worker(proc, event)
		}
	}
}

// CommitEvery starts ticker which is emitting events in periodic fashion
func (w *wttt) CommitEvery(interval time.Duration, event interface{}) {
	go func() {
		ticker := time.NewTicker(interval)
		for {
			select {
			case <-ticker.C:
				w.Commit(event)
			}

		}
	}()
	runtime.Gosched()
}

// RunEvery starts ticker which is run in periodic fashion
func (w *wttt) RunEvery(interval time.Duration, handler func(next Next)) (cancel func()) {
	cancelChan := make(chan struct{})
	cancel = func() {
		cancelChan <- struct{}{}
	}
	go func() {
		ticker := time.NewTicker(interval)
	ForeverLoop:
		for {
			select {
			case <-cancelChan:
				ticker.Stop()
				break ForeverLoop
			case <-ticker.C:
				handler(func(events ...interface{}) {
					go w.Commit(events...)
				})
			}
		}
	}()
	return
}

// Throttle sets throttle timeout
func (w *wttt) Throttle(timeout time.Duration) {
	w.throttleTimeout = timeout
}

// Reset resets wttt stats
func (w *wttt) ResetStats() {
	if w.stats == nil {
		w.stats = &statistics{}
	}
	w.stats.reset()
}

// Stats return stats
func (w *wttt) Stats() string {
	if w.stats != nil {
		return w.stats.string()
	}
	return "no stats"
}

// EventStats returns stats about events run
func (w *wttt) EventStats() string {
	if w.stats != nil {
		return fmt.Sprintf("%+v", w.stats.eventCounts)
	}
	return "no event stats"
}

// NumProcessors returns number of attached processors
func (w *wttt) NumProcessors() int {
	return len(w.processors)
}

// worker performs the processor-event run
func (w *wttt) worker(proc Processor, event interface{}) {
	w.workerLock.Lock()
	defer w.workerLock.Unlock()
	proc(event, func(events ...interface{}) {
		go w.Commit(events...)
	})
}
