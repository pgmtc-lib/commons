package test

import (
	"fmt"
	"gitlab.com/pgmtc-lib/commons/nums"
	"gitlab.com/pgmtc-lib/commons/times"
	"gitlab.com/pgmtc-lib/commons/wttt"
	"runtime"
	"testing"
	"time"
)

func Test(t *testing.T) {
	wt := wttt.New()
	var procs = 1
	var routines = procs * 4

	dueCnt := 0
	dueMarkedCnt := 0
	runtime.GOMAXPROCS(procs)
	done := make(chan bool)
	var x = make(map[int]int)
	wt.Add(func(event interface{}, next wttt.Next) {
		de, ok := event.(proc1Event)
		if !ok || !de.new {
			return
		}
		x[1] = nums.RandomInt(0, 100)
		next(&proc2Event{DeviceID: de.deviceID})
	})

	wt.Add(func(event interface{}, next wttt.Next) {
		if due, ok := event.(*proc2Event); ok && due.result == 0 {
			dueCnt++
			go func() {
				result := Fib(nums.RandomInt(1, 28))
				next(&proc2Event{
					DeviceID: due.DeviceID,
					result:   result,
				})
			}()
		}
	})
	var ticks = 0
	wt.Add(func(event interface{}, next wttt.Next) {
		if event == "tick" {
			rot := runtime.NumGoroutine()
			mem := &runtime.MemStats{}
			runtime.ReadMemStats(mem)
			fmt.Printf("\r%s - %s, goroutine: %d, memory: %d, due: %d, dueMarked: %d, lag: %d\n", times.FormatISO(time.Now().UTC()), wt.Stats(), rot, mem.Alloc, dueCnt, dueMarkedCnt, dueMarkedCnt-dueCnt)
			fmt.Printf("event stats: %s\n", wt.EventStats())
			wt.ResetStats()
			ticks++
			if ticks >= 5 {
				done <- true
			}

		}
	})
	wt.Add(func(event interface{}, next wttt.Next) {
		if due, ok := event.(*proc2Event); ok && due.result > 0 {
			dueMarkedCnt++
		}
		return
	})
	wt.CommitEvery(1*time.Second, "tick")
	wt.CommitEvery(1*time.Nanosecond, "flood")
	wt.Add(func(event interface{}, next wttt.Next) {
		if event == "flood" {
			next(proc1Event{passFrom: "custom", deviceID: "dev3", new: nums.RandomInt(0, 100) > 50}, proc1Event{passFrom: "custom", deviceID: "dev4", new: nums.RandomInt(0, 100) > 50})
		}
	})
	for i := 0; i < routines; i++ {
		go func() {
			for {
				wt.Commit(proc1Event{passFrom: "custom", deviceID: "dev3", new: nums.RandomInt(0, 100) > 50}, proc1Event{passFrom: "custom", deviceID: "dev4", new: nums.RandomInt(0, 100) > 50})
			}
		}()
	}
	<-done
}

type proc1Event struct {
	passFrom string
	new      bool
	deviceID string
}

func (d proc1Event) ID() string {
	return "DU_EVENT"
}

func (d proc1Event) String() string {
	return fmt.Sprintf("%s, %s, %t", d.passFrom, d.deviceID, d.new)
}

// proc2Event is an example event
type proc2Event struct {
	DeviceID string
	result   int
}

// Fib returns fibonacci
func Fib(n int) int {
	if n < 2 {
		return n
	}
	return Fib(n-1) + Fib(n-2)
}
