package wttt

import (
	"github.com/tj/assert"
	"testing"
	"time"
)

func TestGlobalWttt(t *testing.T) {
	var gotFirstEvent interface{}
	var gotSecondEvent interface{}
	var gotThirdEvent interface{}
	var numFirst int
	var numSecond int
	var numThird int
	var proc1 Processor = func(event interface{}, next Next) {
		if event == "first" {
			numFirst++
			gotFirstEvent = event
			next("second")
		}
	}
	var proc2 Processor = func(event interface{}, next Next) {
		if event == "second" {
			numSecond++
			gotSecondEvent = event
		}
	}
	var proc3 Processor = func(event interface{}, next Next) {
		if event == "third" {
			numThird++
			gotThirdEvent = event
		}
	}
	assert.Nil(t, gotFirstEvent)
	assert.Nil(t, gotSecondEvent)
	assert.Nil(t, gotThirdEvent)
	assert.Equal(t, 0, numFirst)
	assert.Equal(t, 0, numSecond)
	assert.Equal(t, 0, numThird)
	assert.Len(t, globalWTTT.(*wttt).processors, 0)
	assert.Equal(t, 0, NumProcessors())
	Add(proc1)
	assert.Len(t, globalWTTT.(*wttt).processors, 1)
	assert.Equal(t, 1, NumProcessors())
	Add(proc2)
	Add(proc3)
	assert.Len(t, globalWTTT.(*wttt).processors, 3)
	assert.Equal(t, 3, NumProcessors())

	// test stats
	globalWTTT.(*wttt).stats.numEvents = 10
	globalWTTT.(*wttt).stats.eventCounts["evt1"] = 1         // set artificially
	assert.Equal(t, 10, globalWTTT.(*wttt).stats.numEvents)  // set artificially
	assert.Equal(t, globalWTTT.Stats()[0:11], Stats()[0:11]) // compare first 11 characters to avoid calculated throughput
	assert.Equal(t, globalWTTT.EventStats(), EventStats())
	// test reset stats
	ResetStats()
	assert.Equal(t, 0, globalWTTT.(*wttt).stats.numEvents)
	assert.Equal(t, globalWTTT.Stats()[0:11], Stats()[0:11]) // compare first 11 characters to avoid calculated throughput

	// test commit
	Commit("first")
	Commit("third")
	time.Sleep(1 * time.Millisecond)
	assert.Equal(t, 1, numFirst)
	assert.Equal(t, 1, numSecond)
	assert.Equal(t, 1, numThird)
	assert.Equal(t, "first", gotFirstEvent)
	assert.Equal(t, "second", gotSecondEvent)
	assert.Equal(t, "third", gotThirdEvent)

	// test commit every
	numFirst, numSecond, numThird = 0, 0, 0
	start := make(chan struct{})
	done := make(chan struct{})
	go func() {
		<-start
		CommitEvery(10*time.Millisecond, "second")
	}()
	go func() {
		<-start
		time.Sleep(105 * time.Millisecond)
		done <- struct{}{}
	}()
	time.Sleep(5 * time.Millisecond)
	close(start)
	<-done
	assert.Equal(t, 0, numFirst)
	assert.Equal(t, 10, numSecond)
	assert.Equal(t, 0, numThird)

	// test run every
	numFirst, numSecond, numThird = 0, 0, 0

	var numRunEvery = 0
	start = make(chan struct{})
	done = make(chan struct{})
	var cancel func()
	go func() {
		<-start
		cancel = RunEvery(10*time.Millisecond, func(next Next) {
			numRunEvery++
			next("third")
		})
	}()
	go func() {
		<-start
		time.Sleep(105 * time.Millisecond)
		done <- struct{}{}
	}()
	time.Sleep(5 * time.Millisecond)
	close(start)
	<-done
	assert.Equal(t, 10, numRunEvery)
	assert.Equal(t, 10, numThird)
	cancel() // test that cancels stops counting
	time.Sleep(105 * time.Millisecond)
	assert.Equal(t, 10, numRunEvery)
	assert.Equal(t, 10, numThird)

	// test throttle
	Throttle(1 * time.Second)
	assert.Equal(t, 1*time.Second, globalWTTT.(*wttt).throttleTimeout)

	// test reset
	assert.Len(t, globalWTTT.(*wttt).processors, 3)
	Reset()
	assert.Len(t, globalWTTT.(*wttt).processors, 0)
}
