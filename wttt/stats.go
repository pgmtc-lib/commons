package wttt

import (
	"fmt"
	"reflect"
	"strconv"
	"sync"
	"time"
)

// statistics is a struct for statistics
type statistics struct {
	lock        sync.RWMutex
	startTime   time.Time
	numEvents   int
	eventCounts map[string]int
}

// now returns current time
var now = time.Now

// string returns string representation of the stats
func (s *statistics) string() string {
	perMs := float64(s.numEvents) / now().Sub(s.startTime).Seconds()
	return fmt.Sprintf("events: %d, throughput: %.2f ps", s.numEvents, perMs)
}

// EventStat returns count for an individual event
func (s *statistics) EventStat(event interface{}) (result int) {
	result = s.eventCounts[getEventID(event)]
	return
}

// Reset resets the stats
func (s *statistics) reset() *statistics {
	s.lock.Lock()
	s.numEvents = 0
	s.startTime = now()
	s.eventCounts = make(map[string]int)
	s.lock.Unlock()
	return s
}

// RecordEvent records event into the stats
func (s *statistics) RecordEvent(event interface{}) {
	s.lock.Lock()
	defer s.lock.Unlock()
	s.numEvents++
	s.eventCounts[getEventID(event)]++
}

func getEventID(event interface{}) (result string) {
	if sevent, ok := event.(string); ok {
		result = "string(" + sevent + ")"
		return
	}
	if ievent, ok := event.(int); ok {
		result = "int(" + strconv.Itoa(ievent) + ")"
		return
	}
	result = reflect.TypeOf(event).String()
	return
}
