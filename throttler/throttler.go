package throttler

import (
	"fmt"
	"gitlab.com/pgmtc-lib/commons/ifs"
	"gitlab.com/pgmtc-lib/commons/ifs/use"
	"sync"
	"time"
)

var timeNow = time.Now

// Throttler represents interface
type Throttler interface {
	Update(id interface{}, runner func())
	String() string
}

type throttler struct {
	running        bool
	flushInterval  time.Duration
	expiryInterval time.Duration
	lock           sync.Mutex
	runnerBuffer   map[interface{}]func()
	expiryBuffer   map[interface{}]time.Time
}

// Update commits runner for running
func (t *throttler) Update(id interface{}, runner func()) {
	if t.flushInterval <= 0 {
		runner() // run straight away
		return
	}
	t.lock.Lock()
	defer t.lock.Unlock()
	// initialize maps if they are not yet initialized
	if t.runnerBuffer == nil {
		t.runnerBuffer = make(map[interface{}]func())
		t.expiryBuffer = make(map[interface{}]time.Time)
	}
	// store in runner with expiry
	t.runnerBuffer[id] = runner
	t.expiryBuffer[id] = timeNow().Add(t.expiryInterval)
}

// String returns string representation
func (t *throttler) String() string {
	return fmt.Sprintf("throttler: %s, flushInterval: %s, expiryInterval: %s", ifs.When(t.running).Then(use.String("on")).Otherwise(use.String("off")), t.flushInterval, t.expiryInterval)
}

func (t *throttler) flush() {
	t.lock.Lock()
	defer t.lock.Unlock()
	if len(t.expiryBuffer) > 0 {
		for id, expiry := range t.expiryBuffer {
			if timeNow().After(expiry) {
				t.runnerBuffer[id]() // run runner
				delete(t.expiryBuffer, id)
				delete(t.runnerBuffer, id)
			}
		}
	}
}

// start starts the periodic flushing buffer
func (t *throttler) start() {
	if t.flushInterval > 0 {
		go func() {
			for {
				time.Sleep(t.flushInterval)
				t.flush()
			}
		}()
		t.running = true
	}
}

// New is a constructor
// expiryInterval drives how long should the property wait for more updates, before it is flushed
// flushInterval is a frequency how often it checks whether there are any runners which have expired (hence will be run)
// example: flushInterval = 500ms, expiryInterval = 5s - when something comited, it will wait for 5 further seconds before it is released.
func New(flushInterval, expiryInterval time.Duration) (result Throttler) {
	result = &throttler{}
	if flushInterval > 0 {
		result.(*throttler).expiryBuffer = make(map[interface{}]time.Time)
		result.(*throttler).runnerBuffer = make(map[interface{}]func())
		result.(*throttler).flushInterval = flushInterval
		result.(*throttler).expiryInterval = expiryInterval
		result.(*throttler).start()
	}
	return result
}
