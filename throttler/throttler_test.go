package throttler

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/pgmtc-lib/commons/fnct"
	"gitlab.com/pgmtc-lib/commons/tst"
	"testing"
	"time"
)

func TestNew(t *testing.T) {
	thr := New(1*time.Second, 5*time.Second)
	assert.Equal(t, 1*time.Second, thr.(*throttler).flushInterval)
	assert.Equal(t, 5*time.Second, thr.(*throttler).expiryInterval)
	assert.True(t, thr.(*throttler).running)
	assert.Equal(t, "throttler: on, flushInterval: 1s, expiryInterval: 5s", thr.String())

	thr = New(0*time.Second, 5*time.Second)
	assert.Equal(t, 0*time.Second, thr.(*throttler).flushInterval)
	assert.Equal(t, 0*time.Second, thr.(*throttler).expiryInterval)
	assert.False(t, thr.(*throttler).running)
	assert.Equal(t, "throttler: off, flushInterval: 0s, expiryInterval: 0s", thr.String())
}

func Test_throttler_Update(t *testing.T) {
	var runnerRunCount, runner1RunCount, runner2RunCount, runner3Count int
	runner1 := func() {
		runnerRunCount++
		runner1RunCount++
	}
	runner2 := func() {
		runnerRunCount++
		runner2RunCount++
	}
	runner3 := func() {
		runnerRunCount++
		runner3Count++
	}
	timeNow = func() time.Time {
		return time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC)
	}
	// test with no flushing
	thr := throttler{}
	thr.Update(1, runner1)
	thr.Update(1, runner2)
	assert.Equal(t, 2, runnerRunCount)
	assert.Equal(t, 1, runner1RunCount)
	assert.Equal(t, 1, runner2RunCount)
	assert.Empty(t, thr.runnerBuffer)
	assert.Empty(t, thr.expiryBuffer)
	// test with flushing
	thr.flush()
	runnerRunCount, runner1RunCount, runner2RunCount = 0, 0, 0
	thr.flushInterval = 1 * time.Second       // this enables the logic
	thr.expiryInterval = 0 * time.Millisecond // expire imediatelly
	thr.Update(1, runner1)
	thr.Update(2, runner2)
	tst.AllTrue(t, runnerRunCount == 0, runner1RunCount == 0, runner2RunCount == 0)
	assert.Len(t, thr.runnerBuffer, 2)
	assert.Contains(t, thr.runnerBuffer, 1)
	assert.Contains(t, thr.runnerBuffer, 2)
	assert.Equal(t, "gitlab.com/pgmtc-lib/commons/throttler.Test_throttler_Update.func1", fnct.Name(thr.runnerBuffer[1]))
	assert.Equal(t, "gitlab.com/pgmtc-lib/commons/throttler.Test_throttler_Update.func2", fnct.Name(thr.runnerBuffer[2]))
	assert.Equal(t, time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC), thr.expiryBuffer[1])
	assert.Equal(t, time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC), thr.expiryBuffer[2])

	timeNow = func() time.Time { return time.Date(2021, 1, 1, 0, 0, 0, 0, time.UTC) }
	thr.Update(1, runner3) // overwrite by runner3 - so it replaces
	assert.Contains(t, thr.runnerBuffer, 1)
	assert.Contains(t, thr.runnerBuffer, 2)
	assert.Equal(t, "gitlab.com/pgmtc-lib/commons/throttler.Test_throttler_Update.func3", fnct.Name(thr.runnerBuffer[1]))
	assert.Equal(t, "gitlab.com/pgmtc-lib/commons/throttler.Test_throttler_Update.func2", fnct.Name(thr.runnerBuffer[2]))
	assert.Equal(t, time.Date(2021, 1, 1, 0, 0, 0, 0, time.UTC), thr.expiryBuffer[1])
	assert.Equal(t, time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC), thr.expiryBuffer[2])
}

func Test_throttler_flush(t *testing.T) {
	timeNow = time.Now
	var runnerRunCount, runner1RunCount, runner2RunCount, runner3Count int
	runner1 := func() {
		runnerRunCount++
		runner1RunCount++
	}
	runner2 := func() {
		runnerRunCount++
		runner2RunCount++
	}
	runner3 := func() {
		runnerRunCount++
		runner3Count++
	}
	thr := throttler{}
	thr.flushInterval = 1 * time.Second  // this enables the logic
	thr.expiryInterval = 1 * time.Second // expire immediately
	thr.Update(1, runner1)
	thr.Update(2, runner2)
	tst.AllTrue(t, runnerRunCount == 0, runner1RunCount == 0, runner2RunCount == 0, runner3Count == 0)
	thr.flush()            // should not do anything due to expiry
	thr.Update(1, runner3) // overwrite
	tst.AllTrue(t, runnerRunCount == 0, runner1RunCount == 0, runner2RunCount == 0, runner3Count == 0)
	time.Sleep(1 * time.Second)
	thr.flush() // this should run the runners
	tst.AllTrue(t, runnerRunCount == 2, runner1RunCount == 0, runner2RunCount == 1, runner3Count == 1)

}
