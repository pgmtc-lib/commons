package svc

import (
	"fmt"
	"net/http"
	"strconv"
	"time"
)

func httpUpTest(url string, tries int) func() error {
	return func() (resultErr error) {
		for i := 0; i < tries; i++ {
			var resp *http.Response
			/* #nosec */
			resp, resultErr = http.Get(url)
			if resultErr != nil {
				time.Sleep(1000 * time.Millisecond)
				continue
			}
			if resp.StatusCode != 200 {
				resultErr = fmt.Errorf("unexpected status coming from connection check: " + strconv.Itoa(resp.StatusCode))
				continue
			}
			break
		}
		return
	}
}
