package svc

import (
	"fmt"
	"time"
)

// Runner is a method signature for service constructor
type Runner func() error

// Service represents service interface
type Service interface {
	Start() error
}

type service struct {
	serviceRunner    func() error
	upTest           func() error
	timeoutAfter     time.Duration
	initialTestDelay time.Duration
	errChan          chan error
	okChan           chan struct{}
	serviceChan      chan struct{}
	checkChan        chan struct{}
}

// Start starts the service
func (r *service) Start() (resultErr error) {
	r.errChan = make(chan error)
	r.okChan = make(chan struct{})
	r.serviceChan = make(chan struct{})
	r.checkChan = make(chan struct{})

	go func() {
		<-r.serviceChan
		if r.upTest != nil {
			r.checkChan <- struct{}{}
		}
		if err := r.serviceRunner(); err != nil {
			r.errChan <- err
			return
		}
	}()
	if r.upTest != nil {
		go func() { // up tester check
			<-r.checkChan
			time.Sleep(r.initialTestDelay)
			if err := r.upTest(); err != nil {
				r.errChan <- err
				return
			}

			close(r.okChan)
		}()
	}

	r.serviceChan <- struct{}{}
	select {
	case <-r.okChan:
	case resultErr = <-r.errChan: // error
	case <-time.After(r.timeoutAfter): // timeout
		resultErr = fmt.Errorf("runner timed out after %s", r.timeoutAfter)
	}
	return
}

// New is a constructor of a new service
func New(serviceRunner Runner, opts ...Option) (result Service) {
	result = &service{
		serviceRunner:    serviceRunner,
		upTest:           nil,
		timeoutAfter:     10 * time.Second,
		initialTestDelay: 100 * time.Millisecond,
	}
	for _, opt := range opts {
		opt(result.(*service))
	}
	return
}
