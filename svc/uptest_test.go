package svc

import (
	"github.com/stretchr/testify/assert"
	"net/http"
	"testing"
	"time"
)

func Test_httpUpTest(t *testing.T) {
	tester := httpUpTest("http://127.0.0.1:8080", 1)
	assert.Error(t, tester())
	go func() {
		http.HandleFunc("/200", func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(200)
			_, _ = w.Write([]byte("success"))
		})
		http.HandleFunc("/400", func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(400)
			_, _ = w.Write([]byte("bad request"))
		})
		if err := http.ListenAndServe("127.0.0.1:8080", nil); err != nil {
			assert.FailNow(t, "error when starting test service")
		}
	}()

	time.Sleep(1 * time.Second)
	tester = httpUpTest("http://127.0.0.1:8080/400", 1)
	assert.EqualError(t, tester(), "unexpected status coming from connection check: 400")

	tester = httpUpTest("http://127.0.0.1:8080/200", 1)
	assert.NoError(t, tester())
}
