package svc

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/pgmtc-lib/commons/fnct"
	"testing"
	"time"
)

func TestWithHTTPUpCheck(t *testing.T) {
	svc := service{}

	opt := WithHTTPUpCheck("url", 5)
	opt(&svc)
	assert.Equal(t, fnct.Name(httpUpTest("url", 5)), fnct.Name(svc.upTest))

	opt = WithTimeout(15 * time.Second)
	opt(&svc)
	assert.Equal(t, 15*time.Second, svc.timeoutAfter)
}
