package svc

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/pgmtc-lib/commons/fnct"
	"testing"
	"time"
)

func TestService_Start(t *testing.T) {
	runner := service{
		serviceRunner: func() error {
			time.Sleep(100 * time.Millisecond)
			return nil
		},
		upTest: func() error {
			time.Sleep(500 * time.Millisecond)
			return nil
		},
		timeoutAfter:     2 * time.Second,
		initialTestDelay: 1 * time.Second,
	}

	err := runner.Start()
	assert.NoError(t, err)

	// test when service fails
	runner = service{
		timeoutAfter: time.Duration(1 * time.Second),
		serviceRunner: func() error {
			return fmt.Errorf("start failed")
		},
	}
	err = runner.Start()
	assert.EqualError(t, err, "start failed")

	// test when check fails
	runner = service{
		timeoutAfter: time.Duration(1 * time.Second),
		serviceRunner: func() error {
			return nil
		},
		upTest: func() error {
			return fmt.Errorf("check failed")
		},
	}
	err = runner.Start()
	assert.EqualError(t, err, "check failed")

	// test when the thing times out
	runner = service{
		serviceRunner: func() error {
			return nil
		},
		upTest: func() error {
			time.Sleep(5 * time.Second)
			return nil
		},
		timeoutAfter:     2 * time.Second,
		initialTestDelay: 1 * time.Second,
	}
	err = runner.Start()
	assert.EqualError(t, err, "runner timed out after 2s")

}

func TestNew(t *testing.T) {
	// test that the defaults are fileld in
	runner := func() error {
		return nil
	}
	svc := New(runner).(*service)
	assert.Equal(t, fnct.Name(runner), fnct.Name(svc.serviceRunner))
	assert.Nil(t, svc.upTest)
	assert.Equal(t, 10*time.Second, svc.timeoutAfter)
	assert.Equal(t, 100*time.Millisecond, svc.initialTestDelay)

	// test options
	svc = New(runner, WithTimeout(1*time.Second)).(*service)
	assert.Equal(t, 1*time.Second, svc.timeoutAfter)

	svc = New(runner, WithHTTPUpCheck("http://localhost:1234", 5)).(*service)
	assert.NotNil(t, svc.upTest)

}
