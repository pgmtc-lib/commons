package svc

import "time"

// Option represents the option for the service
type Option func(*service)

// WithHTTPUpCheck sets http up test with the provided url
func WithHTTPUpCheck(url string, tries int) Option {
	return func(s *service) {
		s.upTest = httpUpTest(url, tries)
	}
}

// WithTimeout sets the start timeout to the service
func WithTimeout(timeout time.Duration) Option {
	return func(s *service) {
		s.timeoutAfter = timeout
	}
}
