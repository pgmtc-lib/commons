# eventbus
Eventbus provides a simple event bus (or pub / sub mechanism) which can be extended by an adapter, such as WebSocket handler.
## Simple websocket example
```go
bus := eventbus.Bus{}
```
### Listen for message
```
func listenSupport(w http.ResponseWriter, r *http.Request) {
    adapter, err := eventbus.NewWsAdapter(w, r)
    if err != nil {
      // ...
    }
    bus.Register(adapter).
      ListenTopics("outgoing1", "outgoing2"). // topics which will be sent out via the websocket
      PublishTopics("incoming1", "incoming2") // topics to publish to when message originating from the remote websocket
    
    // you can also attach listeners directly to the websocket adapter
    adapter.OnIncoming(func(message interface{}) {
      fmt.Printf("--- webservice received a message: %s", message)
    })
    adapter.OnOutgoing(func(message interface{}) {
      fmt.Printf("--- webservice sent a message: %s", message)
    })
    adapter.OnClose(func(reason string) {
      fmt.Printf("--- webservice has been closed: %s", reason)
    })
}
```
### Dispatch message
To dispatch message into the bus (which is then passed to the websocket)
```go
bus.Publish("outgoing1", "message I want to send out using websocket")
```

### Subscribe to topic
To subscribe to the topic (which is populated by the websocket), do
```go
bus.Subscribe("incoming1", func(message interface{}){
	// Something on the other end of the websocket has sent the message
})
```