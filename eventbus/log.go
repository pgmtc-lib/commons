package eventbus

// LogDebug represents log function for the websocket
var LogDebug func(message string, a ...interface{}) = func(message string, a ...interface{}) {}

//// LogInfo represents log function
//var LogInfo func(message string, a ...interface{}) = func(message string, a ...interface{}) {}

// LogError represents log function
var LogError func(message string, a ...interface{}) = func(message string, a ...interface{}) {}
