package eventbus

import (
	"fmt"
	"gitlab.com/pgmtc-lib/commons/marshal"
	"gitlab.com/pgmtc-lib/commons/strs"
	"sync"
)

// Adapter represents interface used by the
type Adapter interface {
	OnIncoming(...func(message interface{})) // listening to incoming messages from the outer world
	OnOutgoing(...func(message interface{})) // listening on outgoing messages
	OnClose(...func(reason string))          // listening to close event
	SendOut(message interface{}) error       // sending outgoing messages
	Close(reason string)                     // closing remote connection
}

// Bus represents event bus
type Bus struct {
	handlers      map[string]map[string]Adapter
	listeners     map[string]map[string]func(message interface{})
	handlersLock  sync.Mutex
	listenersLock sync.Mutex
}

// Subscribe subscribes for updates on the topic
func (b *Bus) Subscribe(topicID string, listener func(message interface{})) (cancel func()) {
	b.listenersLock.Lock()
	defer b.listenersLock.Unlock()
	if b.listeners == nil {
		b.listeners = make(map[string]map[string]func(message interface{}))
	}
	if _, ok := b.listeners[topicID]; !ok {
		b.listeners[topicID] = make(map[string]func(message interface{}))
	}
	listenerID := "listener_" + strs.RandomString(8)
	LogDebug("registering listener %s to the register %s", listenerID, topicID)
	b.listeners[topicID][listenerID] = listener
	cancel = func() {
		b.listenersLock.Lock()
		defer b.listenersLock.Unlock()
		delete(b.listeners[topicID], listenerID)
	}
	return
}

// Publish publishes message to the topic
func (b *Bus) Publish(topicID string, message interface{}) {
	if b.handlers == nil || len(b.handlers[topicID]) == 0 {
		return
	}
	for handlerID, handler := range b.handlers[topicID] {
		LogDebug("sending message to handler with id %s from the Register %s", handlerID, topicID)
		if err := handler.SendOut(message); err != nil {
			handler.Close("failed to send data")
		}
	}
}

// Register register handler against set of topics
func (b *Bus) Register(handler Adapter) (result *BusRegistration) {
	handlerID := "handler_" + strs.RandomString(8)
	result = &BusRegistration{
		b:         b,
		a:         handler,
		handlerID: handlerID,
	}
	return
}

func (b *Bus) registerListenTopics(a Adapter, handlerID string, topicIDs ...string) {
	b.handlersLock.Lock()
	defer b.handlersLock.Unlock()
	if b.handlers == nil {
		b.handlers = make(map[string]map[string]Adapter)
	}
	a.OnClose(func(reason string) {
		b.handlersLock.Lock()
		defer b.handlersLock.Unlock()
		for _, topicID := range topicIDs {
			LogDebug("removing handler with id %s from the Register %s, reason: %s", handlerID, topicID, reason)
			delete(b.handlers[topicID], handlerID)
		}
	})
	for _, topicID := range topicIDs {
		LogDebug("adding handler with id %s from the Register %s", handlerID, topicID)
		if _, ok := b.handlers[topicID]; !ok {
			b.handlers[topicID] = make(map[string]Adapter)
		}
		b.handlers[topicID][handlerID] = a
	}
}

func (b *Bus) registerPublishTopics(a Adapter, topicIDs ...string) {
	a.OnIncoming(func(message interface{}) {
		for _, topicID := range topicIDs {
			if b.listeners == nil || b.listeners[topicID] == nil {
				LogDebug("no listeners for %s", topicID)
				continue
			}
			for lid, listener := range b.listeners[topicID] {
				LogDebug("calling listener %s with message", lid)
				listener(message)
			}
		}
	})
}

// Introspect prints all handlers
func (b *Bus) Introspect() {
	fmt.Println("-")
	fmt.Printf("handlers: %s", marshal.TryJSONPretty(b.handlers))
	fmt.Println("-")
	fmt.Printf("listeners: %s", marshal.TryJSONPretty(b.listeners))
	fmt.Println("-")
}
