package eventbus

// BusRegistration is returned by the Bus and exposes additional setters
type BusRegistration struct {
	b         *Bus
	a         Adapter
	handlerID string
}

// ListenTopics sets handler to listen on specific topics
func (r *BusRegistration) ListenTopics(topicIDs ...string) *BusRegistration {
	r.b.registerListenTopics(r.a, r.handlerID, topicIDs...)
	return r
}

// PublishTopics sets handler to publish to specific topics
func (r *BusRegistration) PublishTopics(topicIDs ...string) *BusRegistration {
	r.b.registerPublishTopics(r.a, topicIDs...)
	return r
}
