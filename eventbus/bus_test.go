package eventbus

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/pgmtc-lib/commons/fnct"
	"strings"
	"testing"
)

func TestBus_Subscribe(t *testing.T) {
	var h1Got interface{}
	var h2Got interface{}
	var h3Got interface{}
	b := Bus{}
	h := func(message interface{}) {
		h1Got = message
	}
	assert.Empty(t, b.listeners)
	b.Introspect()
	cancel1 := b.Subscribe("topicID", h)
	assert.Equal(t, len(b.listeners), 1)
	assert.Equal(t, len(b.listeners["topicID"]), 1)
	for key, handler := range b.listeners["topicID"] {
		assert.True(t, strings.HasPrefix(key, "listener_"))
		assert.NotEmpty(t, fnct.Name(h))
		assert.Equal(t, fnct.Name(h), fnct.Name(handler))
	}
	// test add another listener
	h2 := func(message interface{}) {
		h2Got = message
	}
	h3 := func(message interface{}) {
		h3Got = message
	}
	cancel2 := b.Subscribe("topicID", h2)
	cancel3 := b.Subscribe("topicID2", h3)
	assert.Equal(t, len(b.listeners), 2)
	assert.Equal(t, len(b.listeners["topicID"]), 2)
	assert.Equal(t, len(b.listeners["topicID2"]), 1)
	// try handlers - with no listeners
	ma := &mockAdapter{}
	b.registerPublishTopics(ma, "topicID7")
	ma.triggerIncoming("test")
	assert.Nil(t, h1Got)
	assert.Nil(t, h2Got)
	assert.Nil(t, h3Got)

	// try handlers - with listeners
	b.registerPublishTopics(ma, "topicID")
	assert.Nil(t, h1Got)
	assert.Nil(t, h2Got)
	assert.Nil(t, h3Got)

	ma.triggerIncoming("test")
	assert.Equal(t, "test", h1Got)
	assert.Equal(t, "test", h2Got)
	assert.Nil(t, h3Got)

	// try cancel
	cancel2()
	assert.Equal(t, len(b.listeners), 2)
	assert.Equal(t, len(b.listeners["topicID"]), 1)
	assert.Equal(t, len(b.listeners["topicID2"]), 1)
	cancel3()
	assert.Equal(t, len(b.listeners), 2)
	assert.Equal(t, len(b.listeners["topicID"]), 1)
	assert.Equal(t, len(b.listeners["topicID2"]), 0)
	cancel1()
	assert.Equal(t, len(b.listeners), 2)
	assert.Equal(t, len(b.listeners["topicID"]), 0)
	assert.Equal(t, len(b.listeners["topicID2"]), 0)
}

func TestBus_Register(t *testing.T) {
	b := &Bus{}
	a := &mockAdapter{}
	reg := b.Register(a)
	assert.NotEmpty(t, reg.handlerID)
	assert.Equal(t, a, reg.a)
	assert.Equal(t, b, reg.b)
}
