package eventbus

import (
	"context"
	"net/http"
	"nhooyr.io/websocket"
	"nhooyr.io/websocket/wsjson"
	"time"
)

// wsAdapter is a websocket Adapter
type wsAdapter struct {
	internalIncomingHandlers []func(message interface{})
	internalOutgoingHandlers []func(message interface{})
	internalCloseHandlers    []func(reason string)
	c                        *websocket.Conn
}

// OnIncoming assigns listener for the incoming messages
func (w *wsAdapter) OnIncoming(msgHandlers ...func(message interface{})) {
	w.internalIncomingHandlers = append(w.internalIncomingHandlers, msgHandlers...)
}

func (w *wsAdapter) OnOutgoing(msgHandlers ...func(message interface{})) {
	w.internalOutgoingHandlers = append(w.internalOutgoingHandlers, msgHandlers...)
}

// OnClose assigns listener for the close events
func (w *wsAdapter) OnClose(closeHandlers ...func(reason string)) {
	w.internalCloseHandlers = append(w.internalCloseHandlers, closeHandlers...)
}

// starts the websocket connection
func (w *wsAdapter) start() {
	ctx := context.Background()
	for {
		var data interface{}
		err := wsjson.Read(ctx, w.c, &data)
		if err != nil {
			LogError("error: %s", err.Error())
			break
		}
		LogDebug("message received: %v", data)
		if len(w.internalIncomingHandlers) > 0 {
			for _, fn := range w.internalIncomingHandlers {
				fn(data)
			}
		}
	}
	w.Close("finished with connection")
	LogDebug("finished with connection")
}

// SendOut sends message to the web socket
func (w *wsAdapter) SendOut(data interface{}) (resultErr error) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	if resultErr = wsjson.Write(ctx, w.c, data); resultErr == nil {
		if len(w.internalOutgoingHandlers) > 0 {
			for _, fn := range w.internalOutgoingHandlers {
				fn(data)
			}
		}
	}
	return
}

// Close closes the websocket
func (w *wsAdapter) Close(reason string) {
	_ = w.c.Close(websocket.StatusNormalClosure, reason)
	if len(w.internalCloseHandlers) > 0 {
		for _, fn := range w.internalCloseHandlers {
			fn(reason)
		}
	}
}

// NewWsAdapter is a constructor of ws adapter
func NewWsAdapter(w http.ResponseWriter, r *http.Request) (result Adapter, resultErr error) {
	c, err := websocket.Accept(w, r, nil)
	if err != nil {
		LogError("error: %s", err.Error())
		return
	}
	wsa := &wsAdapter{c: c}
	go wsa.start()
	result = wsa
	return
}
