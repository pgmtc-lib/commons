package eventbus

type mockAdapter struct {
	wantSendErr        error
	onIncomingHandlers []func(message interface{})
	onIncomingSet      bool
	onOutgoingHandlers []func(message interface{})
	onOutgoingCalled   bool
	onCloseHandlers    []func(reason string)
	onCloseCalled      bool
	sendOutCalled      bool
	closeCalled        bool
}

func (m *mockAdapter) triggerIncoming(message interface{}) {
	for _, incomingHandler := range m.onIncomingHandlers {
		incomingHandler(message)
	}
}
func (m *mockAdapter) triggerOnClose(reason string) {
	for _, closeHandler := range m.onCloseHandlers {
		closeHandler(reason)
	}
}

func (m *mockAdapter) triggerOutgoing(message interface{}) {
	for _, outgoingHandler := range m.onOutgoingHandlers {
		outgoingHandler(message)
	}
}

func (m *mockAdapter) OnIncoming(f ...func(message interface{})) {
	m.onIncomingHandlers = append(m.onIncomingHandlers, f...)
	m.onIncomingSet = true
}

func (m *mockAdapter) OnOutgoing(f ...func(message interface{})) {
	m.onOutgoingHandlers = append(m.onOutgoingHandlers, f...)
	m.onOutgoingCalled = true
}

func (m *mockAdapter) OnClose(f ...func(reason string)) {
	m.onCloseHandlers = append(m.onCloseHandlers, f...)
	m.onCloseCalled = true
}

func (m *mockAdapter) SendOut(message interface{}) error {
	m.sendOutCalled = true
	return m.wantSendErr
}

func (m *mockAdapter) Close(reason string) {
	m.closeCalled = true
}
