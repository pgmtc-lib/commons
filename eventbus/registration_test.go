package eventbus

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestBusRegistration_ListenTopics(t *testing.T) {
	b := &Bus{}
	a := &mockAdapter{}
	a2 := &mockAdapter{}
	reg := BusRegistration{
		b:         b,
		a:         a,
		handlerID: "handlerID",
	}
	reg2 := BusRegistration{
		b:         b,
		a:         a2,
		handlerID: "handlerID2",
	}
	reg.ListenTopics("id1", "id2")
	reg2.ListenTopics("id1", "id3")
	expected := map[string]map[string]Adapter{
		"id1": map[string]Adapter{
			"handlerID":  a,
			"handlerID2": a2,
		},
		"id2": map[string]Adapter{
			"handlerID": a,
		},
		"id3": map[string]Adapter{
			"handlerID2": a2,
		},
	}
	assert.Equal(t, expected, b.handlers)
	// test outgoing handler
	b.Publish("id2", "test message")
	assert.Equal(t, true, reg.a.(*mockAdapter).sendOutCalled)
	assert.Equal(t, false, reg2.a.(*mockAdapter).sendOutCalled)
	reg.a.(*mockAdapter).sendOutCalled = false
	reg2.a.(*mockAdapter).sendOutCalled = false
	// test both
	b.Publish("id1", "test message")
	assert.Equal(t, true, reg.a.(*mockAdapter).sendOutCalled)
	assert.Equal(t, true, reg2.a.(*mockAdapter).sendOutCalled)
	reg.a.(*mockAdapter).sendOutCalled = false
	reg2.a.(*mockAdapter).sendOutCalled = false
	// test none
	b.Publish("id7", "test message")
	assert.Equal(t, false, reg.a.(*mockAdapter).sendOutCalled)
	assert.Equal(t, false, reg2.a.(*mockAdapter).sendOutCalled)
	reg.a.(*mockAdapter).sendOutCalled = false
	reg2.a.(*mockAdapter).sendOutCalled = false
	// test when it fails
	reg.a.(*mockAdapter).closeCalled = false
	reg2.a.(*mockAdapter).closeCalled = false
	reg2.a.(*mockAdapter).wantSendErr = fmt.Errorf("mock err")
	b.Publish("id3", "test message")
	assert.Equal(t, false, reg.a.(*mockAdapter).sendOutCalled)
	assert.Equal(t, true, reg2.a.(*mockAdapter).sendOutCalled)
	assert.Equal(t, false, reg.a.(*mockAdapter).closeCalled)
	assert.Equal(t, true, reg2.a.(*mockAdapter).closeCalled)
	// test trigger close from the adapter
	reg.a.(*mockAdapter).triggerOnClose("mock reason")
	expected = map[string]map[string]Adapter{
		"id1": map[string]Adapter{
			"handlerID2": a2,
		},
		"id2": map[string]Adapter{},
		"id3": map[string]Adapter{
			"handlerID2": a2,
		},
	}
	assert.Equal(t, expected, b.handlers)
}

func TestBusRegistration_PublishTopics(t *testing.T) {
	b := &Bus{}
	a := &mockAdapter{}
	reg := BusRegistration{
		b:         b,
		a:         a,
		handlerID: "handlerID",
	}
	assert.False(t, a.onIncomingSet)
	reg.PublishTopics("id1", "id2")
	assert.True(t, a.onIncomingSet)

}
