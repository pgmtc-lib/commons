package chans

import (
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestWaitWithTimeout(t *testing.T) {
	waitChan := make(chan bool)
	// test bare
	err := WaitWithTimeout(waitChan, 1*time.Second)
	assert.EqualError(t, err, "wait time out")

	go func() {
		time.Sleep(500 * time.Millisecond)
		waitChan <- true
	}()

	err = WaitWithTimeout(waitChan, 1*time.Second)
	assert.NoError(t, err)

}

func TestWaitFor(t *testing.T) {
	waitChan := make(chan interface{})
	// test bare
	result, err := WaitFor(waitChan, 1*time.Second)
	assert.EqualError(t, err, "wait time out")
	assert.Empty(t, result)

	go func() {
		time.Sleep(500 * time.Millisecond)
		waitChan <- time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC)
	}()

	result, err = WaitFor(waitChan, 1*time.Second)
	assert.NoError(t, err)
	assert.Equal(t, time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC), result)

}
