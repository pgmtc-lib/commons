package chans

import (
	"fmt"
	"time"
)

// WaitWithTimeout waits until doneChan receives something until time out occurs
func WaitWithTimeout(doneChan chan bool, timeout time.Duration) error {
	select {
	case <-doneChan:
		return nil
	case <-time.After(timeout):
		return fmt.Errorf("wait time out")
	}
}

// WaitFor waits for incoming from chanel up to a maximum timeout
func WaitFor(responseChan chan interface{}, timeout time.Duration) (result interface{}, resultErr error) {
	select {
	case response := <-responseChan:
		result = response

	case <-time.After(timeout):
		resultErr = fmt.Errorf("wait time out")
	}
	return
}
