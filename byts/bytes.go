package byts

import (
	"fmt"
	"strings"
)

// Format formats bytes in hex with dividers
func Format(in []byte, separator ...int) string {
	var strBytes []string
	currentSplitAfter := 0
	currentSplitAfterIdx := 0
	if len(separator) > 0 {
		currentSplitAfter = separator[currentSplitAfterIdx]
	}
	for idx, item := range in {
		strBytes = append(strBytes, fmt.Sprintf("%02x ", item))
		if idx < len(in)-1 && currentSplitAfter == idx+1 {
			strBytes = append(strBytes, "| ")
			if len(separator) > (currentSplitAfterIdx + 1) {
				currentSplitAfterIdx++
				currentSplitAfter = separator[currentSplitAfterIdx] + currentSplitAfter
			}
		}
	}
	return strings.Trim(strings.Join(strBytes, ""), " ")
}

// Chunks split the byte array into chunks and run handler for each of them
func Chunks(in []byte, chunkLength int) (result [][]byte) {
	if chunkLength == 0 {
		return [][]byte{}
	}
	if len(in)%chunkLength != 0 {
		// fill in missing bytes
		filled := make([]byte, ((len(in)/chunkLength)+1)*chunkLength)
		copy(filled, in)
		in = filled
	}
	for i := 0; i < len(in)/chunkLength; i++ {
		msgStart := i * chunkLength
		result = append(result, in[msgStart:msgStart+chunkLength])
	}
	return
}

// Reverse reverses the byte array
func Reverse(in []byte) []byte {
	if in == nil {
		return in
	}
	a := make([]byte, len(in))
	copy(a, in)
	for i := len(a)/2 - 1; i >= 0; i-- {
		opp := len(a) - 1 - i
		a[i], a[opp] = a[opp], a[i]
	}
	return a
}

// SplitByte splits byte into multiple integers by pos
func SplitByte(in byte, pos int) (result [2]int) {
	return [2]int{
		int(in >> (8 - pos)),
		int(in << pos >> pos),
	}
}

// JoinByte joins two small ints into one byte
func JoinByte(num1 int, num2 int, pos int) (result byte) {
	result = byte(num1) << (8 - pos) // we shift it to the left
	result |= byte(num2)             // we overwrite whole thing with num 2
	return
}
