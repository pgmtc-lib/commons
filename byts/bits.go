package byts

import "fmt"

// SetBit the bit at position (from least significant bit) in byte b
func SetBit(b byte, posFromLBit uint) (result byte) {
	//fmt.Printf("before %08b\b", b)
	result = b | (1 << posFromLBit)
	return
}

// SetBitIf sets bit at position (from least significant bit) in byte b, if condition is true
func SetBitIf(b byte, posFromLBit uint, condition bool) (result byte) {
	if !condition {
		return b
	}
	return SetBit(b, posFromLBit)
}

// ClearBit the bit at position (from least significant bit) in byte b
func ClearBit(b byte, posFromLBit uint) byte {
	mask := byte(^(1 << posFromLBit))
	b &= mask
	return b
}

// HasBit returns if a bit in byte b, position posFromLBit (from least significant bit) is 1
func HasBit(b byte, posFromLBit uint) bool {
	val := b & (1 << posFromLBit)
	return val > 0
}

// PrintBits converts byte into string with bits
func PrintBits(b byte) string {
	return fmt.Sprintf("%08b", b)
}
