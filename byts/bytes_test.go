package byts

import (
	"github.com/stretchr/testify/assert"
	"reflect"
	"testing"
)

func TestFormat(t *testing.T) {
	type args struct {
		in         []byte
		splitAfter []int
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "test-no-separator",
			args: args{in: []byte{1, 2, 3, 4, 5, 6, 7, 8, 9}},
			want: "01 02 03 04 05 06 07 08 09",
		},
		{
			name: "test-one-separator",
			args: args{in: []byte{1, 2, 3, 4, 5, 6, 7, 8, 9}, splitAfter: []int{2}},
			want: "01 02 | 03 04 05 06 07 08 09",
		},
		{
			name: "test-separator",
			args: args{in: []byte{1, 2, 3, 4, 5, 6, 7, 8, 9}, splitAfter: []int{2, 2, 3}},
			want: "01 02 | 03 04 | 05 06 07 | 08 09",
		},
		{
			name: "test-separator-end",
			args: args{in: []byte{1, 2, 3, 4, 5, 6, 7, 8, 9}, splitAfter: []int{2, 7}},
			want: "01 02 | 03 04 05 06 07 08 09",
		},
		{
			name: "test-separator-rubbish",
			args: args{in: []byte{1, 2, 3, 4, 5, 6, 7, 8, 9}, splitAfter: []int{10}},
			want: "01 02 03 04 05 06 07 08 09",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Format(tt.args.in, tt.args.splitAfter...); got != tt.want {
				t.Errorf("Format() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestChunks(t *testing.T) {
	var got [][]byte
	in := []byte{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12}
	got = Chunks(in, 3)
	assert.Equal(t, [][]byte{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}, {10, 11, 12}}, got)
	// test by 4
	got = Chunks(in, 4)
	assert.Equal(t, [][]byte{{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12}}, got)
	// test overflowing
	got = Chunks(in, 5)
	assert.Equal(t, [][]byte{{1, 2, 3, 4, 5}, {6, 7, 8, 9, 10}, {11, 12, 0, 0, 0}}, got)
	// test 0 sized chunks
	got = Chunks(in, 0)
	assert.Equal(t, [][]byte{}, got)
	// test one overflowing chunk
	got = Chunks(in, 15)
	assert.Equal(t, [][]byte{{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 0, 0, 0}}, got)
	// test one sized chunk
	got = Chunks(in, 1)
	assert.Equal(t, [][]byte{{1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}}, got)

}

func TestReverse(t *testing.T) {
	tests := []struct {
		name string
		in   []byte
		want []byte
	}{
		{
			name: "test-multiple",
			in:   []byte{1, 3, 2},
			want: []byte{2, 3, 1},
		},
		{
			name: "test-one",
			in:   []byte{3},
			want: []byte{3},
		},
		{
			name: "test-none",
			in:   []byte{},
			want: []byte{},
		},
		{
			name: "test-nil",
			in:   nil,
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Reverse(tt.in); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Reverse() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestSplitByte(t *testing.T) {
	tests := []struct {
		name       string
		in         byte
		pos        int
		wantResult [2]int
	}{
		{in: 0b11111111, pos: 4, wantResult: [2]int{0b1111, 0b1111}},
		{in: 0b11110000, pos: 4, wantResult: [2]int{0b1111, 0b0000}},
		{in: 0b00001111, pos: 4, wantResult: [2]int{0b0000, 0b1111}},
		{in: 0b10010110, pos: 4, wantResult: [2]int{0b1001, 0b0110}},
		{in: 0b01010011, pos: 6, wantResult: [2]int{0b10100, 0b11}},
		{in: 0b11010100, pos: 2, wantResult: [2]int{0b11, 0b10100}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotResult := SplitByte(tt.in, tt.pos); !reflect.DeepEqual(gotResult, tt.wantResult) {
				t.Errorf("SplitByte() = %v, want %v", gotResult, tt.wantResult)
			}
		})
	}
}

func TestJoinByte(t *testing.T) {
	tests := []struct {
		name       string
		wantResult byte
		num1       int
		num2       int
		pos        int
	}{
		{wantResult: 0b11111111, num1: 0b1111, num2: 0b1111, pos: 4},
		{wantResult: 0b00001111, num1: 0b0000, num2: 0b1111, pos: 4},
		{wantResult: 0b11100001, num1: 0b11, num2: 0b100001, pos: 2},
		{wantResult: 0b10000010, num1: 0b100000, num2: 0b10, pos: 6},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotResult := JoinByte(tt.num1, tt.num2, tt.pos); gotResult != tt.wantResult {
				t.Errorf("JoinByte() = %08b, want %08b", gotResult, tt.wantResult)
			}
		})
	}
}
