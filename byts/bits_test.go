package byts

import "testing"

func TestSetBit(t *testing.T) {
	tests := []struct {
		name        string
		n           byte
		posFromLBit uint
		want        byte
	}{
		{n: 0b00000000, posFromLBit: 0, want: 0b00000001},
		{n: 0b00000000, posFromLBit: 7, want: 0b10000000},
		{n: 0b10000001, posFromLBit: 4, want: 0b10010001},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := SetBit(tt.n, tt.posFromLBit); got != tt.want {
				t.Errorf("SetBit() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestSetBitIf(t *testing.T) {
	tests := []struct {
		name        string
		b           byte
		posFromLBit uint
		condition   bool
		want        byte
	}{
		{b: 0b10000000, posFromLBit: 0, condition: true, want: 0b10000001},
		{b: 0b10000000, posFromLBit: 0, condition: false, want: 0b10000000},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotResult := SetBitIf(tt.b, tt.posFromLBit, tt.condition); gotResult != tt.want {
				t.Errorf("SetBitIf() = %v, want %v", gotResult, tt.want)
			}
		})
	}
}

func TestClearBit(t *testing.T) {
	tests := []struct {
		name        string
		n           byte
		posFromLBit uint
		want        byte
	}{
		{n: 0b11111111, posFromLBit: 0, want: 0b11111110},
		{n: 0b11111111, posFromLBit: 7, want: 0b01111111},
		{n: 0b01111110, posFromLBit: 4, want: 0b01101110},
		{n: 0b00000000, posFromLBit: 0, want: 0b00000000},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ClearBit(tt.n, tt.posFromLBit); got != tt.want {
				t.Errorf("ClearBit() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestHasBit(t *testing.T) {
	tests := []struct {
		name        string
		n           byte
		posFromLBit uint
		want        bool
	}{
		{n: 0b01010101, posFromLBit: 0, want: true},
		{n: 0b01010101, posFromLBit: 7, want: false},
		{n: 0b01111110, posFromLBit: 4, want: true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := HasBit(tt.n, tt.posFromLBit); got != tt.want {
				t.Errorf("HasBit() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPrintBits(t *testing.T) {
	tests := []struct {
		name string
		b    byte
		want string
	}{
		{b: 0b01010101, want: "01010101"},
		{b: 0b01111110, want: "01111110"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := PrintBits(tt.b); got != tt.want {
				t.Errorf("PrintBits() = %v, want %v", got, tt.want)
			}
		})
	}
}
